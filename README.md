# FastCodonBS

FastCodonBS is a **prototype** for the inference of positive selection in protein-coding genes.

## Tutorial

### Basic
* [Installing FastCodonBS](documentation/installation.md)
* [Using FastCodonBS command line](documentation/commandLine.md)
* [Output files](documentation/outputFiles.md)

### Advanced
* [Using FastCodonBS XML configuration files](documentation/configXML.md)

## General information

### What is maximized
FastCodonBS always optimize the branch lengths and the model parameters.

### How to use FastCodonBS in parallel
The parallel gradient option (nGradient) will generally be the most efficient way to use FastCodonBS in parallel.
The limitation of the gradient option is the number of parameters (model parameters + branch lengths).
Using multiple thread (nThread) per likelihood evaluation can then further improve the computational speed.

As a general guideline, using parallel gradient will scale well as long as nGradient >> nParams.
When nGradient ~= nParams, then it's better to increase nThread and to keep it as small as possible.

## Data
### Simulated datasets
Example of simulated datasets can be found in the `dataset` folder.
