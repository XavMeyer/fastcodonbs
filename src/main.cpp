//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//============================================================================
// Name        : main.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#define DEBUG_MC3 1


#include <vector>
#include <iostream>
#include <fstream>

#include "Model/Parameter/Parameters.h"
#include "Model/Prior/Priors.h"
#include "Model/Model.h"
#include "Sampler/Samples/Samples.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Likelihood/LikelihoodFactory.h"
#include "Model/Likelihood/Helper/Helpers.h"
#include "Utils/XML/ReaderXML.h"

std::string getHelpMessage() {
	std::stringstream ss;

	ss << ">> mpirun -np nGrad ./fastCodonBS -m BS -nG nGradient -nT nThread -align /path/to/align.fas -tree /path/to/tree.fas -out /path/to/outputBaseName [-s seed]" << std::endl;
	ss << "Model:" << std::endl;
	ss << "\t-m\t\t Model: only Branch-Site is available through commad line for now - all branch are tested + BEB." << std::endl;
	ss << "Parallel config (see Bitbucket documentation):" << std::endl;
	ss << "\t-nG\t\t Number of processor dedicated for the evaluation of the gradient - distributed memory parallelism (mpi)." << std::endl;
	ss << "\t-nT\t\t Number of processor dedicated for the evaluation for one likelihood - shared memory parallelism (posix)." << std::endl;
	ss << "Input/output:" << std::endl;
	ss << "\t-align\t\t Alignment file (fasta)." << std::endl;
	ss << "\t-tree\t\t Tree file (newick)." << std::endl;
	ss << "\t-out\t\t Path and base name for the output file(s)." << std::endl;
	ss << "Misc/Optional:" << std::endl;
	ss << "\t-seed\t\t Random number generator seed for the starting parameter values." << std::endl;
	ss << "\t-h\t\t Print this message." << std::endl;

	return ss.str();
}

enum commandType_t {HELP_CMD, XML_FILE, CMD_LINE};
commandType_t  parseParameters(int argc, char** argv, std::vector< std::string> &values) {

	const size_t N_MANDATORY_ARG = 6;
	const size_t N_OPTIONAL_ARG = 1;
	const size_t N_ARG = N_MANDATORY_ARG + N_OPTIONAL_ARG ;

	std::vector<bool> found(N_MANDATORY_ARG, false);
	values.clear();
	values.assign(N_ARG, "1234");

	bool isHelpCmd = argc == 2;
	bool isXMLCmd = argc == 3;
	bool isOtherCmd = (argc-1) % 2 == 0;
	bool isOkCmd = (isHelpCmd || isXMLCmd || isOtherCmd);

	if(isHelpCmd) {
		if( std::string(argv[1]) == "-h") {
			std::cout << getHelpMessage() << std::endl;
			return HELP_CMD;
		} else {
			throw std::string("Erroneous command - see help command (-h)");
			//std::cerr <<  << std::endl;
			//abort();
		}
	}

	if(isXMLCmd) {
		if( std::string(argv[1]) == "-x") {
			return XML_FILE;
		} else {
			throw std::string("Erroneous command - see help command (-h)");
			//std::cerr << "Erroneous command - see help command (-h)" << std::endl;
			//abort();
		}
	}

	std::vector<std::string> keys = boost::assign::list_of("-m")("-nG")("-nT")("-align")("-tree")("-out")("-seed");

	if(!isOkCmd) {
		throw std::string("Erroneous command - see help command (-h)");
		//std::cerr << "Erroneous command - see help command (-h)" << std::endl;
		//abort();
	}

	for(size_t i=1; i<argc; i+=2) {

		std::string key(argv[i]);
		std::string val(argv[i+1]);

		std::vector<std::string>::iterator itFind = std::find(keys.begin(), keys.end(), key);
		if(itFind == keys.end()) {
			std::stringstream ss;
			ss << "Command : '" << key << "' is not defined (call -h for help). " << std::endl;
			throw ss.str();
		}

		size_t index = std::distance(keys.begin(), itFind);
		values[index] = val;
		if(index < N_MANDATORY_ARG) {
			found[index] = true;
		}

	}

	size_t nFound = std::count(found.begin(), found.end(), true);
	if(nFound != N_MANDATORY_ARG)  {
		throw std::string("Erroneous command - see help command (-h)");
	}

	return CMD_LINE;
}

std::string createBranchSiteXML(std::vector<std::string> &values) {

	std::stringstream ss;

	ss << "<config method=\"BEB\"> " << std::endl;
	ss << "    <topology>" << std::endl;
	ss << "        <nGradient>" << values[1] << "</nGradient> " << std::endl;
	ss << "    </topology>" << std::endl;
	ss << "    <seed>" << values[6] << "</seed>" << std::endl;
	ss << "    <nIteration>0</nIteration>" << std::endl;
	//ss << "    <logFile>" << values[5] << "</logFile> " << std::endl;
	ss << "    <output>" << std::endl;
	ss << "      <filename>" << values[5] << "</filename>" << std::endl;
	ss << "      <verbose>default</verbose>" << std::endl;
	ss << "    </output>" << std::endl;
	ss << "    <likelihood name=\"PositiveSelectionLight\"> " << std::endl;
	ss << "        <hypothesis>H1</hypothesis>" << std::endl;
	ss << "        <isUsingCompression>true</isUsingCompression>" << std::endl;
	ss << "        <foregroundBranch>all</foregroundBranch> " << std::endl;
	ss << "        <nThread>" << values[2] << "</nThread> " << std::endl;
	ss << "        <CodonFrequencyType_ID>3</CodonFrequencyType_ID>" << std::endl;
	ss << "        <alignFile>" << values[3] << "</alignFile>" << std::endl;
	ss << "        <treeFile>" << values[4] << "</treeFile>" << std::endl;
	ss << "        <parameters>default</parameters>" << std::endl;
	ss << "    </likelihood>" << std::endl;
	ss << "    <maximizer name=\"LBFGSB\"> <!--NLOPT or LBFGSB -->" << std::endl;
	ss << "    </maximizer>" << std::endl;
	ss << "</config>" << std::endl;


	return ss.str();
}


int main(int argc, char** argv) {

	// Init the MPI environnement
	Parallel::mpiMgr().init(&argc, &argv);

	std::vector<std::string> values;
	commandType_t cmdType;

	try {
		cmdType = parseParameters(argc, argv, values);
	} catch(std::string const& e) {
		std::cerr << e << std::endl;
		return 1;
	}


	if(cmdType == XML_FILE) {
		XML::ReaderXML readerXML(argv[2]);
		readerXML.run();
	} else if(cmdType == CMD_LINE) {
		if(values[0] == "BS") {
			std::string xmlContent(createBranchSiteXML(values));
			XML::ReaderXML readerXML("", xmlContent);
			readerXML.run();
		} else {
			std::cerr << "Unknown model - see help command (-h) for available models." << std::endl;
			abort();
		}
	}

	return 0;
}
