//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SelPosTreeAlgorithm.h
 *
 * @date Aug 17, 2015
 * @author meyerx
 * @brief
 */
#ifndef SELPOSTREEALGORITHM_H_
#define SELPOSTREEALGORITHM_H_

#include <stddef.h>

#include "Base/BaseAlgorithm.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/IncNodes.h"

namespace DAG { class BaseNode; }

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

class SelPosTreeAlgorithm : public BaseAlgorithm {
public:
	SelPosTreeAlgorithm(const size_t aNThread, const std::list<BaseNode*> &aTopoList);
	~SelPosTreeAlgorithm();

	void init(bool isFullReinit);
	void assignTaskToProc(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap);

private:

	//void separateClass2P(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap);

	void assignSubSiteTree(const size_t iProc, BaseNode *node, vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap, size_t &countBMN);
	void divideSites(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap);



};

} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */

#endif /* SELPOSTREEALGORITHM_H_ */
