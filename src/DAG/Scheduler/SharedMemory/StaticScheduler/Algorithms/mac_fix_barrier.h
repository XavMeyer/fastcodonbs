/*
 * mac_fix_barrier.h
 *
 *  Created on: Mar 27, 2018
 *      Author: meyerx
 */

#ifdef __APPLE__


#ifndef DAG_SCHEDULER_SHAREDMEMORY_STATICSCHEDULER_ALGORITHMS_BASE_MAC_FIX_BARRIER_H_
#define DAG_SCHEDULER_SHAREDMEMORY_STATICSCHEDULER_ALGORITHMS_BASE_MAC_FIX_BARRIER_H_

#include <pthread.h>
#include <errno.h>

typedef int pthread_barrierattr_t;
typedef struct
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int count;
    int tripCount;
} pthread_barrier_t;


int pthread_barrier_init(pthread_barrier_t *barrier, const pthread_barrierattr_t *attr, unsigned int count);
int pthread_barrier_destroy(pthread_barrier_t *barrier);
int pthread_barrier_wait(pthread_barrier_t *barrier);

#endif /* DAG_SCHEDULER_SHAREDMEMORY_STATICSCHEDULER_ALGORITHMS_BASE_MAC_FIX_BARRIER_H_ */
#endif // __APPLE__




