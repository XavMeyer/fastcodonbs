//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SelPosTreeAlgorithm.cpp
 *
 * @date Aug 17, 2015
 * @author meyerx
 * @brief
 */
#include <DAG/Scheduler/SharedMemory/StaticScheduler/Algorithms/SelPosTreeAlgorithm.h>
#include <assert.h>

#include "DAG/Node/Base/BaseNode.h"

namespace DAG {
namespace Scheduler {
namespace Static {
namespace Algorithms {

SelPosTreeAlgorithm::SelPosTreeAlgorithm(const size_t aNThread, const std::list<BaseNode*> &aTopoList) : BaseAlgorithm(aNThread, aTopoList) {

}

SelPosTreeAlgorithm::~SelPosTreeAlgorithm() {
}

void SelPosTreeAlgorithm::init(bool isFullReinit) {
}

void SelPosTreeAlgorithm::assignTaskToProc(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap) {
	divideSites(assignVec, assignMap);
}

void SelPosTreeAlgorithm::assignSubSiteTree(const size_t iProc, BaseNode *node, vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap, size_t &countBMN) {
	using namespace StatisticalModel::Likelihood::PositiveSelection::Light;

	for(size_t iC=0; iC<node->getNChildren(); ++iC) {
		BaseNode *childNode = node->getChild(iC);

		const std::type_info &childtype = typeid(*childNode);
		if(childtype == typeid(RootNode)) {
			assignSubSiteTree(iProc, childNode, assignVec, assignMap, countBMN);
		} else if(childtype == typeid(CPVNode)) {
			assignSubSiteTree(iProc, childNode, assignVec, assignMap, countBMN);
		} else if(childtype == typeid(LeafNode)) {
			assignSubSiteTree(iProc, childNode, assignVec, assignMap, countBMN);
		} else if(childtype == typeid(BranchMatrixNode)) {
			++countBMN;
			if(countBMN % nThread != iProc){
				continue;
			}
		} else {
			continue;
		}

		// assign node
		if(assignMap.find(childNode->getId()) == assignMap.end()) {
			NodeAssignment nAssign(iProc, childNode);
			assignVec[iProc].push_back(childNode);
			assignMap[childNode->getId()] = nAssign;
		}
	}
}


void SelPosTreeAlgorithm::divideSites(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap) {
	using namespace StatisticalModel::Likelihood::PositiveSelection::Light;

	assignVec.resize(nThread);
	for(size_t iP=0; iP<assignVec.size(); ++iP) {
		assignVec[iP].clear();
		assignMap.clear();
	}

	size_t mnCount = 0;
	size_t csnCount = 0;

	typedef std::list<BaseNode*>::const_iterator itList_t;
	for(itList_t it=topoList.begin(); it != topoList.end(); ++it) {
		BaseNode* node = (*it);

		size_t nextProc = 0;
		const std::type_info &nodeType = typeid(*node);
		if(nodeType == typeid(FinalResultNode)){
			nextProc = 0;
		} else if(nodeType == typeid(CombineSiteNode)) {
			assert(csnCount < nThread);
			size_t countBMN = 0;
			assignSubSiteTree(csnCount, node, assignVec, assignMap, countBMN);
			nextProc = csnCount;
			csnCount++;
		} else if(nodeType == typeid(MatrixScalingNode)) {
			nextProc = 0;
		} else if(nodeType == typeid(MatrixNode)) {
			nextProc = mnCount % nThread;
			mnCount++;
		} else {
			continue; // Skip to next node
		}

		// assign node
		NodeAssignment nAssign(nextProc, node);
		assignVec[nextProc].push_back(node);
		assignMap[node->getId()] = nAssign;
	}

	assert(csnCount == nThread);

	/*for(size_t iP=0; iP<assignVec.size(); ++iP) {
		std::cout << "Processor " << iP << " must process : ( " << assignVec[iP].size() << " )" << std::endl;
		for(size_t iN=0; iN<assignVec[iP].size(); ++iN) {
			std::cout << assignVec[iP][iN]->toString() << std::endl;
		}
		std::cout << "-------------------------------------------------" << std::endl;
	}
	getchar();*/


}

/*void SelPosTreeAlgorithm::separateClass2P(vecBaseNode2D_t &assignVec, assignmentMap_t &assignMap) {
	assert(nThread == 2);
	using namespace StatisticalModel::Likelihood::PositiveSelection::Light;

	assignVec.resize(nThread);
	for(size_t iP=0; iP<assignVec.size(); ++iP) {
		assignVec[iP].clear();
		assignMap.clear();
	}
	typedef std::list<BaseNode*>::const_iterator itList_t;
	for(itList_t it=topoList.begin(); it != topoList.end(); ++it) {
		BaseNode* node = (*it);

		size_t nextProc = 0;

		const std::type_info &childtype = typeid(*node);
		if(childtype == typeid(FinalResultNode)){
			nextProc = 0;
		} else if(childtype == typeid(CombineSiteNode)) {
			nextProc = 0;
		} else if(childtype == typeid(RootNode)) {
			RootNode *rNode = dynamic_cast<RootNode*>(node);
			if(rNode->getOmegaClass() == OMEGA_0) {
				nextProc = 0;
			} else {
				nextProc = 1;
			}
		} else if(childtype == typeid(CPVNode)) {
			CPVNode *cpvNode = dynamic_cast<CPVNode*>(node);
			if(cpvNode->getOmegaClass() == OMEGA_0) {
				nextProc = 0;
			} else {
				nextProc = 1;
			}
		} else if(childtype == typeid(LeafNode)) {
			LeafNode *lNode = dynamic_cast<LeafNode*>(node);
			if(lNode->getOmegaClass() == OMEGA_0) {
				nextProc = 0;
			} else {
				nextProc = 1;
			}
		} else if(childtype == typeid(BranchMatrixNode)) {
			BranchMatrixNode *bmNode = dynamic_cast<BranchMatrixNode*>(node);
			if(bmNode->getOmegaClass() == OMEGA_0) {
				nextProc = 0;
			} else {
				nextProc = 1;
			}
		} else if(childtype == typeid(MatrixScalingNode)) {
			MatrixScalingNode *msNode = dynamic_cast<MatrixScalingNode*>(node);
			nextProc = 1;
		} else if(childtype == typeid(MatrixNode)) {
			MatrixNode *mNode = dynamic_cast<MatrixNode*>(node);
			if(mNode->getOmegaClass() == 0) {
				nextProc = 0;
			} else {
				nextProc = 1;
			}
		} else {
			std::cerr << "Forgot a node type..." << std::endl;
		}

		// assign node
		NodeAssignment nAssign(nextProc, node);
		assignVec[nextProc].push_back(node);
		assignMap[node->getId()] = nAssign;
	}
}*/

} /* namespace Algorithms */
} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */
