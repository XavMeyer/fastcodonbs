//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ThreadSafeScheduler.cpp
 *
 * @date Aug 13, 2015
 * @author meyerx
 * @brief
 */
#include "ThreadSafeScheduler.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep

namespace DAG { class BaseNode; }

namespace DAG {
namespace Scheduler {
namespace Static {

ThreadSafeScheduler::ThreadSafeScheduler(const size_t aNThread, DAG::BaseNode* const aRoot) :
		BaseScheduler(aRoot), nThread(aNThread), sharedObj(nThread, aRoot),
		prioAlgo(new Algorithms::DefaultAlgorithm(nThread, topoList)), myWorkers(nThread) {

	cntCall = 0;

	prioAlgo->init(true);
	prioAlgo->assignTaskToProc(sharedObj.assignVec, sharedObj.assignMap);

	// Create Workers here
	for(size_t iT=0; iT<nThread; ++iT) {
		myWorkers[iT] = new Worker(iT, sharedObj);
	}

	// this is not a thread, its the main process worker : things have to be done manually
	myWorkers[0]->init();
	//sharedObj.sendNextTask(SharedObjects::INIT);

}

ThreadSafeScheduler::~ThreadSafeScheduler() {
	sharedObj.sendNextTask(SharedObjects::STOP);
	delete myWorkers[0];
	for(size_t iT=1; iT<nThread; ++iT) {
		myWorkers[iT]->join();
		delete myWorkers[iT];
	}
}


void ThreadSafeScheduler::doProcessing(bool doFakeComputation) {
	if(cntCall > 0 && (cntCall % 500) == 0) {
		// Process new scheduling
		//CustomProfiling cp;
		//cp.startTime();
		prioAlgo->init(false);
		prioAlgo->assignTaskToProc(sharedObj.assignVec, sharedObj.assignMap);
		//std::cout << prioAlgo->getSchedulingLength(sharedObj.assignVec, sharedObj.assignMap)*1.e3 << std::endl;
		//cp.endTime();
		//std::cout << "Took : " << cp.duration() << std::endl;

		// Send changes
		sharedObj.sendNextTask(SharedObjects::INIT_AND_PROCESS);
		myWorkers[0]->init();
		myWorkers[0]->process();
	} else {
		// Launch workers
		sharedObj.fakeComputation = doFakeComputation;
		sharedObj.sendNextTask(SharedObjects::PROCESS);
		myWorkers[0]->process();
	}


	++cntCall;
}

void ThreadSafeScheduler::reinitialize(reinitStrategy_t reinitStrat) {
	bool reinitTopo = true;
	BaseScheduler::defaultReinitialize(reinitStrat, reinitTopo);
	//BaseScheduler::defaultReinitialize(PARTIAL_REINIT, reinitTopo);
	//cntCall = 0;

	//prioAlgo.reset(new Algorithms::DefaultAlgorithm(nThread, topoList));
	prioAlgo->init(false);
	prioAlgo->assignTaskToProc(sharedObj.assignVec, sharedObj.assignMap);

	for(size_t iW=0; iW<myWorkers.size(); ++iW) {
		myWorkers[iW]->init();
	}

}

} /* namespace Static */
} /* namespace Scheduler */
} /* namespace DAG */
