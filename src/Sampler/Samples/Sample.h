//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sample.h
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#ifndef SAMPLE_H_
#define SAMPLE_H_

#include <stddef.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <sstream>
#include <vector>
#include <iosfwd>

#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep



using namespace std;

namespace Sampler {
	class Sample;

namespace SampleUtils {
	typedef std::pair<size_t, Sample> pairItSample_t;
	typedef std::vector<pairItSample_t> vecPairItSample_t;


	typedef std::vector<double> markers_t;

}
}

namespace Sampler {

class Sample {
public:
	Sample();
	Sample(const Sample &toCopy);
	~Sample();

	Sample& operator=(const Sample& toCopy);

	bool isInteger(int aInd) const;
	bool isDouble(int aInd) const;

	void incParameter(int aInd, double aIncr);
	void multParameter(int aInd, double aMult);
	double getDoubleParameter(int aInd) const;
	void setDoubleParameter(int aInd, double aVal);
	long int getIntParameter(int aInd) const;
	void setIntParameter(int aInd, long int aVal);
	size_t getNbParameters() const;

	void setMarkers(const std::vector<double> &aMarkers);

	void setChanged(bool aChanged);
	bool hasChanged() const;

	void setEvaluated(bool aEvaluated);
	bool isEvaluated() const;

	bool hasSameContinuousParameters(const Sample &other) const;

	void setIntValues(const std::vector<long int> &aIV) {intValues = aIV;}
	void setDblValues(const std::vector<double> &aDV) {dblValues = aDV;}

	vector<long int>& getIntValues() {return intValues;}
	vector<double>& getDblValues() {return dblValues;}
	vector<double>& getMarkers() {return markers;}

	const vector<long int>& getIntValues() const {return intValues;}
	const vector<double>& getDblValues() const {return dblValues;}
	const vector<double>& getMarkers() const {return markers;}

	void write(ostream &oFile, char sep='\t') const;
	void writeBin(ostream &oFile) const;
	void writeBinFloat(ostream &oFile) const;

	string toString(char sep='\t') const;

	void load(const Utils::Serialize::buffer_t &buffer);
	Utils::Serialize::buffer_t save() const;

public:
	bool changed, evaluated;
	double prior, likelihood, posterior;

private:
	std::vector<long int> intValues;
	std::vector<double> dblValues;
	std::vector<double> markers;

private:

	void copy(const Sample &toCopy);

	// Serialization
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const;

	template<class Archive>
	void load(Archive & ar, const unsigned int version);
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

} // namespace Sampler


#endif /* SAMPLE_H_ */
