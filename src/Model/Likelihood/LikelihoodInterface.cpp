//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * LikelihoodProcessor.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "LikelihoodInterface.h"

#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include <boost/math/distributions/detail/derived_accessors.hpp>
#include <boost/math/distributions/normal.hpp>
#include "cmath"

namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace Sampler { class Sample; }

namespace StatisticalModel {
namespace Likelihood {

LikelihoodInterface::LikelihoodInterface() : isLogLH(false), isUpdatableLH(false){
}

LikelihoodInterface::~LikelihoodInterface(){
}

size_t LikelihoodInterface::stateSize() const {
	cerr << "LikelihoodInterface::stateSize() not defined." << endl;
	abort();
	return 0;
}

void LikelihoodInterface::setStateLH(const char* aState) {
	cerr << "LikelihoodInterface::setStateLH(void* aState) not defined." << endl;
	abort();
}

void LikelihoodInterface::getStateLH(char* aState) const {
	cerr << "LikelihoodInterface::getStateLH(void* aState) not defined." << endl;
	abort();
}

double LikelihoodInterface::update(const vector<size_t>& pIndices, const Sampler::Sample& sample) {
	cerr << "double LikelihoodInterface::update(const vector<int>& pIndices, const Sampler::Sample& sample) not defined." << endl;
	abort();
	return processLikelihood(sample);
}

double LikelihoodInterface::processLikelihoodSpecificPrior(const Sampler::Sample &sample) {
	if(isLogLH) {
		return 0.;
	} else {
		return 1.;
	}
}

void LikelihoodInterface::initCustomizedLog(const std::string &fnPrefix, const std::string &fnSuffix, const std::vector<std::streamoff> &offsets) {
}

std::vector<std::streamoff> LikelihoodInterface::getCustomizedLogSize() const {
	std::vector<std::streamoff> dummyVec;
	return dummyVec;
}

void LikelihoodInterface::writeCustomizedLog(const Sampler::SampleUtils::vecPairItSample_t &iterSamples){

}

DAG::Scheduler::BaseScheduler* LikelihoodInterface::getScheduler() const {
	return NULL;
}

std::vector<DAG::BaseNode*> LikelihoodInterface::defineDAGNodeToCompute(const Sample& sample) {
	std::cerr << "std::vector<DAG::BaseNode*> LikelihoodInterface::defineDAGNodeToCompute(const Sample& sample) not defined." << endl;
	assert(false);
	return std::vector<DAG::BaseNode*>();
}

const std::vector<std::string>& LikelihoodInterface::getMarkerNames() const {
	return markerNames;
}

const std::vector<double>& LikelihoodInterface::getMarkers() const {
	return markers;
}

bool LikelihoodInterface::isLog() const {
	return isLogLH;
}

bool LikelihoodInterface::isUpdatable() const {
	return isUpdatableLH;
}

} // namespace Likelihood
} // namespace StatisticalModel
