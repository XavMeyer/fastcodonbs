//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelilhoodFactory.h
 *
 * @date May 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef LIKELILHOODFACTORY_H_
#define LIKELILHOODFACTORY_H_

#include <stddef.h>
#include <string>

#include "Model/Likelihood/BranchSiteREL/BranchSiteRELik.h"
#include "Model/Likelihood/SelPositive/Light/SelPositive.h"
#include "LikelihoodInterface.h"
#include "Likelihoods.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"
#include "Model/Model.h"

namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }

namespace StatisticalModel {
namespace Likelihood {

class LikelihoodFactory {
public:
	LikelihoodFactory();
	~LikelihoodFactory();

	static LikelihoodInterface::sharedPtr_t createSelPositiveBase(const bool isH1, const size_t aFGBranch, const size_t aNThread, const std::string &aFileAlign, const std::string &aFileTree);
	static LikelihoodInterface::sharedPtr_t createSelPositiveLight(const bool isH1, const bool aUseCompression, const long int aFGBranch, const size_t aNThread, const std::string &aFileAlign, const std::string &aFileTree);
	static LikelihoodInterface::sharedPtr_t createSelPositiveLight(const bool isH1, const bool aUseCompression, const long int aFGBranch, const size_t aNThread,
																   const PositiveSelection::Light::nuclModel_t aNuclModel, const PositiveSelection::Light::codonFreq_t aCFreqType,
																   const std::string &aFileAlign, const std::string &aFileTree, const PositiveSelection::Light::scalingType_t scalingType=PositiveSelection::Light::GLOBAL);


	static LikelihoodInterface::sharedPtr_t createBranchSiteREL(const bool aUseCompression, const int aIBranch, const size_t aNThread,
																const BranchSiteREL::nuclModel_t aNuclModel, const BranchSiteREL::codonFreq_t aCFreqType,
																const std::string &aFileAlign, const std::string &aFileTree);
																


};

} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* LIKELILHOODFACTORY_H_ */
