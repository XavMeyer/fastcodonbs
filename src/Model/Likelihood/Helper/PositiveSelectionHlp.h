//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PositiveSelectionHlp.h
 *
 * @date May 15, 2015
 * @author meyerx
 * @brief
 */
#ifndef POSITIVESELECTIONHLP_H_
#define POSITIVESELECTIONHLP_H_

#include <stddef.h>

#include "../Likelihoods.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "HelperInterface.h"
#include "Sampler/Samples/Sample.h"

class RNG;
namespace StatisticalModel { class Model; }
namespace StatisticalModel { class Parameters; }

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

template<typename SelPositive>
class PositiveSelectionHlp: public HelperInterface {
public:
	PositiveSelectionHlp(SelPositive *ptrSP);
	~PositiveSelectionHlp();

	void defineParameters(Parameters &params) const;
	Sample defineInitSample(const Parameters &params) const;
	void printSample(const Parameters &params, const Sample &sample, const char sep) const;
	std::string sampleToString(const Parameters &params, const Sample &sample, const char sep) const;

private:
	SelPositive *ptrLik;

	void defineNucleotideSubstParameters(Parameters &params, const bool reflect) const;

	void defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const;

};

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* POSITIVESELECTIONHLP_H_ */
