//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchSiteLik.h
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#ifndef BRANCHSITELIK_H_
#define BRANCHSITELIK_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <stddef.h>
#include <fstream>
#include <iostream>

#include "Model/Likelihood/BranchSiteREL/Nodes/Types.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriorityII/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Nodes/IncNodes.h"
#include "SampleWrapper.h"
#include "TreeNodeREL.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/CodonFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/Definitions/Codons.h"
#include "Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/SharedMatrixFactories.h"
#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace MolecularEvolution { namespace MatrixUtils { class SingleOmegaMF; } }
namespace Sampler { class Sample; }
namespace boost { namespace accumulators { namespace tag { struct mean; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class FinalResultNode;
class SampleWrapper;

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;
namespace MU = ::MolecularEvolution::MatrixUtils;

typedef ::MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;
typedef DL::CodonFrequencies::codonFrequencies_t codonFreq_t;

class BranchSiteRELik: public LikelihoodInterface {
public:
	BranchSiteRELik(const bool aUseCompressioncon, const int aIBranch, const size_t aNThread,
			 	 	const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
			 	 	const std::string &aFileAlign, const std::string &aFileTree);
	~BranchSiteRELik();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;
	int getFGBranch() const;
	nuclModel_t getNucleotideModel() const;
	codonFreq_t getCodonFrequencyType() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	bool isUsingCompression() const;
	size_t getNThread() const;
	std::string getAlignFile() const;
	std::string getTreeFile() const;


	std::string getNewickString() const;
	void printDAG() const;
	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

private:
	bool first, useCompression;
	int iBranch;
	size_t fgInternalNodeId, nThread, nSite;
	const nuclModel_t nuclModel;
	const codonFreq_t cFreqType;
	std::string fileAlign, fileTree;

	/* Input objects */
	/*DL::FastaReader fr;
	DL::MSA msa;*/
	DL::NewickParser np;
	const DL::TreeNode &newickRoot;
	TreeNodeREL rootREL;
	DL::Utils::Frequencies frequencies;

	/* Internal tree */
	const TreeNodeREL* fgNodeREL;
	std::vector<TreeNodeREL*> branchPtr;
	std::vector<size_t> internalBranches;

	/* DAG */
	FinalResultNode *rootDAG;
	DAG::Scheduler::BaseScheduler *scheduler;

	MU::SharedMatrixFactories<MU::SingleOmegaMF> sharedMF;

	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accComp_t;
	accComp_t accComp, accTotal;

	//! Prepare DAG
	void prepareDAG();
	//! Build the internal tree and the DAG
	void createTreeAndDAG();
	//! Build a SubDAG according to the REL tree
	void createSubDAG(const std::vector<size_t> &subSites, TreeNodeREL *node, EdgeNode *nodeDAG, DL::MSA &msa);

	//! Create the interal tree structure
	void createTree(const DL::TreeNode &newickNode, TreeNodeREL *node);
	//! Add the DAG Nodes related to a branch
	void addBranchNodes(TreeNodeREL *node);
	//! Add the DAG Nodes related to a branch omega class
	void addBranchOmegaClass(omegaClass_t aOmegaClass, TreeNodeREL *node);
	//! Labelize the internal branches
	void labelizeInternalBranches(const DL::TreeNode &newickNode);

	// Setters for the samples
	void setSample(const SampleWrapper &sw);
	void setBranch(const size_t iB, const SampleWrapper &sw);

	std::string getCompressionReport() const;
};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BRANCHSITELIK_H_ */
