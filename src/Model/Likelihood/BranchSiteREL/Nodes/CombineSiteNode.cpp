//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.cpp
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#include "CombineSiteNode.h"

#include "Model/Likelihood/BranchSiteREL/Nodes/RootNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

CombineSiteNode::CombineSiteNode(const std::vector<size_t> &aSites) :
		DAG::BaseNode(), sites(aSites), index(sites.size(), 0) {
	rootNode = NULL;
}

CombineSiteNode::~CombineSiteNode() {
}

TI_TYPE CombineSiteNode::getLikelihood() const {
	return likelihood;
}

void CombineSiteNode::doProcessing() {

	// LOG_LIKELIHOOD
	likelihood = 0.;
	const TI_EigenVector_t &siteLik = rootNode->getLikelihood();
	for(size_t iS=0; iS<sites.size(); ++iS) {

		// Conbine likelihood at site level
		likelihood += log(siteLik(index[iS]));
	}

}

bool CombineSiteNode::processSignal(BaseNode* aChild) {

	return true;
}

void CombineSiteNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootNode)){
		rootNode = dynamic_cast<RootNode*>(aChild);
		linkLikSites(rootNode);
	}
}

void CombineSiteNode::linkLikSites(RootNode *node) {

	// Get node omegaClass
	for(size_t iS=0; iS<sites.size(); ++iS){
		int idxSLik = node->getLikelihoodIndex(sites[iS]);
		if(idxSLik >= 0) {
			index[iS] = idxSLik;
		} else {
			cerr << "Error : void CombineSiteNode::linkLikSites(RootNode *node);" << std::endl;
			cerr << "RootNode " << node->getId() << " doesn't have the required likelihood for site " << sites[iS] << std::endl;
			abort();
		}
	}

}


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
