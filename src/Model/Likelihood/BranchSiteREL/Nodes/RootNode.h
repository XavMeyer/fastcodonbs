//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootNode.h
 *
 * @date Feb 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef ROOTNODE_BS_REL_H_
#define ROOTNODE_BS_REL_H_

#include <stddef.h>

#include "CPVNode.h"
#include "LeafNode.h"
#include "ParentNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class RootNode: public ParentNode {
public:
	RootNode(const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies,
			 const bool compress = false);
	~RootNode();

	int getLikelihoodIndex(const size_t aSitePos) const;
	const TI_EigenVector_t& getLikelihood() const;

private:

	TI_EigenVector_t likelihood;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* ROOTNODE_BS_REL_H_ */
