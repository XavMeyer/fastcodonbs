//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.h
 *
 * @date June 30, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXNODEREL_H_
#define MATRIXNODEREL_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"
#include "Utils/MolecularEvolution/MatrixFactory/SharedMatrixFactories.h"

namespace MolecularEvolution { namespace MatrixUtils { class SingleOmegaMF; } }
namespace MolecularEvolution { namespace MatrixUtils { template <class matrixType> class SharedMatrixFactories; } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;
namespace MU = ::MolecularEvolution::MatrixUtils;

class MatrixNode: public DAG::BaseNode {

public:
	MatrixNode(const MU::nuclModel_t aNuclModel, const omegaClass_t aOmega, DL_Utils::Frequencies &aFrequencies,
			MU::SharedMatrixFactories<MU::SingleOmegaMF> &aSharedMF);
	~MatrixNode();

	void setThetas(const std::vector<TI_TYPE> &aThetas);
	void setKappa(const TI_TYPE aKappa);
	void setOmega(const TI_TYPE aW);

	MU::nuclModel_t getNucleotideModel() const;
	omegaClass_t getOmegaClass() const;
	const TI_TYPE getMatrixQScaling() const;

	const TI_EigenSquareMatrix_t& getScaledEigenVectors() const;
	const TI_EigenVector_t& getEigenValues() const;

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

private:

	MU::nuclModel_t nuclModel;
	omegaClass_t omegaClass;
	// Coefficients are [Thethas, omega]
	std::vector<TI_TYPE> coefficients;
	DL_Utils::Frequencies &frequencies;

	TI_TYPE matrixScaling;
	TI_EigenVector_t D;
	TI_EigenSquareMatrix_t scaledV;

	MU::SharedMatrixFactories<MU::SingleOmegaMF> &sharedMF;

	void doProcessing();
	bool processSignal(BaseNode* aChild);
	void comprEIG(TI_EigenSquareMatrix_t &A);

};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MATRIXNODEREL_H_ */
