//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IncNodes.h
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#ifndef INCNODES_BRANCHESITEREL_H_
#define INCNODES_BRANCHESITEREL_H_

#include "BranchMatrixNode.h"
#include "CombinedBMNode.h"
#include "CombineSiteNode.h"
#include "CPV.h"
#include "CPVNode.h"
#include "EdgeNode.h"
#include "FinalResultNode.h"
#include "MatrixNode.h"
#include "MatrixScalingNode.h"
#include "ParentNode.h"
#include "RootNode.h"


#endif /* INCNODES_BRANCHESITEREL_H_ */
