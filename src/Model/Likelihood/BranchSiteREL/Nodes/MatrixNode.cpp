//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.cpp
 *
 * @date June 30, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixNode.h"

#include <assert.h>

#include "Model/Likelihood/BranchSiteREL/Nodes/Types.h"

namespace MolecularEvolution { namespace MatrixUtils { class SingleOmegaMF; } }
namespace MolecularEvolution { namespace MatrixUtils { template <class matrixType> class SharedMatrixFactories; } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

MatrixNode::MatrixNode(const MU::nuclModel_t aNuclModel, const omegaClass_t aOmega, DL_Utils::Frequencies &aFrequencies,
		MU::SharedMatrixFactories<MU::SingleOmegaMF> &aSharedMF) :
		DAG::BaseNode(), nuclModel(aNuclModel), omegaClass(aOmega), frequencies(aFrequencies),
		D(frequencies.size()), //V(frequencies.size(),frequencies.size()),
		scaledV(frequencies.size(), frequencies.size()), sharedMF(aSharedMF) {

	std::pair<int, MU::SingleOmegaMF*> matrix = sharedMF.getMatrix();
	coefficients.assign(matrix.second->getNBCoefficients(), 0.);
	sharedMF.releaseMatrix(matrix);
	matrixScaling = 0;
}

MatrixNode::~MatrixNode() {
}

void MatrixNode::setThetas(const std::vector<TI_TYPE> &aThetas) {
	assert(nuclModel == MU::GTR);
	for(size_t iT=0; iT < aThetas.size(); ++iT) {
		if(aThetas[iT] != coefficients[iT]) {
			coefficients[iT] = aThetas[iT];
			BaseNode::updated(); // signal that the node must be recomputed
		}
	}
}

void MatrixNode::setKappa(const TI_TYPE aKappa) {
	assert(nuclModel == MU::K80);
	if(aKappa != coefficients[0]) {
		coefficients[0] = aKappa;
		BaseNode::updated(); // signal that the node must be recomputed
	}
}

void MatrixNode::setOmega(const TI_TYPE aW) {
	if(aW != coefficients.back()) {
		coefficients.back() = aW;
		BaseNode::updated(); // signal that the node must be recomputed
	}
}

omegaClass_t MatrixNode::getOmegaClass() const {
	return omegaClass;
}

const TI_TYPE MatrixNode::getMatrixQScaling() const {
	return matrixScaling;
}

const TI_EigenSquareMatrix_t& MatrixNode::getScaledEigenVectors() const {
	return scaledV;
}


const TI_EigenVector_t& MatrixNode::getEigenValues() const {
	return D;
}

size_t MatrixNode::serializedSize() const {
	/*const size_t N = soMF.getMatrixDimension();
	return (1+soMF.getNBCoefficients()+N+N*N)*sizeof(double);*/
	return 0;
}

void MatrixNode::serializeToBuffer(char *buffer) const {
	/*double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy Q scaling
	tmp[cnt] = matrixScaling; cnt++;

	// Copy coefficients
	for(size_t iC=0; iC<soMF.getNBCoefficients(); ++iC) {
		tmp[cnt] = coefficients[iC]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		tmp[cnt+i] = *(D.data() + i);
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		tmp[cnt+i] = *(scaledV.data() + i);
	}
	cnt += scaledV.size();*/
}

void MatrixNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	/*bool hasChanged = false;

	size_t cnt = 0;
	// Copy Q scaling
	matrixScaling = tmp[cnt]; cnt++;

	// Copy coefficients
	for(size_t iC=0; iC<soMF.getNBCoefficients(); ++iC) {
		hasChanged = hasChanged || coefficients[iC] != tmp[cnt];
		coefficients[iC] = tmp[cnt]; cnt++;
	}

	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		*(D.data() + i) = tmp[cnt+i];
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		*(scaledV.data() + i) = tmp[cnt+i];
	}
	cnt += scaledV.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}*/
}

void MatrixNode::doProcessing() {

	// shortcut to sqrt frequencies
	const Eigen::VectorXd &sqrtPi = frequencies.getSqrtEigen();
	const Eigen::VectorXd &sqrtInvPi = frequencies.getSqrtInvEigen();

	// Fill the matrix given the new coefficient
	//MolecularEvolution::MatrixUtils::SingleOmegaMF soMF(nuclModel);

	size_t myMatrix=0;

	std::pair<int, MU::SingleOmegaMF*> matrix = sharedMF.getMatrix();

	matrix.second->setFrequencies(frequencies.getStd());
	matrix.second->fillMatrix(coefficients);
	matrixScaling = matrix.second->getScalingFactor();

	// Compute
	// Prepare S
	const size_t N = matrix.second->getMatrixDimension();
	Eigen::Map< TI_EigenMatrixDyn_t > mappedMat(matrix.second->getMatrix().data(), N, N);
	TI_EigenSquareMatrix_t Q = mappedMat;

	// Compute
	/*
	 * Q = S * PI
	 * S = Q*PI^(-1)
	 *
	 * A = PI^(1/2)*S*PI^(1/2) = PI^(1/2)*Q*PI^(-1)*PI^(1/2) = PI^(1/2)*Q*PI^(-1/2)
	 */
	TI_EigenSquareMatrix_t A = sqrtPi.asDiagonal()*Q*sqrtInvPi.asDiagonal();

	comprEIG(A);

	sharedMF.releaseMatrix(matrix);
}

bool MatrixNode::processSignal(BaseNode* aChild) {
	return true;
}


void MatrixNode::comprEIG(TI_EigenSquareMatrix_t &A) {
	size_t nValidF = frequencies.getNValidFreq();
	const std::vector<bool> &validF = frequencies.getValidFreq();
	const Eigen::VectorXd &sqrtPi = frequencies.getSqrtEigen();

	Eigen::Matrix<TI_TYPE, Eigen::Dynamic, Eigen::Dynamic> compactA(nValidF, nValidF);

	compactA.setZero();

	// Compress
	for(size_t iC=0, jC=0; iC<(size_t)A.cols(); ++iC) {
		if(validF[iC]) {
			for(size_t iR=0, jR=0; iR<(size_t)A.rows(); ++iR) {
				if(validF[iR]) {
					compactA(jR, jC) = A(iR, iC);
					jR++;
				} // end if
			} // end for
			jC++;
		} // end if
	} // end for

	// Do EIG
	Eigen::SelfAdjointEigenSolver< Eigen::Matrix<TI_TYPE, Eigen::Dynamic, Eigen::Dynamic> > es;
	es.compute(compactA);

	//TI_EigenSquareMatrix_t V(A.rows(), A.cols());
	//V.setZero();

	// Decompress
	for(size_t iC=0, jC=0; iC<(size_t)A.cols(); ++iC) {
		if(validF[iC]) {
			D(iC) = es.eigenvalues()(jC);
			for(size_t iR=0, jR=0; iR<(size_t)A.rows(); ++iR) {
				if(validF[iR]) {
					//V(iR, iC) = es.eigenvectors()(jR, jC);
					scaledV(iR, iC) = sqrtPi(iR)*es.eigenvectors()(jR, jC);
					jR++;
				} else {
					//V(iR, iC) = 0.;
					scaledV(iR, iC) = 0.;
				} // end if
			} // end for
			jC++;
		} else {
			D(iC) = 0.;
			for(size_t iR=0; iR<(size_t)A.rows(); ++iR) {
				//V(iR, iC) = iC == iR ? 1.0 : 0.0;
				scaledV(iR, iC) = iC == iR ? sqrtPi(iR) : 0.0;
			}
		} // end if
	} // end for

}

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
