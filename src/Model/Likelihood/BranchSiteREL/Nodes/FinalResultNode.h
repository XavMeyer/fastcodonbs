//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file FinalResultNode.h
 *
 * @date Mar 4, 2015
 * @author meyerx
 * @brief
 */
#ifndef FINALRESULTNODE_BS_REL_H_
#define FINALRESULTNODE_BS_REL_H_

#include "DAG/Node/Base/BaseNode.h"
#include <stddef.h>
#include <iomanip>
#include <vector>

#include "CombineSiteNode.h"
#include "DAG/Node/Base/BaseNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class FinalResultNode: public DAG::BaseNode {
public:
	FinalResultNode();
	~FinalResultNode();

	TI_TYPE getLikelihood() const;

private:
	TI_TYPE likelihood;
	std::vector<CombineSiteNode*> csNodes;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* FINALRESULTNODE_BS_REL_H_ */
