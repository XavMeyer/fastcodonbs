//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombinedBMNode.h
 *
 * @date Jun 30, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMBINEDBMNODE_H_
#define COMBINEDBMNODE_H_

#include <stddef.h>
#include <vector>

#include "BranchMatrixNode.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "MatrixScalingNode.h"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class MatrixScalingNode;

class CombinedBMNode :  public DAG::BaseNode {
public:
	CombinedBMNode(DL_Utils::Frequencies &aFrequencies);
	~CombinedBMNode();

	//void setProportions(const double aP0, const double aP1);

	const TI_EigenSquareMatrix_t& getPbs() const;
	/*double getPNegative() const;
	double getPNeutral() const;
	double getPPositive() const;*/

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

private:
	//double p0, p1, pNegative, pNeutral, pPositive;

	DL_Utils::Frequencies &frequencies;
	TI_EigenSquareMatrix_t Pbs;
	std::vector<BranchMatrixNode *> BMNodes;

	MatrixScalingNode *scalingNode;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);


};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINEDBMNODE_H_ */
