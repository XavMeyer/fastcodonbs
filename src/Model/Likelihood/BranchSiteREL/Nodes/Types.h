//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Jun 30, 2015
 * @author meyerx
 * @brief
 */
#ifndef TYPES_BRANCHSITE_REL_H_
#define TYPES_BRANCHSITE_REL_H_

#include <stddef.h>

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Sparse"

#if TI_USE_STAN
#include "stan/math.hpp"
#endif

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

enum NUMBER_OF_CLASS {N_CLASS=3};
enum OMEGA_CLASS {OMEGA_NEGATIVE=0, OMEGA_NEUTRAL=1, OMEGA_POSITIVE=2};
typedef OMEGA_CLASS omegaClass_t;

#define TI_NSYMBOLS 			61

//#define TI_USE_STAN 			0

#if TI_USE_STAN

	typedef Eigen::Matrix<stan::math::var, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenSquareMatrix_t;
	typedef Eigen::Matrix<stan::math::var, TI_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_EigenMatrix_t;
	typedef Eigen::Matrix<stan::math::var, Eigen::Dynamic, Eigen::Dynamic > TI_EigenMatrixDyn_t;
	typedef Eigen::Matrix<stan::math::var, Eigen::Dynamic, 1 > TI_EigenVector_t;
	typedef Eigen::Array<stan::math::var, Eigen::Dynamic, Eigen::Dynamic > TI_EigenArray_t;
	#define TI_TYPE					stan::math::var

	typedef Eigen::Matrix<stan::math::var, Eigen::Dynamic, 1 > TI_EigenEIGVector_t;
	typedef Eigen::Array<stan::math::var, Eigen::Dynamic, Eigen::Dynamic > TI_EigenEIGArray_t;
	typedef Eigen::Matrix<stan::math::var, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenEIGMatrix_t;

	typedef Eigen::SparseMatrix<stan::math::var> TI_EigenSparseMatrix_t;

#else

	typedef Eigen::Matrix<double, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenSquareMatrix_t;
	typedef Eigen::Matrix<double, TI_NSYMBOLS, Eigen::Dynamic, Eigen::ColMajor> TI_EigenMatrix_t;
	typedef Eigen::MatrixXd TI_EigenMatrixDyn_t;
	typedef Eigen::VectorXd TI_EigenVector_t;
	typedef Eigen::ArrayXd TI_EigenArray_t;
	#define TI_TYPE					double

	typedef Eigen::VectorXd TI_EigenEIGVector_t;
	typedef Eigen::ArrayXd TI_EigenEIGArray_t;
	typedef Eigen::Matrix<double, TI_NSYMBOLS, TI_NSYMBOLS, Eigen::ColMajor> TI_EigenEIGMatrix_t;

	typedef Eigen::SparseMatrix<double> TI_EigenSparseMatrix_t;

#endif



	static const double LOG_OF_2 = log(2.);


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TYPES_BRANCHSITE_REL_H_  */
