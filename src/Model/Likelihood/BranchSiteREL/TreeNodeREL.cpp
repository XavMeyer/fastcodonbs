//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNodeREL.cpp
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#include "Model/Likelihood/BranchSiteREL/TreeNodeREL.h"

#include "Model/Likelihood/BranchSiteREL/Nodes/MatrixScalingNode.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class BranchMatrixNode;
class CombinedBMNode;
class MatrixNode;

TreeNodeREL::TreeNodeREL(const MolecularEvolution::DataLoader::TreeNode &aNewickNode) :
		matrixNodes(N_CLASS, NULL), bmNodes(N_CLASS, NULL) {
	id = aNewickNode.getId();
	name = aNewickNode.getName();
	branchLength = aNewickNode.getLength();
	leaf = false;

	cbmNode = NULL;
	scalingNode = NULL;
}

TreeNodeREL::~TreeNodeREL() {
}

void TreeNodeREL::addChild(TreeNodeREL *child) {
	children.push_back(child);
}

TreeNodeREL* TreeNodeREL::getChild(size_t id) {
	return children[id];
}

const TreeNodeREL_children& TreeNodeREL::getChildren() const {
	return children;
}

const std::string& TreeNodeREL::getName() const {
	return name;
}

void TreeNodeREL::setLeaf(const bool aLeaf) {
	leaf = aLeaf;
}

bool TreeNodeREL::isLeaf() const {
	return leaf;
}

void TreeNodeREL::setOmegaNegative(const TI_TYPE aWNegative) {
	matrixNodes[OMEGA_NEGATIVE]->setOmega(aWNegative);
}

void TreeNodeREL::setOmegaNeutral(const TI_TYPE aWNeutral) {
	matrixNodes[OMEGA_NEUTRAL]->setOmega(aWNeutral);
}

void TreeNodeREL::setOmegaPositive(const TI_TYPE aWPositive) {
	matrixNodes[OMEGA_POSITIVE]->setOmega(aWPositive);
}

void TreeNodeREL::setKappa(const TI_TYPE aKappa) {
	for(size_t iN=0; iN<matrixNodes.size(); ++iN)  {
		matrixNodes[iN]->setKappa(aKappa);
	}
}

void TreeNodeREL::setThetas(const std::vector<TI_TYPE> &aThetas) {
	for(size_t iN=0; iN<matrixNodes.size(); ++iN)  {
		matrixNodes[iN]->setThetas(aThetas);
	}
}

void TreeNodeREL::setBranchLength(const TI_TYPE aBL) {
	if(branchLength != aBL) {
		branchLength = aBL;
		for(size_t iN=0; iN<bmNodes.size(); ++iN) {
			bmNodes[iN]->setBranchLength(aBL);
		}
	}
}

void TreeNodeREL::setP0(const TI_TYPE aP0) {
	scalingNode->setP0(aP0);
}

void TreeNodeREL::setP1(const TI_TYPE aP1) {
	scalingNode->setP1(aP1);
}

const std::string TreeNodeREL::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << name << " - " << branchLength;
	ss << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i]->id << "]" << children[i]->name;
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNodeREL::deleteChildren() {
	while(!children.empty()) {
		TreeNodeREL *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

std::string TreeNodeREL::buildNewick() const {
	std::string nwkStr;
	addNodeToNewick(nwkStr);
	return nwkStr;
}

void TreeNodeREL::addNodeToNewick(std::string &nwk) const {
	if(!children.empty()) {
		nwk.push_back('(');
		children[0]->addNodeToNewick(nwk);

		for(size_t iC=1; iC<children.size(); ++iC) {
			nwk.push_back(',');
			nwk.push_back(' ');
			children[iC]->addNodeToNewick(nwk);
		}
		nwk.push_back(')');
	}

	nwk.append(name);
	nwk.push_back(':');
	std::stringstream ss;
	ss << branchLength;
	nwk.append(ss.str());
}

void TreeNodeREL::addMatrixNode(MatrixNode *node) {
	matrixNodes[node->getOmegaClass()] = node;
}

MatrixNode* TreeNodeREL::getMatrixNode(omegaClass_t aOmegaClass) {
	return matrixNodes[aOmegaClass];
}

void TreeNodeREL::addBranchMatrixNode(BranchMatrixNode *node) {
	bmNodes[node->getOmegaClass()] = node;
}

BranchMatrixNode* TreeNodeREL::getBranchMatrixNode(omegaClass_t aOmegaClass) {
	return bmNodes[aOmegaClass];
}

void TreeNodeREL::setCombineBranchMatrixNode(CombinedBMNode *node) {
	cbmNode = node;
}

CombinedBMNode* TreeNodeREL::getCombineBranchMatrixNode() {
	return cbmNode;
}

void TreeNodeREL::setMatrixScalingNode(MatrixScalingNode *node) {
	scalingNode = node;
}

MatrixScalingNode* TreeNodeREL::getMatrixScalingNode() {
	return scalingNode;
}


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
