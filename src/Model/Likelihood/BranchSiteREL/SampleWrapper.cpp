//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.cpp
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#include <Model/Likelihood/BranchSiteREL/SampleWrapper.h>

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

const size_t SampleWrapper::N_KAPPA = 1;
const size_t SampleWrapper::N_THETA = 5;
const size_t SampleWrapper::K = 3;
const size_t SampleWrapper::N_PARAM_PER_BRANCH = 2*K;

SampleWrapper::SampleWrapper(const MU::nuclModel_t aNuclModel, const size_t aN_BRANCH, const std::vector<double> &sample) :
	N_BRANCH(aN_BRANCH), kappa(0.), branchParams(aN_BRANCH) {

	size_t nNucleotideParams;
	if(aNuclModel == MU::GTR) {
		nNucleotideParams = N_THETA;
		for(size_t iT=0; iT<N_THETA; iT++) {
			thetas.push_back(sample[iT]);
		}
	} else if(aNuclModel == MU::K80) {
		nNucleotideParams = N_KAPPA;
		kappa = sample[0];
	} else {
		std::cerr << "Error : SampleWrapper::SampleWrapper(const MU::nuclModel_t aNuclModel, const size_t aN_BRANCH, const std::vector<double> &sample);"<< std::endl;
		std::cerr << "Nucleotide model not supported."<< std::endl;
		abort();
	}
	//thetas.insert(thetas.begin(), sample.begin(), sample.begin()+N_THETA);

	for(size_t iB=0; iB<N_BRANCH; ++iB) {
		size_t offset = nNucleotideParams+iB*N_PARAM_PER_BRANCH;
		branchParams[iB].t = sample[offset+0];
		branchParams[iB].wNeg = sample[offset+1];
		branchParams[iB].wNeutral = sample[offset+2];
		branchParams[iB].wPos = sample[offset+3];
		branchParams[iB].p0 = sample[offset+4];
		branchParams[iB].p1 = sample[offset+5];
	}
}

SampleWrapper::~SampleWrapper() {
}

std::vector<TI_TYPE> SampleWrapper::getParameters() const {

	std::vector<TI_TYPE> vars;

	if(thetas.size() > 0 ) {
		for(size_t iT=0; iT<N_THETA; ++iT) {
			vars.push_back(thetas[iT]);
		}
	} else {
		vars.push_back(K);
	}

	for(size_t iB=0; iB<N_BRANCH; ++iB) {
		vars.push_back(branchParams[iB].t);
		vars.push_back(branchParams[iB].wNeg);
		vars.push_back(branchParams[iB].wNeutral);
		vars.push_back(branchParams[iB].wPos);
		vars.push_back(branchParams[iB].p0);
		vars.push_back(branchParams[iB].p1);
	}

	return vars;
}

std::string SampleWrapper::toString() const {
	std::stringstream ss;
	ss.precision(4);
	ss << std::fixed;

	for(size_t iT=0; iT<N_THETA; ++iT) {
		ss << "theta_" << iT << " = " << thetas[iT] << "\t";
	}

	for(size_t iB=0; iB<N_BRANCH; ++iB) {
		ss << "B" << iB << ".t = " << branchParams[iB].t << "\t";
		ss << "B" << iB << ".w0 = " << branchParams[iB].wNeg << "\t";
		ss << "B" << iB << ".w1 = " << branchParams[iB].wNeutral << "\t";
		ss << "B" << iB << ".w2 = " << branchParams[iB].wPos << "\t";
		ss << "B" << iB << ".p0 = " << branchParams[iB].p0 << "\t";
		ss << "B" << iB << ".p1 = " << branchParams[iB].p1 << "\t";
	}
	return ss.str();

}


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
