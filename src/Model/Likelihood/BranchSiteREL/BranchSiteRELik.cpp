//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchSiteLik.cpp
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#include "BranchSiteRELik.h"

#include <math.h>

#include "Model/Likelihood/BranchSiteREL/Nodes/FinalResultNode.h"
#include "Model/Likelihood/BranchSiteREL/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/BranchSiteREL/SampleWrapper.h"
#include "Model/Likelihood/BranchSiteREL/TreeNodeREL.h"
#include "DAG/Scheduler/BaseScheduler.h"
#include "Parallel/Manager/MpiManager.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "cmath"
#include "new"

namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace Sampler { class Sample; }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class BranchMatrixNode;
class CPVNode;
class CombineSiteNode;
class CombinedBMNode;
class EdgeNode;
class LeafNode;
class MatrixNode;
class RootNode;

BranchSiteRELik::BranchSiteRELik(const bool aUseCompressioncon, const int aIBranch, const size_t aNThread,
								 const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
								 const std::string &aFileAlign, const std::string &aFileTree) :
		LikelihoodInterface(), useCompression(aUseCompressioncon), iBranch(aIBranch), nThread(aNThread),
		nuclModel(aNuclModel), cFreqType(aCFreqType), fileAlign(aFileAlign), fileTree(aFileTree),
		//fr(fileAlign), msa(DL::MSA::CODON, fr.getAlignments(), false),*/
		np(fileTree), newickRoot(np.getRoot()), rootREL(newickRoot),
		frequencies(MD::Codons::NB_CODONS_WO_STOP), sharedMF(nThread) {

	/*for(size_t i=0; i < frequencies.getStd().size(); ++i) {
		std::cout << frequencies.getStd()[i] << std::endl;
	}
	abort();*/

	first = true;
	isLogLH = true;
	#if TI_USE_STAN
		isUpdatableLH = false;
	#else
		isUpdatableLH = true;
	#endif
	fgInternalNodeId = 0;
	fgNodeREL = NULL;

	prepareDAG();
	createTreeAndDAG();


	if(iBranch >= (int)branchPtr.size()) {
		std::cerr << "Error in constructor : BranchSiteRELik::BranchSiteRELik(..)" << std::endl;
		std::cerr << "There are " << branchPtr.size() << " branches in the three therefore " << iBranch << " cannot be specified as not positively evolving." << std::endl;
		abort();
	}


	if(nThread > 1) {
		Eigen::initParallel();
		scheduler = new DAG::Scheduler::Static::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	for(size_t i=0; i<branchPtr.size(); ++i) {
		std::stringstream ss;
		ss.str("");
		ss << "B" << i <<  ".Pn";
		markerNames.push_back(ss.str());
		ss.str("");
		ss << "B" << i <<  ".Pe";
		markerNames.push_back(ss.str());
		ss.str("");
		ss << "B" << i <<  ".Pp";
		markerNames.push_back(ss.str());
	}
	markers.resize(markerNames.size());

}

BranchSiteRELik::~BranchSiteRELik() {
	rootREL.deleteChildren();
	rootDAG->deleteChildren();
	delete rootDAG;
	delete scheduler;
}


double BranchSiteRELik::processLikelihood(const Sampler::Sample &sample) {


#if TI_USE_STAN
	std::cout << "Here 1 !!" << std::endl;
	stan::math::start_nested();
#endif

	// Update the values
	SampleWrapper sw(nuclModel, branchPtr.size(), sample.getDblValues());
	setSample(sw);

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	scheduler->process();

	// return the likelihood
	TI_TYPE lik = rootDAG->getLikelihood();

#if TI_USE_STAN

	size_t cnt = 0;
	for(size_t i=0; i<branchPtr.size(); ++i) {
		markers[cnt] = sw.branchParams[i].p0.val(); cnt++;
		markers[cnt] = (sw.branchParams[i].p1*(1.-sw.branchParams[i].p0)).val(); cnt++;
		markers[cnt] = ((1.-sw.branchParams[i].p0)*(1.-sw.branchParams[i].p1)).val(); cnt++;
	}

	std::vector<double> gradient;
	std::vector<stan::math::var> params(sw.getParameters());
	//std::vector<stan::math::var> tmpParams(1, params.front());
	//lik.grad(params, gradient);

	double likVal = lik.val();
	//std::cout << likVal << std::endl;
	//stan::math::grad(lik.vi_);
	gradient.clear();
	lik.grad(params, gradient);

	stan::math::recover_memory_nested();
	//getchar();

	/*std::cout << "Gradient : ";
	for(size_t i=0; i<gradient.size(); ++i) {
		std::cout << gradient[i] << "\t";
	}
	std::cout << std::endl;*/


	return likVal;
#else
	size_t cnt = 0;
	for(size_t i=0; i<branchPtr.size(); ++i) {
		markers[cnt] = sw.branchParams[i].p0; cnt++;
		markers[cnt] = sw.branchParams[i].p1*(1.-sw.branchParams[i].p0); cnt++;
		markers[cnt] = (1.-sw.branchParams[i].p0)*(1.-sw.branchParams[i].p1); cnt++;
	}


	return lik;
#endif
}


std::string BranchSiteRELik::getName(char sep) const {
	std::stringstream ss;
	ss << "Branch" << sep << "site" << sep << "REL";
	return ss.str();
}
size_t BranchSiteRELik::getNBranch() const {
	return branchPtr.size();
}

int BranchSiteRELik::getFGBranch() const {
	return iBranch;
}

nuclModel_t BranchSiteRELik::getNucleotideModel() const {
	return nuclModel;
}

codonFreq_t BranchSiteRELik::getCodonFrequencyType() const {
	return cFreqType;
}

size_t BranchSiteRELik::stateSize() const {
	return branchPtr.size()*branchPtr.front()->getCombineBranchMatrixNode()->serializedSize();
}

void BranchSiteRELik::setStateLH(const char* aState) {
	size_t cbmStateSize = branchPtr.front()->getCombineBranchMatrixNode()->serializedSize();
	for(size_t iB=0; iB<branchPtr.size(); ++iB) {
		branchPtr[iB]->getCombineBranchMatrixNode()->serializeFromBuffer(aState+cbmStateSize*iB);
	}
}

void BranchSiteRELik::getStateLH(char* aState) const {
	size_t cbmStateSize = branchPtr.front()->getCombineBranchMatrixNode()->serializedSize();
	for(size_t iB=0; iB<branchPtr.size(); ++iB) {
		branchPtr[iB]->getCombineBranchMatrixNode()->serializeToBuffer(aState+cbmStateSize*iB);
	}
}

double BranchSiteRELik::update(const vector<size_t>& pIndices, const Sample& sample) {

	SampleWrapper sw(nuclModel, branchPtr.size(), sample.getDblValues());
	setSample(sw);

	// Reset the DAG
	if(!first) { // Partial
		if(!scheduler->resetPartial()) {
			std::cerr << "Reset partial did not found any change for parameters : " << std::endl;
			for(size_t i=0; i< pIndices.size(); ++i) {
				std::cerr << "[" << pIndices[i] << "] = "  << sample.getDoubleParameter(i) << "\t";
			}
			std::cerr << std::endl;
		}
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	// Process the tree
	scheduler->process();

	TI_TYPE lik = rootDAG->getLikelihood();

#if TI_USE_STAN

	size_t cnt = 0;
	for(size_t i=0; i<branchPtr.size(); ++i) {
		markers[cnt] = sw.branchParams[i].p0.val(); cnt++;
		markers[cnt] = (sw.branchParams[i].p1*(1.-sw.branchParams[i].p0)).val(); cnt++;
		markers[cnt] = ((1.-sw.branchParams[i].p0)*(1.-sw.branchParams[i].p1)).val(); cnt++;
	}


	std::vector<double> gradient;
	std::vector<TI_TYPE> params(sw.getParameters());
	//std::vector<stan::math::var> tmpParams(1, params.front());
	//lik.grad(params, gradient);

	double likVal = lik.val();
	//std::cout << likVal << std::endl;
	stan::math::grad(lik.vi_);

	stan::math::recover_memory_nested();
	//getchar();

	/*std::cout << "Gradient : ";
	for(size_t i=0; i<gradient.size(); ++i) {
		std::cout << gradient[i] << "\t";
	}
	std::cout << std::endl;*/

	return likVal;
#else

	size_t cnt = 0;
	for(size_t i=0; i<branchPtr.size(); ++i) {
		markers[cnt] = sw.branchParams[i].p0; cnt++;
		markers[cnt] = sw.branchParams[i].p1*(1.-sw.branchParams[i].p0); cnt++;
		markers[cnt] = (1.-sw.branchParams[i].p0)*(1.-sw.branchParams[i].p1); cnt++;
	}

	return lik;
#endif

}

bool BranchSiteRELik::isUsingCompression() const {
	return useCompression;
}

size_t BranchSiteRELik::getNThread() const {
		return nThread;
}

std::string BranchSiteRELik::getAlignFile() const {
	return fileAlign;
}

std::string BranchSiteRELik::getTreeFile() const {
	return fileTree;
}

std::string BranchSiteRELik::getNewickString() const {
	return rootREL.buildNewick();
}

void BranchSiteRELik::printDAG() const {
	std::cout << "**************************************************************************" << std::endl;
	std::cout << rootDAG->subtreeToString() << std::endl;
	std::cout << "**************************************************************************" << std::endl;
}

DAG::Scheduler::BaseScheduler* BranchSiteRELik::getScheduler() const {
	return scheduler;
}



void BranchSiteRELik::prepareDAG() {
	rootDAG = new FinalResultNode();
}

void BranchSiteRELik::createTreeAndDAG() {

	DL::FastaReader fr(fileAlign);
	DL::MSA msa(DL::MSA::CODON, fr.getAlignments(), false);

	for(size_t i=0; i<nThread; ++i) {
		sharedMF.addMatrix(new MU::SingleOmegaMF(nuclModel));
	}

	//DL::CompressedAlignements compressedAligns(msa, false, false);
	frequencies.set(DL::CodonFrequencies(msa).getCodonFrequency(cFreqType));

	if(useCompression) {
		msa.sortByDistance();
	}
	nSite = msa.getNValidSite();

	// Labelize internal branches
	labelizeInternalBranches(newickRoot);

	// Create tree
	createTree(newickRoot, &rootREL);

	// Decompose the DAG in function of the NB of thread.
	size_t nSubDAG = 1;
	if(isUpdatableLH && nThread > 1){
		nSubDAG = std::min(nThread, nSite);
	}
	size_t sizeSubDag = ceil(static_cast<double>(nSite) / static_cast<double>(nSubDAG));
	for(size_t iSD=0; iSD<nSubDAG; ++iSD) {
		// Init subSites
		size_t start = iSD*sizeSubDag;
		size_t end = std::min((iSD+1)*sizeSubDag, nSite);
		//std::cout << nSite << " -- " << start << " :: " << end << std::endl;
		std::vector<size_t> subSites;
		for(size_t iSS=start; iSS<end; ++iSS) {
			subSites.push_back(iSS);
		}

		// Create site combiner
		CombineSiteNode* ptrCSN = new CombineSiteNode(subSites);

		// Create root
		RootNode *ptrR = new RootNode(subSites, frequencies, useCompression);
		createSubDAG(subSites, &rootREL, ptrR, msa); // Create sub DAG
		if(useCompression) {
			ptrR->compressNode();
			accComp(ptrR->getNCompressed());
			accTotal(end-start);
		}

		// Add them to the CombineSiteNode
		ptrCSN->addChild(ptrR);

		// Add this CombineSiteNode to the DAG root
		rootDAG->addChild(ptrCSN);
	}

	if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << getCompressionReport();
		std::cout << "**************************************" << std::endl;
	}

}

/**
 * @param newickNode a new from DataLoader::TreeNode. i.e. the ones returned from NewickParser
 * @param node a node in the internal Branch REL tree
 */
void BranchSiteRELik::createTree(const DL::TreeNode &newickNode, TreeNodeREL *node) {

	if(newickNode.children.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		for(size_t i=0; i<newickNode.getChildren().size(); ++i){
			TreeNodeREL *childNode = new TreeNodeREL(newickNode.getChildren()[i]);

			addBranchNodes(childNode); // set matrix, branchMatrix, combineBranchMatrix nodes

			branchPtr.push_back(childNode); // Keep pointer to branch for t update

			createTree(newickNode.getChildren()[i], childNode);
			node->addChild(childNode);
		}
	} else { // Else it is a leaf node
		node->setLeaf(true);
	}
}

/**
 * @param node a node in the internal Branch REL tree
 */
void BranchSiteRELik::addBranchNodes(TreeNodeREL *node) {


	addBranchOmegaClass(OMEGA_NEGATIVE, node);
	addBranchOmegaClass(OMEGA_NEUTRAL, node);
	addBranchOmegaClass(OMEGA_POSITIVE, node);
}

/**
 * @param node a node in the internal Branch REL tree
 */
void BranchSiteRELik::addBranchOmegaClass(omegaClass_t aOmegaClass, TreeNodeREL *node) {
	// Try to retrieve the combine BMN node  and ScalingMatrixNode of this TreeNodeREL
	CombinedBMNode *combineBMN = node->getCombineBranchMatrixNode();
	MatrixScalingNode *scalingNode = node->getMatrixScalingNode();

	if(combineBMN == NULL){ // Create CombineBMNode and ScalingMatrixNode if not yet done
		combineBMN = new CombinedBMNode(frequencies);
		node->setCombineBranchMatrixNode(combineBMN);
		scalingNode = new MatrixScalingNode();
		node->setMatrixScalingNode(scalingNode);
		// Register scaling node into combineBMN
		combineBMN->addChild(scalingNode);
	}

	// Create matrix node and add it to TreeNodeREL
	MatrixNode *matrixNode = new MatrixNode(nuclModel, aOmegaClass, frequencies, sharedMF);
	node->addMatrixNode(matrixNode);
	scalingNode->addChild(matrixNode); // Register MatrixNode into ScalingNode


	// Create branch matrix node and add it to TreeNodeREL, then register matrix node
	BranchMatrixNode *branchMNode = new BranchMatrixNode(aOmegaClass, frequencies);
	node->addBranchMatrixNode(branchMNode);
	branchMNode->addChild(matrixNode);
	branchMNode->addChild(scalingNode); // Register ScalingNode into BranchMatrixNode

	// Register the branch node into the combine branch matrix node
	combineBMN->addChild(branchMNode);
}

/**
 * Define the internal branch number for each branch of the tree.
 * Branch number are mapped using the internalBranches vectors
 *
 * @param newickNode node of the newick tree
 */
void BranchSiteRELik::labelizeInternalBranches(const DL::TreeNode &newickNode) {
	if(newickNode.children.size() > 0 && &newickNode != &newickRoot) {
		internalBranches.push_back(newickNode.getId());
	}

	for(size_t i=0; i<newickNode.children.size(); ++i) {
		labelizeInternalBranches(newickNode.children[i]);
	}
}

/**
 * This method create one sub-DAG for a set of sites.
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG nodes (LeafNode or CPVNode)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add DAG dependenices to the ndoe (CombineBMNode)
 *
 * @param subSites the vector of sites.
 * @param node the "parent" in the internal Branch REL tree.
 * @param nodeDAG the "parent" node in the DAG (there can be multiple for foreground branch)
 */
void BranchSiteRELik::createSubDAG(const std::vector<size_t> &subSites, TreeNodeREL *node, EdgeNode *nodeDAG, DL::MSA &msa) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		// DAG Node(s)
		EdgeNode *childNodeDAG = NULL;
		TreeNodeREL *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			LeafNode *newNode;
			newNode = new LeafNode(subSites, childNode->getName(), msa, frequencies, useCompression);
			childNodeDAG = newNode;

			if(useCompression) {
				accComp(newNode->getNCompressed());
				accTotal(subSites.size());
			}

		} else { // create CPV element
			CPVNode *newNode;
			newNode = new CPVNode(subSites, frequencies, useCompression);
			childNodeDAG = newNode;

			// create sub-DAG
			createSubDAG(subSites, childNode, childNodeDAG, msa);
		}

		// (2) We add the created child node to the current node (register to father).
		nodeDAG->addChild(childNodeDAG);

		// (3) We add dependencies (CombineBMNode) to the newly created node
		childNodeDAG->addChild(childNode->getCombineBranchMatrixNode());
	}


	// We try to compress the current node now that his children a created
	if(useCompression) {
		const std::type_info &type = typeid(*nodeDAG);
		if(type == typeid(CPVNode)){
			CPVNode *pNode = dynamic_cast<CPVNode*>(nodeDAG);
			pNode->compressNode();

			accComp(pNode->getNCompressed());
			accTotal(subSites.size());
		}
	}
}


void BranchSiteRELik::setSample(const SampleWrapper &sw) {
	for(size_t iB=0; iB<branchPtr.size(); ++iB) {
		setBranch(iB, sw);
	}
}

void BranchSiteRELik::setBranch(const size_t iB, const SampleWrapper &sw) {
	if(nuclModel == MU::GTR) {
		branchPtr[iB]->setThetas(sw.thetas);
	} else if(nuclModel == MU::K80) {
		branchPtr[iB]->setKappa(sw.kappa);
	} else {
		std::cerr << "Error : void BranchSiteRELik::setBranch(const size_t iB, const SampleWrapper &sw);" << std::endl;
		std::cerr << "Nucleotide model not supported by BranchSiteRELik" << std::endl;
		abort();
	}

	branchPtr[iB]->setBranchLength(sw.branchParams[iB].t);
	branchPtr[iB]->setP0(sw.branchParams[iB].p0);
	branchPtr[iB]->setP1(sw.branchParams[iB].p1);

	branchPtr[iB]->setOmegaNegative(sw.branchParams[iB].wNeg);
	branchPtr[iB]->setOmegaNeutral(sw.branchParams[iB].wNeutral);
	branchPtr[iB]->setOmegaPositive(sw.branchParams[iB].wPos);

}

std::vector<DAG::BaseNode*> BranchSiteRELik::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	SampleWrapper sw(nuclModel, branchPtr.size(), sample.getDblValues());
	setSample(sw);

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

std::string BranchSiteRELik::getCompressionReport() const {
	std::stringstream ss;
	double nCompressed = boost::accumulators::sum(accComp);
	double nTotal = boost::accumulators::sum(accTotal);

	ss << "Total number of CPV : " << nTotal << std::endl;
	ss << "Number of compressed CPV : " << nCompressed << std::endl;
	ss << "Global compression rate : " << nCompressed/nTotal << std::endl;
	ss << "Average CPV per node : " << boost::accumulators::mean(accTotal);
	ss << std::endl;
	ss << "Average Comp. CPV per node : " << boost::accumulators::mean(accComp);
	ss << std::endl;

	return ss.str();
}


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
