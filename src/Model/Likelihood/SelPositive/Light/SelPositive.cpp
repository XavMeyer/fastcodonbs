//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SelPositive.cpp
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief
 */
#include "SelPositive.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep

#include <assert.h>
#include <math.h>

#include "Model/Likelihood/SelPositive/Light/../SampleWrapper.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/FinalResultNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/MatrixNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/SelPositive/Light/TreeNodePS.h"
#include "DAG/Scheduler/Sequential/../BaseScheduler.h"
#include "Parallel/Manager/MpiManager.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "cmath"
#include "new"

namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace Sampler { class Sample; }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class BranchMatrixNode;
class CPVNode;
class CombineSiteNode;
class LeafNode;
class RootNode;

const bool SelPositive::USE_COMPRESSED_MSA = true;

SelPositive::SelPositive(const bool aH1, const bool aUseCompression,
		const long int aFGBranch, const size_t aNThread,
		const std::string &aFileAlign, const std::string &aFileTree) :
				H1(aH1), ONLY_INTERNAL(false), fgBranch(aFGBranch), nThread(aNThread),
				nuclModel(MolecularEvolution::MatrixUtils::K80), cFreqType(DL::CodonFrequencies::F3x4),
				fileAlign(aFileAlign), fileTree(aFileTree),
				fr(fileAlign), msa(DL::MSA::CODON, fr.getAlignments(), false),
				np(fileTree), newickRoot(np.getRoot()), rootPS(newickRoot),
				frequencies(MD::Codons::NB_CODONS_WO_STOP, DL::CodonFrequencies(msa).getCodonFrequency(cFreqType))  {
	scalingType = GLOBAL;
	init(aUseCompression);
}

SelPositive::SelPositive(const bool aH1, const bool aUseCompression, const scalingType_t aScalingType,
		const long int aFGBranch, const size_t aNThread,
		const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
		const std::string &aFileAlign, const std::string &aFileTree) :
				H1(aH1), ONLY_INTERNAL(false), scalingType(aScalingType), fgBranch(aFGBranch), nThread(aNThread),
				nuclModel(aNuclModel), cFreqType(aCFreqType), fileAlign(aFileAlign), fileTree(aFileTree),
				fr(fileAlign), msa(DL::MSA::CODON, fr.getAlignments(), false),
				np(fileTree), newickRoot(np.getRoot()), rootPS(newickRoot),
				frequencies(MD::Codons::NB_CODONS_WO_STOP, DL::CodonFrequencies(msa).getCodonFrequency(cFreqType))  {
	init(aUseCompression);
}

SelPositive::~SelPositive() {

	if(USE_COMPRESSED_MSA) {
		delete ptrCA;
	}

	rootPS.deleteChildren();
	rootDAG->deleteChildren();
	delete rootDAG;
	delete scheduler;
}

void SelPositive::init(const bool aUseCompression) {

	first = true;
	isLogLH = true;

	#if TI_USE_STAN
		isUpdatableLH = false;
	#else
		isUpdatableLH = true;
	#endif


	useCompression = aUseCompression;
	if(useCompression) {
		msa.sortByDistance();
	}

	if(USE_COMPRESSED_MSA) {
		ptrCA = new DL::CompressedAlignements(msa, true);
		nSite = ptrCA->getNSite();
		//std::cout << "Total number of sites : " << msa.getNSite() << std::endl;
		//std::cout << "Number of unique sites : " << ptrCA->getNSite() << std::endl;
	} else {
		ptrCA = NULL;
		nSite = msa.getNValidSite();
	}

	fgNodePS = NULL;

	prepareDAG();
	createTreeAndDAG();

	//if(nThread > 1) {
		//Eigen::initParallel();
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
	if(nThread > 1) {
		Eigen::initParallel();
		//scheduler = new DAG::Scheduler::Static::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::PriorityListII::ThreadSafeScheduler(nThread, rootDAG);
		scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	markerNames.push_back("MRKR_Prop0");
	markerNames.push_back("MRKR_Prop1");
	markerNames.push_back("MRKR_Prop2a");
	markerNames.push_back("MRKR_Prop2b");
	markers.resize(markerNames.size());

}

double SelPositive::processLikelihood(const Sampler::Sample &sample) {

	//std::cout << "Here 1 !!" << std::endl;
#if TI_USE_STAN
	stan::math::start_nested();
#endif

	// Update the values
	SampleWrapper sw(nuclModel, isH1(), sample.getDblValues());
	setSample(sw);

	//std::cout << "Here 2 !!" << std::endl;

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();
	// Process the tree
	scheduler->process();

	//std::cout << "Here 3 !!" << std::endl;

	TI_TYPE lik = rootDAG->getLikelihood();

	//std::cout << "Here 4 !!" << std::endl;

#if TI_USE_STAN
	//std::vector<double> gradient;
	std::vector<TI_TYPE> params(sw.getParameters());
	//std::vector<stan::math::var> tmpParams(1, params.front());

	double likVal = lik.val();

	if(likVal != likVal) {
		gradient.assign(params.size(), -std::log(-1));
		/*for(size_t i=0; i<params.size(); ++i) {
			gradient.push_back();
		}*/
	} else {
		gradient.clear();
		lik.grad(params, gradient);
	}
	//std::cout << likVal << std::endl;
	//stan::math::grad(lik.vi_);

	//stan::math::recover_memory();
	stan::math::recover_memory_nested();

	//std::cout << likVal << std::endl;
	//getchar();

	//getchar();

	/*std::cout << "Gradient : ";
	for(size_t i=0; i<gradient.size(); ++i) {
		std::cout << gradient[i] << "\t";
	}
	std::cout << std::endl;*/

		markers[0] = scalingMat->getProportions().getProportion(0).val();
		markers[1] = scalingMat->getProportions().getProportion(1).val();
		markers[2] = scalingMat->getProportions().getProportion(2).val();
		markers[3] = scalingMat->getProportions().getProportion(3).val();


	return likVal;
#else

	markers[0] = scalingMat->getProportions().getProportion(0);
	markers[1] = scalingMat->getProportions().getProportion(1);
	markers[2] = scalingMat->getProportions().getProportion(2);
	markers[3] = scalingMat->getProportions().getProportion(3);

	return lik;
#endif

}

std::string SelPositive::getName(char sep) const {
	return std::string("SelectionPositiveLight");
}

size_t SelPositive::getNBranch() const {
	return branchPtr.size();
}

nuclModel_t SelPositive::getNucleotideModel() const {
	return nuclModel;
}

codonFreq_t SelPositive::getCodonFrequencyType() const {
	return cFreqType;
}

size_t SelPositive::stateSize() const {
	return Q0->serializedSize() + Q1->serializedSize() + Q2->serializedSize();
}

void SelPositive::setStateLH(const char* aState) {
	Q0->serializeFromBuffer(aState);
	Q1->serializeFromBuffer(aState+Q0->serializedSize());
	Q2->serializeFromBuffer(aState+Q0->serializedSize()+Q1->serializedSize());
}

void SelPositive::getStateLH(char* aState) const {
	Q0->serializeToBuffer(aState);
	Q1->serializeToBuffer(aState+Q0->serializedSize());
	Q2->serializeToBuffer(aState+Q0->serializedSize()+Q1->serializedSize());
}

double SelPositive::update(const vector<size_t>& pIndices, const Sample& sample) {

	SampleWrapper sw(nuclModel, isH1(), sample.getDblValues());
	setSample(sw);

	// Reset the DAG
	if(!first) { // Partial
		if(!scheduler->resetPartial()) {
			/*std::cerr << "Reset partial did not found any change for parameters : " << std::endl;
			for(size_t i=0; i< pIndices.size(); ++i) {
				std::cerr << "[" << pIndices[i] << "] = "  << sample.getDoubleParameter(i) << "\t";
			}
			std::cerr << std::endl;*/
		}
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	// Process the tree
	scheduler->process();

	TI_TYPE lik = rootDAG->getLikelihood();

#if TI_USE_STAN
	std::vector<double> gradient;
	std::vector<TI_TYPE> params(sw.getParameters());
	//std::vector<stan::math::var> tmpParams(1, params.front());
	//lik.grad(params, gradient);

	double likVal = lik.val();
	//std::cout << likVal << std::endl;
	stan::math::grad(lik.vi_);

	stan::math::recover_memory_nested();
	//getchar();

	/*std::cout << "Gradient : ";
	for(size_t i=0; i<gradient.size(); ++i) {
		std::cout << gradient[i] << "\t";
	}
	std::cout << std::endl;*/

	markers[0] = scalingMat->getProportions().getProportion(0).val();
	markers[1] = scalingMat->getProportions().getProportion(1).val();
	markers[2] = scalingMat->getProportions().getProportion(2).val();
	markers[3] = scalingMat->getProportions().getProportion(3).val();

	return likVal;
#else

	markers[0] = scalingMat->getProportions().getProportion(0);
	markers[1] = scalingMat->getProportions().getProportion(1);
	markers[2] = scalingMat->getProportions().getProportion(2);
	markers[3] = scalingMat->getProportions().getProportion(3);

	return lik;
#endif

}

DAG::Scheduler::BaseScheduler* SelPositive::getScheduler() const {
	return scheduler;
}

std::vector<DAG::BaseNode*> SelPositive::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	SampleWrapper sw(nuclModel, isH1(), sample.getDblValues());
	setSample(sw);

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

bool SelPositive::isUsingCompression() const {
	return useCompression;
}

long int SelPositive::getFGBranch() const {
	return fgBranch;
}

size_t SelPositive::getNThread() const {
	return nThread;
}

std::string SelPositive::getAlignFile() const {
	return fileAlign;
}

std::string SelPositive::getTreeFile() const {
	return fileTree;
}

bool SelPositive::isH1() const {
	return H1;
}

double SelPositive::getGlobalCompressionRate() const {
	double nCompressed = boost::accumulators::sum(accComp);
	double nTotal = boost::accumulators::sum(accTotal);

	return nCompressed/nTotal;
}


std::vector<size_t> SelPositive::getBranchesLabels() const {
	return labelBranches;
}

std::vector<std::string> SelPositive::getBranchNames() const {

	std::vector<std::string> names;
	for(size_t i=0; i<labelBranches.size(); ++i) {
		names.push_back(branchPtr[i]->getName());
	}

	return names;
}

std::string SelPositive::getCompressionReport() const {
	std::stringstream ss;
	double nCompressed = boost::accumulators::sum(accComp);
	double nTotal = boost::accumulators::sum(accTotal);

	ss << "Total number of CPV : " << nTotal << std::endl;
	ss << "Number of compressed CPV : " << nCompressed << std::endl;
	ss << "Global compression rate : " << nCompressed/nTotal << std::endl;
	ss << "Average CPV per node : " << boost::accumulators::mean(accTotal);
	ss << std::endl;
	ss << "Average Comp. CPV per node : " << boost::accumulators::mean(accComp);
	ss << std::endl;

	return ss.str();
}

std::string SelPositive::getNewickString() const {
	return rootPS.buildNewick();
}

void SelPositive::printDAG() const {
	std::cout << "**************************************************************************" << std::endl;
	std::cout << rootDAG->subtreeToString() << std::endl;
	std::cout << "**************************************************************************" << std::endl;
}

void SelPositive::initForBEB(const Sample &sample){
	assert(isH1());
	SampleWrapper sw(nuclModel, isH1(), sample.getDblValues());
	setSample(sw);

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();
	// Process the tree
	scheduler->process();

	scalingMat->setFixedScalingForBEB();
}

void SelPositive::resetForBEB(){
	assert(isH1());

	scalingMat->resetFixedScalingForBEB();
}

TI_EigenMatrixDyn_t SelPositive::getSiteLikelihoodAtRoot(size_t iClass) {

	std::vector<CombineSiteNode*> csNodes = rootDAG->getCsNodes();

	TI_EigenMatrixDyn_t matLik;

	// Resize in function of the use of log scaling
	if(EdgeNode::SCALING_TYPE == EdgeNode::LOG || EdgeNode::SCALING_TYPE == EdgeNode::LOG_ACCURATE) {
		matLik.resize(2, msa.getNValidSite());
	} else {
		matLik.resize(1, msa.getNValidSite());
	}

	// For each Site Combine node, retrieve results for class iClass
	for(size_t iN=0; iN<csNodes.size(); ++iN) {
		std::vector<size_t> iSitePos;
		std::vector<TI_TYPE> subLik, subNrm;

		// Retrieve results
		csNodes[iN]->getLikelihoodsForBEB(iClass, iSitePos, subLik, subNrm);

		// for each likelihood, translate compressed position if needed
		for(size_t iS=0; iS<iSitePos.size(); ++iS) {
			size_t sitePosition = iSitePos[iS];
			std::vector<size_t> validPositions;
			if(ptrCA) {
				validPositions = ptrCA->getOriginalSites(sitePosition);
			} else {
				validPositions.push_back(sitePosition);
			}

			for(size_t iV=0; iV<validPositions.size(); ++iV) {
				size_t iValid = validPositions[iV];
				matLik(0, iValid) = subLik[iS];
				if(EdgeNode::SCALING_TYPE == EdgeNode::LOG || EdgeNode::SCALING_TYPE == EdgeNode::LOG_ACCURATE) {
					matLik(1, iValid) = subNrm[iS];
				}
			}
		}
	}
	return matLik;
}


void SelPositive::prepareDAG() {

	// Create Matrix nodes
	Q0 = new MatrixNode(nuclModel, OMEGA_0, frequencies);
	Q1 = new MatrixNode(nuclModel, OMEGA_1, frequencies);
	Q1->setOmega(1.);
	Q2 = new MatrixNode(nuclModel, OMEGA_2, frequencies);
	if(!isH1()) {
		Q2->setOmega(1.);
	}


	// Create Matrix Scaling and proportions node
	scalingMat = new MatrixScalingNode();
	scalingMat->addChild(Q0);
	scalingMat->addChild(Q1);
	scalingMat->addChild(Q2);

	// Root of the DAG
	rootDAG = new FinalResultNode();

}

void SelPositive::createTreeAndDAG() {

	// Labelize internal branches
	labelizeBranch(newickRoot);
	if(fgBranch >= 0) { // Branch is specified by its number
		if(fgBranch >= (int)labelBranches.size()) {
			std::cerr << "[ERROR] There are " << labelBranches.size() << " branches therefore the branch (" << fgBranch << ") is invalid." << std::endl;
			abort();
		} else {
			fgInternalNodeIds.insert(labelBranches[fgBranch]);
		}
	} else { // Branch(es) is(are) specified in the file by name starting with #
		extractFgBranches(newickRoot);
		if(fgInternalNodeIds.empty()) {
			std::cerr << "[ERROR] There are no labeled branches in the newick file. To manually design a FG branch use positive number in [foregroundBranch] tag." << std::endl;
			abort();
		}
	}

	// Root
	bool isAfterFG = createTree(newickRoot, &rootPS);
	assert(isAfterFG == true);
	rootPS.setAfterFG(isAfterFG);

	/*if(isH1() && Parallel::mpiMgr().isMainProcessor()) {
		std::cout << "The foreground branch is : " << fgNodePS->getName() << std::endl;
	}*/

	// Decompose the DAG in function of the NB of thread.
	size_t nSubDAG = 1;
	if(isUpdatableLH && nThread > 2){
		size_t tmpVal = ceil((double)nThread/2.);
		nSubDAG = std::min(tmpVal, nSite);
	}
	size_t sizeSubDag = ceil(static_cast<double>(nSite) / static_cast<double>(nSubDAG));
	for(size_t iSD=0; iSD<nSubDAG; ++iSD) {
		// Init subSites
		size_t start = iSD*sizeSubDag;
		size_t end = std::min((iSD+1)*sizeSubDag, nSite);
		//std::cout << start << "\t::\t" << end << std::endl;
		std::vector<size_t> subSites;
		for(size_t iSS=start; iSS<end; ++iSS) {
			subSites.push_back(iSS);
		}

		// Create site combiner
		CombineSiteNode* ptrCSN = new CombineSiteNode(subSites, ptrCA);
		//std::cout << "CombineSiteNode : " << ptrCSN->getId() << std::endl;
		ptrCSN->addChild(scalingMat); // Add scaling mat to CombineSiteNode

		// Create root(s) (OMEGA_0 is for C0 and C2a // OMEGA_1 is for C1 and C2b)
		RootNode *ptrR0N = new RootNode(OMEGA_0, subSites, frequencies, useCompression);
		//std::cout << "Root node 0 : " << ptrR0N->toString() << std::endl;
		RootNode *ptrR1N = new RootNode(OMEGA_1, subSites, frequencies, useCompression);
		//std::cout << "Root node 1 : " << ptrR1N->getId() << std::endl;

		createSubDAG(subSites, &rootPS, ptrR0N); // Create sub DAG for C0/C2a
		createSubDAG(subSites, &rootPS, ptrR1N); // Create sub DAG for C1/C2b

		if(useCompression) {
			ptrR0N->compressNode();
			accComp(ptrR0N->getNCompressed());
			accTotal(end-start);
			ptrR1N->compressNode();
			accComp(ptrR1N->getNCompressed());
			accTotal(end-start);
		}

		// Add them to the CombineSiteNode
		ptrCSN->addChild(ptrR0N);
		ptrCSN->addChild(ptrR1N);

		// Add this CombineSiteNode to the DAG root
		rootDAG->addChild(ptrCSN);
	}

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		std::cout << getCompressionReport();
		std::cout << "**************************************" << std::endl;
	}*/
}

/**
 * Define the internal branch number for each branch of the tree.
 * Branch number are mapped using the internalBranches vectors
 *
 * @param newickNode node of the newick tree
 */
void SelPositive::labelizeBranch(const DL::TreeNode &newickNode) {
	if((!ONLY_INTERNAL || newickNode.children.size() > 0) && &newickNode != &newickRoot) {
		labelBranches.push_back(newickNode.getId());
	}

	for(size_t i=0; i<newickNode.children.size(); ++i) {
		labelizeBranch(newickNode.children[i]);
	}
}

/**
 * Define the branch marked with # as first character in their name as the FG branches
 *
 * @param newickNode node of the newick tree
 * @param fgInternalNodeIds id of the marked branches
 */
void SelPositive::extractFgBranches(const DL::TreeNode &newickNode) {
	if((!ONLY_INTERNAL || newickNode.children.size() > 0) && &newickNode != &newickRoot) {
		if(!newickNode.getName().empty() && newickNode.getName().at(0) == '#') {
			fgInternalNodeIds.insert(newickNode.getId());
		}
	}

	for(size_t i=0; i<newickNode.children.size(); ++i) {
		extractFgBranches(newickNode.children[i]);
	}
}

void SelPositive::addBranchMatrixNodes(TreeNodePS *node) {
	// Add W0, W1 and if needed W2 matrix nodes
	BranchMatrixNode *ptrBMN;
	// Omega 0
	ptrBMN = new BranchMatrixNode(scalingType, node->isFGBranch(), OMEGA_0, frequencies);
	if(scalingType != NONE) {
		ptrBMN->addChild(scalingMat);
	}
	ptrBMN->addChild(Q0);
	//std::cout << "BM Q0 NODE : " << ptrBMN->getId() << std::endl;
	node->addBranchMatrixNode(ptrBMN);

	// Omega 1
	ptrBMN = new BranchMatrixNode(scalingType, node->isFGBranch(), OMEGA_1, frequencies);
	if(scalingType != NONE) {
		ptrBMN->addChild(scalingMat);
	}
	ptrBMN->addChild(Q1);
	//std::cout << "BM Q1 NODE : " << ptrBMN->getId() << std::endl;
	node->addBranchMatrixNode(ptrBMN);

	// if required Omega 2
	if(node->isFGBranch()) {
		ptrBMN = new BranchMatrixNode(scalingType, node->isFGBranch(), OMEGA_2, frequencies);
		//std::cout << "BM Q2 NODE : " << ptrBMN->getId() << std::endl;
		if(scalingType != NONE) {
			ptrBMN->addChild(scalingMat);
		}
		ptrBMN->addChild(Q2);
		node->addBranchMatrixNode(ptrBMN);
	}
}


/**
 * @param newickNode a new from DataLoader::TreeNode. i.e. the ones returned from NewickParser
 * @param node a node in the internal Positive Selection tree
 * @return If the current node is the FG node or after the FG node
 */
bool SelPositive::createTree(const DL::TreeNode &newickNode, TreeNodePS *node) {

	if(newickNode.children.size() > 0) { // If node has children, process
		// Set boolean info.
		node->setLeaf(false);

		// For all children in newick, create in PosSel
		bool isAfterFG = false;
		for(size_t i=0; i<newickNode.getChildren().size(); ++i){
			//bool isChildFGBranch = newickNode.getChildren()[i].getId() == fgInternalNodeId;
			bool isChildFGBranch = fgInternalNodeIds.find(newickNode.getChildren()[i].getId()) != fgInternalNodeIds.end();
			//isChildFGBranch = isChildFGBranch && isH1();

			TreeNodePS *childNode = new TreeNodePS(newickNode.getChildren()[i], isChildFGBranch);
			branchPtr.push_back(childNode); // Keep pointer to branch for t update

			bool childIsAfterFG = createTree(newickNode.getChildren()[i], childNode);
			isAfterFG = isAfterFG || childIsAfterFG;
			node->addChild(childNode);
			addBranchMatrixNodes(childNode); // Add BranchMatrixNode(s)
			if(childNode->isFGBranch()) { // Keep pointer to FG node -> branch
				fgNodePS = childNode;
			}
		}

		// Updage if it is after FG branch)
		node->setAfterFG(isAfterFG);

	} else { // Else it is a leaf node
		node->setLeaf(true);
	}

	return node->isFGBranch() || node->isAfterFG();
}

void SelPositive::createSubDAG(const std::vector<size_t> &subSites, TreeNodePS *node, EdgeNode *nodeDAG){
	vecEdgeN_t nodesDAG;
	nodesDAG.push_back(nodeDAG);
	createSubDAG(subSites, node, nodesDAG);
}


/**
 * This method create one sub-DAG for a set of sites and for a couple of class (C0/C2a or C1/C2b).
 * For each children node it is doing the following steps :
 * 1) Create the right child DAG node (LeafNode or CPVNode)
 * 1*) Special case for FG branch where 2 children are created (addition is for OMEGA_2)
 * 2) Register the newly created DAG node(s) to its parent
 * 3) Add the correct DAG dependencies to the child node (BranchMatrixNode)
 *
 * @param subSites the vector of sites.
 * @param node the "parent" in the internal positive sel. tree.
 * @param nodesDAG the "parent" nodes in the DAG (there can be multiple for foreground branch)
 */
void SelPositive::createSubDAG(const std::vector<size_t> &subSites, TreeNodePS *node, vecEdgeN_t &nodesDAG) {

	// For each children
	for(size_t i=0; i<node->getChildren().size(); ++i) {
		// DAG Node(s)
		vecEdgeN_t childNodesDAG;
		TreeNodePS *childNode = node->getChild(i);

		// (1) Create New DAG Node
		if(childNode->isLeaf()) { // Create leaf element (no FG on leaf transition)
			bool isFG = childNode->isFGBranch();
			bool isAftFG = childNode->isAfterFG();
			LeafNode *newNode;
			newNode = new LeafNode(isFG, isAftFG, nodesDAG.front()->getOmegaClass(), subSites, childNode->getName(),
					msa, ptrCA, frequencies, useCompression);
			childNodesDAG.push_back(newNode);
			if(useCompression) {
				accComp(newNode->getNCompressed());
				accTotal(subSites.size());
			}

			// (1*) If FG Branch we have to create a new element for OMEGA_2
			if(childNode->isFGBranch()) {
				LeafNode *newNodeC2;
				newNodeC2 = new LeafNode(isFG, isAftFG, OMEGA_2, subSites, childNode->getName(),
						msa, ptrCA, frequencies, useCompression);
				childNodesDAG.push_back(newNodeC2);
				if(useCompression) {
					accComp(newNodeC2->getNCompressed());
					accTotal(subSites.size());
				}
			}

		} else { // create CPV element
			bool isFG = childNode->isFGBranch();
			bool isAftFG = childNode->isAfterFG();
			CPVNode *newNode;
			newNode = new CPVNode(isFG, isAftFG, nodesDAG.front()->getOmegaClass(), subSites, frequencies, useCompression);
			//std::cout << "CPV NODE : " << newNode->getId() << std::endl;
			childNodesDAG.push_back(newNode);

			// (1*) If FG Branch we have to create a new element for OMEGA_2
			if(childNode->isFGBranch()) {
				CPVNode *newNodeC2;
				newNodeC2 = new CPVNode(isFG, isAftFG, OMEGA_2, subSites, frequencies, useCompression);
				childNodesDAG.push_back(newNodeC2);
			}

			// create sub-DAG
			createSubDAG(subSites, childNode, childNodesDAG);
		}

		// (2) We add all child nodes created to the current node (register to father).
		// It only surpasses 1 when we are at the FG branch or below
		for(size_t iP=0; iP<nodesDAG.size(); ++iP) {
			for(size_t iC=0; iC<childNodesDAG.size(); ++iC) {
				nodesDAG[iP]->addChild(childNodesDAG[iC]);
			}
		}

		// (3) We add dependencies to the newly created node (BranchMatrixNode)
		for(size_t i=0; i<childNodesDAG.size(); ++i) {
			//std::cout << childNodesDAG[i]->toString() << std::endl << "Omega class : " << childNodesDAG[i]->getOmegaClass() << std::endl;
			BranchMatrixNode *ptrBMN = childNode->getBranchMatrixNode(childNodesDAG[i]->getOmegaClass());
			childNodesDAG[i]->addChild(ptrBMN);
		}

	}

	// We try to compress each node
	if(useCompression) {
		for(size_t iP=0; iP<nodesDAG.size(); ++iP) {

			const EdgeNode &eNode = *nodesDAG[iP];
			const std::type_info &type = typeid(eNode);
			if(type == typeid(CPVNode)){
				//std::cout << "Here!" << std::endl;
				CPVNode *pNode = dynamic_cast<CPVNode*>(nodesDAG[iP]);
				pNode->compressNode();
				accComp(pNode->getNCompressed());
				accTotal(subSites.size());
			}
		}
	}
}

void SelPositive::setSample(const SampleWrapper &sw) {
	if(nuclModel == MU::GTR) {
		setThetas(sw);
	} else if(nuclModel == MU::K80) {
		setKappa(sw);
	} else {
		std::cerr << "Error : void SelPositive::setSample(const SampleWrapper &sw);" << std::endl;
		std::cerr << "Nucleotide model not supported by Light::SelPositive" << std::endl;
		abort();
	}

	setOmega0(sw);
	if(isH1()) {
		setOmega2(sw);
	}
	setProportions(sw);
	setBranchLength(sw);
}

void SelPositive::setKappa(const SampleWrapper &sw) {
	Q0->setKappa(sw.K);
	Q1->setKappa(sw.K);
	Q2->setKappa(sw.K);
}

void SelPositive::setThetas(const SampleWrapper &sw) {
	Q0->setThetas(sw.thetas);
	Q1->setThetas(sw.thetas);
	Q2->setThetas(sw.thetas);
}

void SelPositive::setOmega0(const SampleWrapper &sw) {
	Q0->setOmega(sw.W0);
}

void SelPositive::setOmega2(const SampleWrapper &sw) {
	//std::cout << "Omega W2 = " << sw.W2 << std::endl;
	Q2->setOmega(sw.W2);
}

void SelPositive::setProportions(const SampleWrapper &sw) {
	scalingMat->setP0(sw.P0);
	scalingMat->setP1(sw.P1);
}

void SelPositive::setBranchLength(const SampleWrapper &sw) {
	for(size_t i=0; i<sw.N_BL; ++i) {
		branchPtr[i]->setBranchLength(sw.branchLength[i]);
		//branchPtr[i]->signalUpdated(); // FIXME
	}
}

void SelPositive::cleanTreeAndDAG() {
	/*delete Q0;
	delete Q1;
	delete Q2;

	delete scalingMat;*/
	rootPS.deleteChildren();
	rootDAG->deleteChildren();
	delete rootDAG;
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
