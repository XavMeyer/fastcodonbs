//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SelPositive.h
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief This class process the likelihood for the Positive Selection model.
 *
 * This class process the likelihood for the Positive Selection model. It begins by
 * creating an internal tree that will be used to be linked with the DAG nodes. Then
 * the DAG is created and the DAG scheduler is initialised.
 *
 * A sample is formed of the following components :
 * [ k, p0, p1, w0, w1, t(1..nBranch) ]
 *
 */
#ifndef SELPOSITIVELIGHT_H_
#define SELPOSITIVELIGHT_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <stddef.h>
#include <fstream>
#include <iostream>

#include "../SampleWrapper.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriorityII/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/StaticScheduler/ThreadSafeScheduler.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Nodes/IncNodes.h"
#include "TreeNodePS.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/CodonFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace MolecularEvolution { namespace DataLoader { class CompressedAlignements; } }
namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace Sampler { class Sample; }
namespace StatisticalModel { namespace Likelihood { namespace PositiveSelection { class SampleWrapper; } } }
namespace boost { namespace accumulators { namespace tag { struct mean; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class FinalResultNode;
class MatrixNode;
class MatrixScalingNode;

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;

typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;
typedef DL::CodonFrequencies::codonFrequencies_t codonFreq_t;

class SelPositive : public LikelihoodInterface {
public:
	SelPositive(const bool aH1, const bool aUseCompression, const long int aFGBranch, const size_t aNThread,
				const std::string &aFileAlign, const std::string &aFileTree);

	SelPositive(const bool aH1, const bool aUseCompression, const scalingType_t aScalingType,
				const long int aFGBranch, const size_t aNThread,
				const nuclModel_t aNuclModel, const codonFreq_t aCFreqType,
				const std::string &aFileAlign, const std::string &aFileTree);
	~SelPositive();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;
	nuclModel_t getNucleotideModel() const;
	codonFreq_t getCodonFrequencyType() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	bool isUsingCompression() const;
	long int getFGBranch() const;
	size_t getNThread() const;
	std::string getAlignFile() const;
	std::string getTreeFile() const;
	bool isH1() const;
	std::vector<size_t> getBranchesLabels() const;
	std::vector<std::string> getBranchNames() const;

	double getGlobalCompressionRate() const;
	std::string getCompressionReport() const;
	std::string getNewickString() const;
	void printDAG() const;

	void initForBEB(const Sample &sample);
	void resetForBEB();
	TI_EigenMatrixDyn_t getSiteLikelihoodAtRoot(size_t iClass);

private:

	static const bool USE_COMPRESSED_MSA;

	/* Input data */
	const bool H1, ONLY_INTERNAL;
	scalingType_t scalingType;
	bool useCompression, first;
	long int fgBranch;
	size_t nThread, nSite;
	const nuclModel_t nuclModel;
	const codonFreq_t cFreqType;
	std::string fileAlign, fileTree;
	DL::FastaReader fr;
	DL::MSA msa;
	DL::CompressedAlignements *ptrCA;
	DL::NewickParser np;
	const DL::TreeNode &newickRoot;

	/* Internal data */
	TreeNodePS rootPS;
	const TreeNodePS* fgNodePS;
	std::vector<TreeNodePS*> branchPtr;
	DL::Utils::Frequencies frequencies;
	std::vector<size_t> labelBranches;
	std::set<size_t> fgInternalNodeIds;
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accComp_t;
	accComp_t accComp, accTotal;

	/* DAG data */
	MatrixNode *Q0, *Q1, *Q2;
	MatrixScalingNode *scalingMat;
	FinalResultNode *rootDAG;

	/* DAG Scheduler */
	DAG::Scheduler::BaseScheduler *scheduler;

	//! Called by constructor to init everything
	void init(const bool aUseCompression);

	//! Prepare DAG elements (matrices, scaling matrix
	void prepareDAG();
	//! Build the internal tree and the DAG
	void createTreeAndDAG();

	//! Labelize the internal branches
	void labelizeBranch(const DL::TreeNode &newickNode);
	//! Exctract FGs branches
	void extractFgBranches(const DL::TreeNode &newickNode);

	//! Create recursively the internal tree based on the newick one
	bool createTree(const DL::TreeNode &newickNode, TreeNodePS *node);
	//! Add the adequate BranchMatrix node to the PS node
	void addBranchMatrixNodes(TreeNodePS *node);

	//! Create recursively a sub-DAG (based on the internal Tree)
	void createSubDAG(const std::vector<size_t> &subSites, TreeNodePS *node, EdgeNode *nodeDAG);
	typedef std::vector<EdgeNode*> vecEdgeN_t;
	void createSubDAG(const std::vector<size_t> &subSites, TreeNodePS *node, vecEdgeN_t &nodesDAG);

	void cleanTreeAndDAG();

	// Setters for the samples
	void setSample(const SampleWrapper &sw);
	void setKappa(const SampleWrapper &sw);
	void setThetas(const SampleWrapper &sw);
	void setProportions(const SampleWrapper &sw);
	void setOmega0(const SampleWrapper &sw);
	void setOmega2(const SampleWrapper &sw);
	void setBranchLength(const SampleWrapper &sw);

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SELPOSITIVELIGHT_H_ */
