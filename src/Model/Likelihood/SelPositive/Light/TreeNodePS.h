//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNodePS.h
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODEPSLIGHT_H_
#define TREENODEPSLIGHT_H_

#include <stddef.h>
#include <sstream>
#include <vector>

#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"
#include "Nodes/IncNodes.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"

namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class TreeNodePS;
class BranchMatrixNode;

typedef std::vector<TreeNodePS*> treeNodePS_children;

class TreeNodePS {
public:
	TreeNodePS(const MolecularEvolution::DataLoader::TreeNode &aNewickNode, bool aIsFGBranch=false);
	~TreeNodePS();

	void addChild(TreeNodePS *child);

	TreeNodePS* getChild(size_t id);
	const treeNodePS_children& getChildren() const;
	const std::string& getName() const;

	void addBranchMatrixNode(BranchMatrixNode *node);
	BranchMatrixNode* getBranchMatrixNode(omegaClass_t aOmegaClass);

	void setBranchLength(const TI_TYPE aBL);
	void setFGBranch(const bool aFGBranch);
	void setAfterFG(const bool aAfterFG);
	void setLeaf(const bool aLeaf);
	void signalUpdated();


	bool isFGBranch() const;
	bool isAfterFG() const;
	bool isLeaf() const;

	const std::string toString() const;

	void deleteChildren();

	std::string buildNewick() const;
	void addNodeToNewick(std::string &nwk) const;

private:

	typedef std::vector<BranchMatrixNode *> branchMDAG_t;

	/* Default data */
	size_t id;
	std::string name;
	TI_TYPE branchLength;

	/* state linked to foreground branch */
	bool fgBranch, afterFG, leaf;
	treeNodePS_children children;

	/* Linkage with DAG elements */
	branchMDAG_t branchMDAG;



};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TREENODEPSLIGHT_H_ */
