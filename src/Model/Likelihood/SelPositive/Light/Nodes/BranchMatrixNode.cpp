//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixNode.cpp
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#include "BranchMatrixNode.h"

#include "Model/Likelihood/SelPositive/Light/Nodes/MatrixNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

BranchMatrixNode::BranchMatrixNode(const scalingType_t aScalingType, const bool aFGBranch,
					const omegaClass_t aOmegaClass, DL_Utils::Frequencies &aFrequencies) :
		BaseNode(), scalingType(aScalingType), fgBranch(aFGBranch), omegaClass(aOmegaClass),
		frequencies(aFrequencies), Z(frequencies.size(), frequencies.size()) {

	branchLength = 0;
	afterFgBranch = false;
	scalingNode = NULL;
	matrixNode = NULL;

}

BranchMatrixNode::~BranchMatrixNode() {
}

void BranchMatrixNode::setBranchLength(const TI_TYPE aBL) {
	branchLength = aBL;
	BaseNode::updated();
}

void BranchMatrixNode::setFgBranch(const bool aFgBranch) {
	fgBranch = aFgBranch;
	BaseNode::updated();
}

void BranchMatrixNode::setAfterFgBranch(const bool aAfterFgBranch) {
	afterFgBranch = aAfterFgBranch;
	BaseNode::updated();
}

omegaClass_t BranchMatrixNode::getOmegaClass() const {
	return omegaClass;
}


const TI_EigenSquareMatrix_t& BranchMatrixNode::getZ() const {
	return Z;
}

void BranchMatrixNode::doProcessing() {

	// Shortcuts
	const TI_EigenEIGVector_t &D = matrixNode->getEigenValues();
	const TI_EigenEIGMatrix_t &scaledV = matrixNode->getScaledEigenVectors();

	// Process number of substitutions
	TI_TYPE t = branchLength;
	if(scalingType == GLOBAL) {
		if(fgBranch) {
			t /= scalingNode->getScalingFG();
			//t /= matrixNode->getMatrixQScaling();
		} else {
			t /= scalingNode->getScalingBG();
			//t /= matrixNode->getMatrixQScaling();
		}
	} else if(scalingType == LOCAL){
		t /= matrixNode->getMatrixQScaling();
	} else { // NONE
		// Do nothing
	}

	// Processing Y
	TI_EigenEIGArray_t dt = (t/2.0) * D.array();
	dt = dt.exp();
	TI_EigenEIGVector_t dt2 = dt.matrix();

	TI_EigenEIGMatrix_t Y = scaledV * dt2.asDiagonal();
	//Z.setZero();
	Z.triangularView<Eigen::Lower>() = Y*Y.transpose();
	Z.eval();
}

bool BranchMatrixNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void BranchMatrixNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		matrixNode = dynamic_cast<MatrixNode*>(aChild);
	} else if(childtype == typeid(MatrixScalingNode)){
		scalingNode = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
