//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.h
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMBINESITENODELIGHT_H_
#define COMBINESITENODELIGHT_H_

#include <stddef.h>
#include <vector>

#include "DAG/Node/Base/BaseNode.h"
#include "MatrixScalingNode.h"
#include "RootNode.h"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"

namespace MolecularEvolution { namespace DataLoader { class CompressedAlignements; } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class MatrixScalingNode;
class RootNode;

namespace DL = ::MolecularEvolution::DataLoader;

class CombineSiteNode: public DAG::BaseNode {
public:
	CombineSiteNode(const std::vector<size_t> &aSites, const DL::CompressedAlignements *aPtrCA);
	~CombineSiteNode();

	void getLikelihoodsForBEB(size_t iClass, std::vector<size_t> &iSite, std::vector<TI_TYPE> &lik, std::vector<TI_TYPE> &nrm);

	TI_TYPE getLikelihood() const;

private:

	TI_TYPE likelihood;

	struct SiteSubLikelihood {
		size_t site;
		int idxSLik[N_CLASS];
		RootNode* ptrNode[N_CLASS];
		SiteSubLikelihood(const size_t aSite) : site(aSite) {
			for(size_t iC=0; iC<N_CLASS; ++iC){idxSLik[iC] = -1; ptrNode[iC] = NULL;}
		}
		~SiteSubLikelihood(){}
		bool operator== (const SiteSubLikelihood &aPLS) const{
			return aPLS.site == site;
		}
	};

	typedef std::vector<SiteSubLikelihood> vecSSLik_t;
	typedef vecSSLik_t::iterator itVecPtrLS_t;

	MatrixScalingNode *mScaling;
	vecSSLik_t vecSSLik;
	const DL::CompressedAlignements *ptrCA;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

	void linkLikSite(RootNode *node);

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINESITENODELIGHT_H_ */
