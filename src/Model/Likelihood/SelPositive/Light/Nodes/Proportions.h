//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proportions.h
 *
 * @date Feb 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef PROPORTIONSLIGHT_H_
#define PROPORTIONSLIGHT_H_

#include <math.h>
#include <vector>

#include "Types.h"
#include "stddef.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class Proportions {
public:
	Proportions();
	Proportions(const TI_TYPE aP0, const TI_TYPE aP1);
	~Proportions();

	void set(const TI_TYPE aP0, const TI_TYPE aP1);
	TI_TYPE getP0() const;
	TI_TYPE getP1() const;
	TI_TYPE getProportion(const size_t idx) const;
	const std::vector<TI_TYPE>& getProportions() const;

private:

	enum proportions_t {ORIGINAL, NEW};
	static const proportions_t PROP_TYPE;

	TI_TYPE p0, p1;
	std::vector<TI_TYPE> proportions;

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PROPORTIONSLIGHT_H_ */
