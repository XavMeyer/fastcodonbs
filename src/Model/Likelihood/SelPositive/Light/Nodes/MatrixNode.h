//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.h
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXNODELIGHT_H_
#define MATRIXNODELIGHT_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/CodonModels/SingleOmegaMF.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;
namespace MU = ::MolecularEvolution::MatrixUtils;

class MatrixNode: public DAG::BaseNode {

public:
	MatrixNode(const MU::nuclModel_t aNuclModel, omegaClass_t aOmegaClass, DL_Utils::Frequencies &aFrequencies);
	~MatrixNode();

	void setKappa(const TI_TYPE aK);
	void setThetas(const std::vector<TI_TYPE> &aThetas);
	void setOmega(const TI_TYPE aW);

	const omegaClass_t getOmegaClass() const;
	const TI_TYPE getMatrixQScaling() const;
	const std::vector<TI_TYPE>& getMatrixQ() const;
	const TI_EigenEIGMatrix_t& getMatrixEigenQ() const ;
	const TI_EigenEIGMatrix_t& getMatrixA() const ;
	const TI_EigenEIGMatrix_t& getEigenVectors() const;
	const TI_EigenEIGMatrix_t& getScaledEigenVectors() const;
	const TI_EigenEIGVector_t& getEigenValues() const;

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

private:

	// Coefficients are [kappa, omega] or [thetas, omega]
	MU::nuclModel_t nuclModel;
	omegaClass_t omegaClass;
	std::vector<TI_TYPE> coefficients;
	DL_Utils::Frequencies &frequencies;

	TI_TYPE matrixScaling;
	MolecularEvolution::MatrixUtils::SingleOmegaMF soMF;
	TI_EigenEIGVector_t D;
	TI_EigenEIGMatrix_t A, Q, V, scaledV;

	void doProcessing();
	bool processSignal(BaseNode* aChild);
	void comprEIG();

	// DEBUG: void eigenLapack(const Eigen::MatrixXd& mat, Eigen::VectorXd &eigenD, Eigen::MatrixXd &eigenV);

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MATRIXNODELIGHT_H_ */
