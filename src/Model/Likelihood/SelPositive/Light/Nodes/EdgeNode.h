//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeNode.h
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef EDGENODELIGHT_H_
#define EDGENODELIGHT_H_

#include <stddef.h>

#include "BranchMatrixNode.h"
#include "CPV.h"
#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class BranchMatrixNode;
class CPV;

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class EdgeNode : public DAG::BaseNode {
public:
	EdgeNode(const bool aFgBranch, const bool aAfterFgBranch, const omegaClass_t aOmegaClass,
			 const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies);
	virtual ~EdgeNode();

	const CPV* getPtrCPV(const size_t aSitePos, const siteClass_t aSiteClass) const;
	const TI_EigenMatrixDyn_t& getH() const;
	const TI_EigenEIGVector_t& getNorm() const;
	omegaClass_t getOmegaClass() const;

	bool isFGBranch() const;
	bool isAfterFGBranch() const;

	bool hasEqualVecCPV(const EdgeNode &other) const;

	size_t compressVecCPV();

	size_t getNCompressed() const;

public:
	enum scaling_t {NONE, NORMAL, LOG, LOG_ACCURATE};
	static const scaling_t SCALING_TYPE;
	static const float SCALING_THRESHOLD;

protected:

	typedef std::vector<CPV> vecCPV_t; // CPV vector
	typedef vecCPV_t::const_iterator cstItVecCPV_t; // Associated const iterator

	const bool fgBranch, afterFgBranch;
	const omegaClass_t omegaClass;
	DL_Utils::Frequencies &frequencies;
	BranchMatrixNode *bmNode;
	TI_EigenMatrixDyn_t H;
	TI_EigenEIGVector_t nrm;

	vecCPV_t vecCPV;

	void init(const std::vector<size_t> &sitePos);
};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* EDGENODELIGHT_H_ */
