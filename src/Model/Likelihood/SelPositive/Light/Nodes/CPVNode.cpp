//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVNode.cpp
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#include "CPVNode.h"

#include "Model/Likelihood/SelPositive/Light/Nodes/BranchMatrixNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

CPVNode::CPVNode(const bool aFgBranch, const bool aAfterFgBranch, const omegaClass_t aOmegaClass,
		         const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies, const bool compress) :
		        		 ParentNode(aFgBranch, aAfterFgBranch, aOmegaClass, aSitePos, aFrequencies, compress) {

}

CPVNode::~CPVNode() {
}

void CPVNode::doProcessing() {

	assert(bmNode);
	const TI_EigenSquareMatrix_t &Z = bmNode->getZ();
	const TI_EigenVector_t &invPi = frequencies.getInvEigen();

	TI_EigenMatrix_t G = TI_EigenMatrix_t::Zero(H.rows(), H.cols());


	//std::cout << "CPV Here 1" << std::endl;

	if(SCALING_TYPE == LOG_ACCURATE) {
		//nrm.setZero();
		nrm.resize(0);
	}

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<hChildrenBlock.size(); ++iE) {
		if(iE == 0) {
			G = hChildrenBlock[iE]->getH();
			first = false;
			if(SCALING_TYPE == NORMAL || SCALING_TYPE == LOG) {
				nrm = hChildrenBlock[iE]->getNorm();
			} else if(SCALING_TYPE == LOG_ACCURATE  && hChildrenBlock[iE]->getNorm().size() > 0) {
				nrm = hChildrenBlock[iE]->getNorm();
			}

		} else {
			G = G.cwiseProduct(hChildrenBlock[iE]->getH());
			if(SCALING_TYPE == NORMAL) {
				nrm = nrm.cwiseProduct(hChildrenBlock[iE]->getNorm());
			} else if (SCALING_TYPE == LOG) {
				nrm += hChildrenBlock[iE]->getNorm();
			} else if(SCALING_TYPE == LOG_ACCURATE  && hChildrenBlock[iE]->getNorm().size() > 0) {
				if(nrm.size() == 0) {
					nrm = hChildrenBlock[iE]->getNorm();
				} else {
					nrm += hChildrenBlock[iE]->getNorm();
				}
			}
		}
	}

	//std::cout << "CPV Here 2" << std::endl;

	// Check column wise
	for(size_t iH=0; iH<hChildrenPos.size(); ++iH) {
		for(size_t iC=0; iC<hChildrenPos[iH].size(); ++iC) {
			const TI_EigenMatrixDyn_t &tmpH = hChildrenPos[iH][iC].node->getH();
			const TI_EigenVector_t &tmpV = tmpH.col(hChildrenPos[iH][iC].cpv->getIndex());
			const TI_EigenVector_t &tmpNrm = hChildrenPos[iH][iC].node->getNorm();

			if(first && iC==0) { // first element : copy
				G.col(iH) = tmpV;
				if(SCALING_TYPE == NORMAL || SCALING_TYPE == LOG) {
					nrm(iH) = tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				} else if(SCALING_TYPE == LOG_ACCURATE  && tmpNrm.size() > 0) {
					if(nrm.size() == 0) {
						nrm = TI_EigenVector_t::Zero(vecCPV.size());
					}
					nrm(iH) += tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				}
			} else { // then : cwiseProd
				G.col(iH) = G.col(iH).cwiseProduct(tmpV);
				if(SCALING_TYPE == NORMAL) {
					nrm(iH) = nrm(iH)*tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				} else if(SCALING_TYPE == LOG) {
					nrm(iH) += tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				} else if(SCALING_TYPE == LOG_ACCURATE  && tmpNrm.size() > 0) {
					if(nrm.size() == 0) {
						nrm = TI_EigenVector_t::Zero(vecCPV.size());
					}
					nrm(iH) += tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
				}
			}
		}
	}

	//std::cout << "CPV Here 3" << std::endl;
	//std::cout << invPi << std::endl;

	// Multiplication
	H = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);

	//std::cout << "CPV Here 5" << std::endl;


	if(SCALING_TYPE == NORMAL) {
		for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
			TI_TYPE tmpNrm = H.col(iC).norm();
			if(tmpNrm > 0.) {
				nrm(iC) = nrm(iC)*tmpNrm;
				H.col(iC) = H.col(iC) / tmpNrm;
			}
		}
	} else if(SCALING_TYPE == LOG) {
		for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
			TI_TYPE tmpNrm = H.col(iC).norm();
			if(tmpNrm > 0.) {
				nrm(iC) = nrm(iC)+log(tmpNrm);
				H.col(iC) = H.col(iC) / tmpNrm;
			}
		}
	} else if(SCALING_TYPE == LOG_ACCURATE) {
	#if TI_USE_STAN
		assert(false && "LOG_ACCURATE scaling not working due to frexp and ldexp operations on AD variables.");
	#else
		TI_EigenVector_t vecMax = H.colwise().maxCoeff();
		if(SCALING_THRESHOLD >= 1. || vecMax.minCoeff() < SCALING_THRESHOLD) {
			if(nrm.size() == 0) {
				nrm = Eigen::VectorXd::Zero(vecMax.size());
			}
			for(size_t iC=0; iC<(size_t)vecMax.size(); ++iC){
				if(vecMax(iC) > 0 && (SCALING_THRESHOLD >= 1. || vecMax(iC) < SCALING_THRESHOLD)){
					int exponent;
					frexp(vecMax(iC), &exponent);
					nrm(iC) += exponent*LOG_OF_2;
					H.col(iC) /= ldexp((float)1., exponent); // TODO CHECK IF CORRECT
				}
			}
		}
	#endif
	}
	//std::cout << "Here 5" << std::endl;

}

bool CPVNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixNode)){
		bmNode = dynamic_cast<BranchMatrixNode*>(aChild);
		if(bmNode->getOmegaClass() != omegaClass){
			std::cerr << "void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixNode children. Found Omega_" << bmNode->getOmegaClass();
			std::cerr << " while waiting for omega_" <<  omegaClass << std::endl;
			abort();
		}
	} else if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		linkChildren(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		linkChildren(leafNode);
	}
}


} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
