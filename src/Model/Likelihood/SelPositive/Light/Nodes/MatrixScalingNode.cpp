//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixScalingNode.cpp
 *
 * @date Feb 12, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixScalingNode.h"

#include "Model/Likelihood/SelPositive/Light/Nodes/MatrixNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Proportions.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

MatrixScalingNode::MatrixScalingNode() : DAG::BaseNode(), proportions() {

	useFixedScalingForBEB = false;
	p0 = p1 = 0.;
	scalingFG =	scalingBG = 0.;

	Q0 = Q1 = Q2 = NULL;
}

MatrixScalingNode::~MatrixScalingNode() {
}

TI_TYPE MatrixScalingNode::getScalingFG() const {
	return scalingFG;
}

TI_TYPE MatrixScalingNode::getScalingBG() const {
	return scalingBG;
}

const Proportions& MatrixScalingNode::getProportions() const {
	return proportions;
}

void MatrixScalingNode::setP0(const TI_TYPE aP0) {
	if(p0 != aP0) {
		p0 = aP0;
		DAG::BaseNode::updated();
	}
}

void MatrixScalingNode::setP1(const TI_TYPE aP1) {
	if(p1 != aP1) {
		p1 = aP1;
		DAG::BaseNode::updated();
	}
}

void MatrixScalingNode::setFixedScalingForBEB() {
	useFixedScalingForBEB = true;
}

void MatrixScalingNode::setFixedScalingForBEB(TI_TYPE aScalFG, TI_TYPE aScalBG) {
	useFixedScalingForBEB = true;
	scalingFG = aScalFG;
	scalingBG = aScalBG;
}

void MatrixScalingNode::resetFixedScalingForBEB() {
	useFixedScalingForBEB = false;
}


void MatrixScalingNode::doProcessing() {
	proportions.set(p0, p1);

	// If we are not in fixed scaling mode (used for BEB) we refresh the scaling
	if(!useFixedScalingForBEB) {
		// BG scaling
		scalingBG = proportions.getP1()*Q0->getMatrixQScaling();
		scalingBG += (1.-proportions.getP1())*Q1->getMatrixQScaling();
		//scalingBG = proportions.getProportion(0)*Q0->getMatrixQScaling();
		//scalingBG += proportions.getProportion(1)*Q1->getMatrixQScaling();
		//scalingBG += proportions.getProportion(2)*Q0->getMatrixQScaling();
		//scalingBG += proportions.getProportion(3)*Q1->getMatrixQScaling();

		// FG scaling
		scalingFG = proportions.getProportion(0)*Q0->getMatrixQScaling();
		scalingFG += proportions.getProportion(1)*Q1->getMatrixQScaling();
		scalingFG += (1.-proportions.getP0())*Q2->getMatrixQScaling();
		/*scalingFG = proportions.getProportion(0)*Q0->getMatrixQScaling();
		scalingFG += proportions.getProportion(1)*Q1->getMatrixQScaling();
		scalingFG += proportions.getProportion(2)*Q2->getMatrixQScaling();
		scalingFG += proportions.getProportion(3)*Q2->getMatrixQScaling();
		std::cout << scalingFG << "\t" << scalingBG << std::endl;*/
	}
}

bool MatrixScalingNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void MatrixScalingNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		MatrixNode* matrixNode = dynamic_cast<MatrixNode*>(aChild);

		if(matrixNode->getOmegaClass() == OMEGA_0) {
			Q0 = matrixNode;
		} else if(matrixNode->getOmegaClass() == OMEGA_1) {
			Q1 = matrixNode;
		} else if(matrixNode->getOmegaClass() == OMEGA_2) {
			Q2 = matrixNode;
		}

	} else {
		std::cerr << "Child nodes should be MatrixNode only." << std::endl;
		abort();
	}
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
