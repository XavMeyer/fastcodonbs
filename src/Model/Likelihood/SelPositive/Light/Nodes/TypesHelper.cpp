//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Helper.cpp
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#include "Types.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

bool isOmegaValidForClass(const omegaClass_t omegaClass, const siteClass_t siteClass) {

	switch (omegaClass) {
		case OMEGA_0:
			return (siteClass == CLASS_0) || (siteClass == CLASS_2a);
		case OMEGA_1:
			return (siteClass == CLASS_1) || (siteClass == CLASS_2b);
		case OMEGA_2:
			return (siteClass == CLASS_2a) || (siteClass == CLASS_2b);
	}
	return false;
}

bool isOmegaValidForClass(const omegaClass_t omegaClass, const siteClass_t siteClass, const bool isFGBranch) {

	switch (omegaClass) {
		case OMEGA_0:
			return (siteClass == CLASS_0) || (!isFGBranch && (siteClass == CLASS_2a));
		case OMEGA_1:
			return (siteClass == CLASS_1) || (!isFGBranch && (siteClass == CLASS_2b));
		case OMEGA_2:
			return isFGBranch && ((siteClass == CLASS_2a) || (siteClass == CLASS_2b));
	}
	return false;
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
