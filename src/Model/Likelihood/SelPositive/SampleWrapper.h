//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.h
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief
 */
#ifndef SAMPLEWRAPPER_H_
#define SAMPLEWRAPPER_H_

#include <stddef.h>
#include <iostream>
#include <sstream>
#include <vector>

#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {

namespace MU = ::MolecularEvolution::MatrixUtils;

namespace Base {
	class SelPositive;
}

namespace Light {
	class SelPositive;
}

namespace LightNoScaling {
	class SelPositive;
}

class SampleWrapper {
	friend class Base::SelPositive;
	friend class Light::SelPositive;
	friend class LightNoScaling::SelPositive;
public:
	SampleWrapper(const bool aH1, const std::vector<double> &sample);
	SampleWrapper(const MU::nuclModel_t aNuclModel, const bool aH1, const std::vector<double> &sample);
	~SampleWrapper();

	std::string toString() const;

	std::vector<TI_TYPE> getParameters() const;

private:
	static const double ONE;
	static const size_t N_KAPPA, N_THETA;

	const bool H1;
	const TI_TYPE K;
	std::vector< TI_TYPE > thetas;
	const TI_TYPE P0;
	const TI_TYPE P1;
	const TI_TYPE W0;
	const TI_TYPE W2;
	const size_t N_P;
	const size_t N_BL;
	std::vector< TI_TYPE > branchLength;

	size_t offset(const MU::nuclModel_t aNuclModel);

};

} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SAMPLEWRAPPER_H_ */
