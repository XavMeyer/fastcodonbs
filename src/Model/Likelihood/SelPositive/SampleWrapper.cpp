//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.cpp
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief
 */
#include "SampleWrapper.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {

const double SampleWrapper::ONE = 1.0;
const size_t SampleWrapper::N_KAPPA = 1;
const size_t SampleWrapper::N_THETA = 5;

SampleWrapper::SampleWrapper(const bool aH1, const std::vector<double> &sample) :
	H1(aH1), K(sample[0]), P0(sample[1]), P1(sample[2]), W0(sample[3]),
	W2(sample[4]), N_P(5), N_BL(sample.size()-N_P) {

	for(size_t iBL = 0; iBL < N_BL; ++iBL) {
		branchLength.push_back(sample[N_P+iBL]);
	}

}

SampleWrapper::SampleWrapper(const MU::nuclModel_t aNuclModel, const bool aH1, const std::vector<double> &sample) :
		H1(aH1), K(sample[0]), P0(sample[offset(aNuclModel)+1]), P1(sample[offset(aNuclModel)+2]), W0(sample[offset(aNuclModel)+3]),
		W2(sample[offset(aNuclModel)+4]), N_P(5+offset(aNuclModel)), N_BL(sample.size()-N_P) {

		if(aNuclModel == MU::GTR) {
			for(size_t iT=0; iT<N_THETA; iT++) {
				thetas.push_back(sample[iT]);
			}
		}

		for(size_t iBL = 0; iBL < N_BL; ++iBL) {
			branchLength.push_back(sample[N_P+iBL]);
		}
}

SampleWrapper::~SampleWrapper() {
}

std::string SampleWrapper::toString() const {
	std::stringstream ss;
	ss.precision(4);
	ss << std::fixed;
	ss << "P0 = " << P0 << "\t";
	ss << "P1 = " << P1 << "\t";
	ss << "K = " << K << "\t";
	ss << "W0 = " << W0 << "\t";
	ss << "W2 = " << W2 << std::endl;
	for(size_t i=0; i<N_BL; ++i) {
		ss << branchLength[i] << "\t";
	}

	return ss.str();
}

std::vector<TI_TYPE> SampleWrapper::getParameters() const {

	std::vector<TI_TYPE> vars;

	if(thetas.size() > 0) {
		for(size_t iT=0; iT<thetas.size(); iT++) {
			vars.push_back(thetas[iT]);
		}
	} else {
		vars.push_back(K);
	}

	vars.push_back(P0);
	vars.push_back(P1);
	vars.push_back(W0);
	vars.push_back(W2);

	for(size_t iBL = 0; iBL < N_BL; ++iBL) {
		vars.push_back(branchLength[iBL]);
	}

	return vars;
}



size_t SampleWrapper::offset(const MU::nuclModel_t aNuclModel) {
	if(aNuclModel == MU::GTR) {
		return N_THETA-1;
	} else if(aNuclModel == MU::K80) {
		return N_KAPPA-1;
	} else {
		std::cerr << "Error : size_t SampleWrapper::offset(const MU::nuclModel_t aNuclModel);"<< std::endl;
		std::cerr << "Nucleotide model not supported."<< std::endl;
		abort();
		return 0;
	}
}

} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
