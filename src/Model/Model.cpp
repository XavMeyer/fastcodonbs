//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Model.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Model.h"

#include "Model/Likelihood/LikelihoodInterface.h"

namespace StatisticalModel {


Model::Model(LikelihoodInterface::sharedPtr_t aProc) : proc(aProc){
}

Model::~Model() {
}


Sampler::Sample Model::getRandomSample() const {
	return params.generateBaseRandomSample();
}

double Model::processPriorValue(const Sampler::Sample &sample) const {
	if(proc->isLog()){
		double paramPrior = params.processLogPriorValue(sample);
		double likSpecificPrior = proc->processLikelihoodSpecificPrior(sample);
		return paramPrior+likSpecificPrior;
	} else {
		double paramPrior = params.processPriorValue(sample);
		double likSpecificPrior = proc->processLikelihoodSpecificPrior(sample);
		return paramPrior*likSpecificPrior;
	}
}

Parameters& Model::getParams() {
	return params;
}

const Parameters& Model::getParams() const {
	return params;
}

LikelihoodInterface::sharedPtr_t Model::getLikelihood() {
	return proc;
}

const LikelihoodInterface::sharedPtr_t Model::getLikelihood() const {
	return proc;
}

void Model::processSample(std::vector<Sampler::Sample> &proposedSamples) {
	for(size_t iS=0; iS<proposedSamples.size(); ++iS){
		// Process prior value
		proposedSamples[iS].prior = processPriorValue(proposedSamples[iS]);
		if(isPriorPossible(proposedSamples[iS])){
			// Then we process the likelihood value
			if(!proposedSamples[iS].isEvaluated()) {
				proposedSamples[iS].likelihood = proc->processLikelihood(proposedSamples[iS]);
				proposedSamples[iS].setEvaluated(true);
			}
			proposedSamples[iS].setMarkers(proc->getMarkers());

			proposedSamples[iS].posterior = processPosterior(proposedSamples[iS]);
		}
	}
}

void Model::processSample(const vector<size_t> &pInd, std::vector<Sampler::Sample> &proposedSamples) {
	if(proposedSamples.size() == 1) {
		// Process prior value
		proposedSamples.back().prior = processPriorValue(proposedSamples.back());
		if(isPriorPossible(proposedSamples.back())){
			// Then we process the likelihood value
			if(!proposedSamples.back().isEvaluated()) {
				proposedSamples.back().likelihood = proc->update(pInd, proposedSamples.back());
				proposedSamples.back().setEvaluated(true);
			}
			proposedSamples.back().setMarkers(proc->getMarkers());

			proposedSamples.back().posterior = processPosterior(proposedSamples.back());
		}
	} else {
		for(size_t iS=0; iS<proposedSamples.size(); ++iS){
			// Process prior value
			proposedSamples[iS].prior = processPriorValue(proposedSamples[iS]);
			if(isPriorPossible(proposedSamples[iS])){
				// Then we process the likelihood value
				if(!proposedSamples[iS].isEvaluated()) {
					proposedSamples[iS].likelihood = proc->update(pInd, proposedSamples[iS]);
					proposedSamples[iS].setEvaluated(true);
				}
				proposedSamples[iS].setMarkers(proc->getMarkers());

				proposedSamples[iS].posterior = processPosterior(proposedSamples[iS]);
			}
		}
	}
}

double Model::processPosterior(const Sampler::Sample &sample) const {
	if(proc->isLog()){
		return sample.prior + sample.likelihood;
	} else {
		return sample.prior * sample.likelihood;
	}
}

bool Model::isPriorPossible(Sampler::Sample &sample) const {
	if(proc->isLog()){
		if(sample.prior == -numeric_limits<double>::infinity()) {
			sample.likelihood = -numeric_limits<double>::infinity();
			sample.posterior = -numeric_limits<double>::infinity();
			return false;
		}
	} else {
		if(sample.prior == 0.){
			sample.likelihood = 0.;
			sample.posterior = 0.;
			return false;
		}
	}

	return true;
}


} // namespace StatisticalModel
