//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParameterType.h
 *
 * @date Feb 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef PARAMETERTYPE_H_
#define PARAMETERTYPE_H_

namespace StatisticalModel {
	enum enumParameterType {TREE_PARAM=0, RATE_PARAM=1, BRANCH_LENGTH=2, COEV_PARAM=3, TIME_BIN_PARAM=4, VARIOUS_PARAM=999};
	typedef enumParameterType parameterType_t;
}

#endif /* PARAMETERTYPE_H_ */
