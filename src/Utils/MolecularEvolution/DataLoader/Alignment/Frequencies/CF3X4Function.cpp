//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CF3X4Function.cpp
 *
 * @date Jul 9, 2015
 * @author meyerx
 * @brief
 */
#include "CF3X4Function.h"

#include "Utils/MolecularEvolution/Definitions/Codons.h"
#include "Utils/MolecularEvolution/Definitions/Nucleotides.h"

namespace MolecularEvolution {
namespace DataLoader {

const size_t CF3X4_Function::N = 9;

CF3X4_Function::CF3X4_Function(const std::vector<std::vector<double> > &aF3x4) :
		ObjectiveFunctionInterface(N, MINIMIZE), F3x4(aF3x4)  {

	const size_t NB_BASE_NUCL = Nucleotides::getInstance()->NB_BASE_NUCL;
	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;

	CF3x4.resize(NB_NUCL_PER_CODON);
	for(size_t iP=0; iP<NB_NUCL_PER_CODON; ++iP) {
		CF3x4[iP].assign(NB_BASE_NUCL, 0.);
	}

}

CF3X4_Function::~CF3X4_Function() {
}

void CF3X4_Function::computeMatrixN(const Eigen::VectorXd &values, vec2D_t &N) {

	const size_t NB_BASE_NUCL = Nucleotides::getInstance()->NB_BASE_NUCL;
	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;

	// Initialize P
	// std::cout << "Process P" << std::endl;
	vec2D_t P;
	P.resize(NB_NUCL_PER_CODON);
	for(size_t i=0; i<NB_NUCL_PER_CODON; ++i) {
		P[i].assign(NB_NUCL_PER_CODON, 0.);
		for(size_t j=0; j<NB_NUCL_PER_CODON; ++j) {
			P[i][j] = values[i*NB_NUCL_PER_CODON+j];
			//std::cout << P[i][j] << "\t";
		}
		//std::cout << std::endl;
	}
	//std::cout << std::endl;

	// initialize N
	// std::cout << "Process N" << std::endl;
	N.resize(NB_NUCL_PER_CODON);
	for(size_t i=0; i<NB_NUCL_PER_CODON; ++i) {
		N[i].assign(NB_BASE_NUCL, 0.);
	}
	for(size_t i=0; i<NB_NUCL_PER_CODON; ++i) {
		N[i][0] = P[i][0]; // A
		N[i][1] = (1.-P[i][0])*P[i][1]; // C
		N[i][2] = (1.-P[i][0])*(1.-P[i][1])*P[i][2]; // T
		N[i][3] = (1.-P[i][0])*(1.-P[i][1])*(1.-P[i][2]); // G
	}

	/*for(size_t iC=0; iC<NB_NUCL_PER_CODON; ++iC) {
		for(size_t jC=0; jC<NB_BASE_NUCL; ++jC) {
			std::cout << N[iC][jC] << "\t";
		}
		std::cout <<  std::endl;
	}*/

}

double CF3X4_Function::computeMatrixSDEF(const vec2D_t &N, vec2D_t &SDEF) {

	const size_t NB_BASE_NUCL = Nucleotides::getInstance()->NB_BASE_NUCL;
	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;
	const size_t NB_CODONS = Codons::getInstance()->NB_CODONS;
	const size_t NB_CODONS_WO_STOP = Codons::getInstance()->NB_CODONS_WO_STOP;

	SDEF.resize(NB_NUCL_PER_CODON);
	for(size_t i=0; i<NB_NUCL_PER_CODON; ++i) {
		SDEF[i].assign(NB_BASE_NUCL, 0.);
	}

	double S = 1.;
	for(size_t iC=NB_CODONS_WO_STOP; iC<NB_CODONS; ++iC) {
		// SUM
		double freq = 1.;
		std::string stopCodon = Codons::getInstance()->getCodonStr(iC);
		for(size_t jC=0; jC<NB_NUCL_PER_CODON; ++jC) {
			size_t iNucl = Nucleotides::getInstance()->getNucleotideIdx(stopCodon[jC]);
			freq *= N[jC][iNucl];
		}
		S -= freq;

		// CORRECTION
		size_t iNucl0 = Nucleotides::getInstance()->getNucleotideIdx(stopCodon[0]);
		size_t iNucl1 = Nucleotides::getInstance()->getNucleotideIdx(stopCodon[1]);
		size_t iNucl2 = Nucleotides::getInstance()->getNucleotideIdx(stopCodon[2]);

		SDEF[0][iNucl0] -= N[1][iNucl1] * N[2][iNucl2];
		SDEF[1][iNucl1] -= N[0][iNucl0] * N[2][iNucl2];
		SDEF[2][iNucl2] -= N[0][iNucl0] * N[1][iNucl1];
	}

	return S;
}

double CF3X4_Function::doComputeObjectiveValue(const Eigen::VectorXd &values) {
	const size_t NB_BASE_NUCL = Nucleotides::getInstance()->NB_BASE_NUCL;
	const size_t NB_NUCL_PER_CODON = Codons::getInstance()->NB_NUCL_PER_CODON;

	nFuncCall++;

	// Define matrix N
	vec2D_t N;
	computeMatrixN(values, N);

	// Define matrix SDEF
	vec2D_t SDEF, RES;
	double S = computeMatrixSDEF(N, SDEF);

	// Define matrix RES
	// Corrected F3x4
	//std::cout << "Process Corrected F3x4" << std::endl;

	RES.resize(NB_NUCL_PER_CODON);
	for(size_t iP=0; iP<NB_NUCL_PER_CODON; ++iP) {
		RES[iP].assign(NB_BASE_NUCL, 0.);
	}

	for(size_t iP=0; iP<NB_NUCL_PER_CODON; ++iP) {
		for(size_t jC=0; jC<NB_BASE_NUCL; ++jC) {
			RES[iP][jC] = (N[iP][jC]*(1.+SDEF[iP][jC]))/S;
		}
	}

	/*for(size_t iC=0; iC<NB_NUCL_PER_CODON; ++iC) {
		for(size_t jC=0; jC<NB_BASE_NUCL; ++jC) {
			std::cout << RES[iC][jC] << "\t";
		}
		std::cout <<  std::endl;
	}*/

	// Error
	//std::cout << "Process Error" << std::endl;
	double error = 0.;
	for(size_t iP=0; iP<NB_NUCL_PER_CODON; ++iP) {
		for(size_t jC=0; jC<NB_BASE_NUCL; ++jC) {
			double tmp = RES[iP][jC] - F3x4[iP][jC];
			error += tmp*tmp;
		}
	}
	//std::cout << "Error : " << error << std::endl;
	CF3x4 = N;

	return error;
}

void CF3X4_Function::doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient) {
	defaultComputeGradient(objective, values, gradient);
}

double CF3X4_Function::getLowerBound(size_t idxParameter) const {
	return 0.;
}

double CF3X4_Function::getUpperBound(size_t idxParameter) const {
	return 1.;
}

const std::vector<std::vector<double> > & CF3X4_Function::getCF3x4() const {
	return CF3x4;
}

} /* namespace DataLoader */
} /* namespace MolecularEvolution */
