//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CF3X4Function.h
 *
 * @date Jul 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef CF3X4FUNCTION_H_
#define CF3X4FUNCTION_H_

#include <stddef.h>
#include <vector>

#include "../MSA.h"
#include "Utils/Maximizer/Custom/ObjectiveFunction/ObjectiveFunctionInterface.h"

namespace MolecularEvolution { namespace Definition { class Codons; } }
namespace MolecularEvolution { namespace Definition { class Nucleotides; } }

namespace MolecularEvolution {
namespace DataLoader {

using ::Utils::Maximizer::Custom::ObjectiveFunctionInterface;
using MolecularEvolution::Definition::Nucleotides;
using MolecularEvolution::Definition::Codons;

class CF3X4_Function: public ObjectiveFunctionInterface {
public:
	CF3X4_Function(const std::vector<std::vector<double> > &aF3x4);
	~CF3X4_Function();

	double getLowerBound(size_t idxParameter) const;
	double getUpperBound(size_t idxParameter) const;

	const std::vector<std::vector<double> > & getCF3x4() const;

private:

	typedef std::vector<std::vector<double> > vec2D_t;

	static const size_t N;
	const vec2D_t &F3x4;
	vec2D_t CF3x4;

	double doComputeObjectiveValue(const Eigen::VectorXd &values);
	void doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient);

	void computeMatrixN(const Eigen::VectorXd &values, vec2D_t &N);
	double computeMatrixSDEF(const vec2D_t &N, vec2D_t &SDEF);
};

} /* namespace DataLoader */
} /* namespace MolecularEvolution */

#endif /* CF3X4FUNCTION_H_ */
