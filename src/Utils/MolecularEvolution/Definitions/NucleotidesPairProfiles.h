//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotidesPairProfiles.h
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#ifndef NUCLEOTIDESPAIRPROFILES_H_
#define NUCLEOTIDESPAIRPROFILES_H_

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "Types.h"

namespace MolecularEvolution {
namespace Definition {

typedef unsigned short int idProfile_t;

class NucleotidesPairProfiles {
public:
	const vecProfiles_t ALL_POSSIBLE_PROFILES;
	const size_t NB_POSSIBLE_PROFILES;
public:

	static const NucleotidesPairProfiles* getInstance();

	std::vector<idProfile_t> getPossibleProfilesId(bitset16b_t observedPairs) const;
	vecProfiles_t getPossibleProfiles(bitset16b_t observedPairs) const;

	profile_t idToProfile(idProfile_t idProfile) const;
	std::string profileToString(profile_t profile) const;

private:
	static NucleotidesPairProfiles*	instance;

	NucleotidesPairProfiles();
	~NucleotidesPairProfiles();

	typedef std::map<profile_t, idProfile_t> mapProfilesId_t;
	const mapProfilesId_t MAP_PROFILES_ID;

	vecProfiles_t initAllPossibleProfile() const;
	mapProfilesId_t initMapProfilesId(const vecProfiles_t &vecProfiles) const;

	void recursiveProfileBuilding(size_t iObserved, size_t iLevel, size_t maxLevel,
								  bitset16b_t observedPairs, bitset16b_t inProfile,
								  vecProfiles_t &vecProfiles) const;
};

} /* namespace Definition */
} /* namespace MolecularEvolution */

#endif /* NUCLEOTIDESPAIRPROFILES_H_ */
