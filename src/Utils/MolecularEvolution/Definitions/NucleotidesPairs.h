//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file NucleotidesPairs.h
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#ifndef NUCLEOTIDESPAIRS_H_
#define NUCLEOTIDESPAIRS_H_

#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>

#include "Types.h"

namespace MolecularEvolution {
namespace Definition {

class NucleotidesPairs {
public:

	static const NucleotidesPairs* getInstance();



	std::string getNuclPairString(size_t nuclPairId) const;
	std::vector<std::string> getNuclPairsString(bitset16b_t codedPairs) const;

	std::string codedIdPairsToString(bitset16b_t codePairNucl) const;
	bitset16b_t stringToCodedIdPairs(std::string pairString) const;

public:
	const std::vector<std::string> NUCL_PAIR;
	const size_t NB_NUCL_PAIR;

private:

	static NucleotidesPairs* instance;

	NucleotidesPairs();
	~NucleotidesPairs();

	std::vector<std::string> initNuclPairs() const;

};

} /* namespace Definition */
} /* namespace MolecularEvolution */

#endif /* NUCLEOTIDESPAIRS_H_ */
