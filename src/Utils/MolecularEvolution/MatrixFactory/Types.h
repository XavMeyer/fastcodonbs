//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixFactory.h
 *
 * @date Feb 3, 2015
 * @author meyerx
 * @brief
 */

#ifndef MATRIX_FACTORY_TYPES_H_
#define MATRIX_FACTORY_TYPES_H_

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Sparse"

#if TI_USE_STAN
#include "stan/math.hpp"
#endif


namespace MolecularEvolution {
namespace MatrixUtils {

//#define TI_USE_STAN 			1

  #if TI_USE_STAN
  	#define TI_TYPE					stan::math::var
  #else
  	#define TI_TYPE					double
  #endif

}
}

#endif
