//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SharedMatrixFactories.h
 *
 * @date Mar 3, 2017
 * @author meyerx
 * @brief
 */
#ifndef SHAREDMATRIXFACTORIES_H_
#define SHAREDMATRIXFACTORIES_H_

#include <vector>
#include <assert.h>
#include <pthread.h>

namespace MolecularEvolution {
namespace MatrixUtils {

template <class matrixType>
class SharedMatrixFactories {
public:
	SharedMatrixFactories(size_t aNThreads) :
		nThread(aNThreads), freeMatrices(nThread, true) {
		pthread_mutex_init(&mutex, NULL);
	}

	~SharedMatrixFactories() {
		for(size_t i=0; i<matrices.size(); ++i) {
			delete matrices[i];
		}
	}


	void addMatrix(matrixType* aMatrix) {
		matrices.push_back(aMatrix);
	}

	std::pair<int, matrixType*> getMatrix() {

		assert(matrices.size() == nThread && "Matrices should be added using the addMatrix function prior to access.");

		if(nThread == 1) {
			return std::make_pair(0, matrices.front());
		} else {
			int myMatrixId = -1;
			pthread_mutex_lock(&mutex);
			for(size_t i=0; i<nThread; ++i) {
				if(freeMatrices[i]) {
					myMatrixId=i;
					freeMatrices[i] = false;
					break;
				}
			}
			assert(myMatrixId >= 0);
			pthread_mutex_unlock(&mutex);

			return std::make_pair(myMatrixId, matrices[myMatrixId]);
		}
	}

	void releaseMatrix(std::pair<int, matrixType*> &aMatrix) {
		if(nThread > 1) {
			pthread_mutex_lock(&mutex);
			freeMatrices[aMatrix.first] = true;
			pthread_mutex_unlock(&mutex);
		}
	}

private:

	size_t nThread;
	std::vector<bool> freeMatrices;
	std::vector<matrixType*> matrices;

	pthread_mutex_t mutex;

};

} /* namespace MatrixUtils */
} /* namespace MolecularEvolution */

#endif /* SHAREDMATRIXFACTORIES_H_ */
