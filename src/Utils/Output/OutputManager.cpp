//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OutputManager.cpp
 *
 * @date Dec 13, 2016
 * @author meyerx
 * @brief
 */
#include "OutputManager.h"

namespace Utils {

OutpoutManager::OutpoutManager() : baseFileName("debug_"), verbosityThreshold(MEDIUM_VERB) {
}

OutpoutManager::~OutpoutManager() {
}

void OutpoutManager::setVerbosityThreshold(verboseLevel_t aVerbLvl) {
	verbosityThreshold = aVerbLvl;
}

verboseLevel_t OutpoutManager::getVerbosityThreshold() {
	return verbosityThreshold;
}

void OutpoutManager::setBaseFileName(const std::string& aBFName) {
	baseFileName = aBFName;
}

const std::string& OutpoutManager::getBaseFileName() const {
	return baseFileName;
}

void OutpoutManager::setPrefixFileName(const std::string& aPFName) {
	prefixFileName = aPFName;
}

const std::string& OutpoutManager::getPrefixFileName() const {
	return prefixFileName;
}

bool OutpoutManager::check(verboseLevel_t aVerbLvl) const {
	return verbosityThreshold >= aVerbLvl;
}

} /* namespace Utils */
