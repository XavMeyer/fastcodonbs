//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CheckpointOutputFiles.cpp
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */

#include "OutputFiles.h"

#include <assert.h>
#include <sstream>

namespace Utils {

OutputFiles::OutputFiles(const std::string &aFName) :
		fName(aFName) {

	std::stringstream ssLog;

	ssLog << fName << ".txt";
	std::string logFileName = ssLog.str();
	logFile.open(logFileName.c_str(), std::ios::out);

	std::stringstream ssBEB;
	ssBEB << fName << "_BEB.txt";
	std::string bebFileName = ssBEB.str();
	bebFile.open(bebFileName.c_str(), std::ios::out);

}

OutputFiles::~OutputFiles() {
}

std::ofstream& OutputFiles::getLogStream() {
	return logFile;
}

std::ofstream& OutputFiles::getBEBStream() {
	return bebFile;
}

} /* namespace Utils */
