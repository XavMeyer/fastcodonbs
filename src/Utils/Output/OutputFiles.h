//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OUTPUTFILES.h
 *
 * @date Jan 28, 2016
 * @author meyerx
 * @brief
 */
#ifndef OUTPUTFILES_H_
#define OUTPUTFILES_H_

#include <assert.h>
#include <boost/shared_ptr.hpp>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>

namespace Utils {

class OutputFiles {
public:
	typedef boost::shared_ptr< OutputFiles > sharedPtr_t;

public:
	OutputFiles(const std::string &aFName);
	~OutputFiles();

	std::ofstream& getLogStream();
	std::ofstream& getBEBStream();

private:

	std::string fName;
	std::ofstream logFile, bebFile;


};

} /* namespace Utils */

#endif /* OUTPUTFILES_H_ */
