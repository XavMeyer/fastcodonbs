//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HolmBonferroniREL.h
 *
 * @date Feb 13, 2017
 * @author meyerx
 * @brief
 */
#ifndef HOLMBONFERRONIREL_H_
#define HOLMBONFERRONIREL_H_

#include <vector>

#include "Model/Likelihood/BranchSiteREL/BranchSiteRELik.h"
#include "Sampler/Samples/Sample.h"

using namespace ::StatisticalModel::Likelihood::BranchSiteREL;

namespace Utils {
namespace Statistics {
namespace Tests {

class HolmBonferroniREL {
public:
	HolmBonferroniREL(double aAlpha, const Sampler::Sample &aMLE, const std::vector<Sampler::Sample> &aNullHypotheses);
	~HolmBonferroniREL();

	void compute();

	std::string summarizeResult(std::string &strMLE, std::string &treeMLE, std::vector<string> &strNullHypothesis, std::vector<string> &treesNullHypothesis);

private:

	double alpha;

	Sampler::Sample MLE;
	std::vector<bool> rejH0;
	std::vector<double> pValues;
	std::vector<Sampler::Sample> nullHypotheses;

};

} /* namespace Tests */
} /* namespace Statistics */
} /* namespace Utils */

#endif /* HOLMBONFERRONIREL_H_ */
