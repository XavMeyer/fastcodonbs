//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PosSelectionBEB.h
 *
 * @date Oct 12, 2016
 * @author meyerx
 * @brief
 */
#ifndef POSSELECTIONBEB_H_
#define POSSELECTIONBEB_H_

#include <stddef.h>
#include <map>
#include <vector>

#include "Eigen/Core"
#include "Eigen/Dense"
#include "Model/Likelihood/SelPositive/Light/SelPositive.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { namespace Likelihood { namespace PositiveSelection { namespace Light { class SelPositive; } } } }

#define LOG_PLUS(x,y) (x>y ? x+log(1.+exp(y-x)) : y+log(1.+exp(x-y)))

using namespace StatisticalModel::Likelihood::PositiveSelection::Light;

namespace Utils {
namespace Statistics {
namespace Tests {

typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;

class PosSelectionBEB {
public:

	typedef struct {
		std::vector<double> w0, w2, Ps, siteProb;
	} result_t;

public:
	PosSelectionBEB(const Sampler::Sample &aSample, SelPositive* aPtrLik);
	~PosSelectionBEB();

	void computeBEB();

	result_t getResult();

	std::string summarizeResult();

private:

	typedef struct {
		double w0, w2;
		std::vector<TI_TYPE> pClass;
		TI_TYPE scaling, logLik;
	} point_t;

private:

	static const size_t N_PER_OMEGA, N_PER_PROPORTION;
	static const double MIN_W0, MAX_W0, MIN_W2, MAX_W2, STEP_W0, STEP_W2;

	Sampler::Sample MLE;
	SelPositive *ptrLik;

	TI_TYPE fX;
	std::vector<double> valW0, valW2;
	std::vector<TI_TYPE>	margW0, margW2, margP0P1;
	std::map< double, size_t > mapW0, mapW2;
	std::vector< point_t > gridPoints;
	std::vector< std::pair<TI_TYPE, TI_TYPE> > trianglePoints, pairOmega;
	std::map< std::pair<TI_TYPE, TI_TYPE>, size_t > triangleMap, pairOmegaMap;
	std::vector< TI_EigenMatrixDyn_t > likW0, likW1, likW2a, likW2b;
	TI_EigenMatrixDyn_t siteClassProbability;

	void init();
	void computeLikelihoods();
	void computeGridLikelihood();
	void normalizeMarginals();
	void computeSiteClassProbability();

	std::pair<double, double> getIndexTernary(size_t aTriangleId);

};

} /* namespace Tests */
} /* namespace Statistics */
} /* namespace Utils */

#endif /* POSSELECTIONBEB_H_ */
