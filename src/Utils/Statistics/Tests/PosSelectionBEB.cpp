//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PosSelectionBEB.cpp
 *
 * @date Oct 12, 2016
 * @author meyerx
 * @brief
 */
#include "PosSelectionBEB.h"

#include <assert.h>

#include "Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"
#include "Model/Likelihood/SelPositive/Light/SelPositive.h"

namespace Utils {
namespace Statistics {
namespace Tests {

const size_t PosSelectionBEB::N_PER_OMEGA = 10;
const size_t PosSelectionBEB::N_PER_PROPORTION = 10;

const double PosSelectionBEB::MIN_W0 = 0.0;
const double PosSelectionBEB::MAX_W0 = 1.0;
const double PosSelectionBEB::STEP_W0 = (MAX_W0-MIN_W0)/N_PER_OMEGA;

const double PosSelectionBEB::MIN_W2 = 1.0;
const double PosSelectionBEB::MAX_W2 = 11.0;
const double PosSelectionBEB::STEP_W2 = (MAX_W2-MIN_W2)/N_PER_OMEGA;

PosSelectionBEB::PosSelectionBEB(const Sampler::Sample &aSample, SelPositive* aPtrLik) :
		MLE(aSample), ptrLik(aPtrLik) {

	init();
}

PosSelectionBEB::~PosSelectionBEB() {
}

void PosSelectionBEB::init() {

	fX = 0.;

	// Init omegas values
	// W0 and W2
	valW0.push_back(STEP_W0/2.);
	valW2.push_back(1.0+STEP_W2/2.);
	mapW0.insert(std::make_pair(valW0.back(), valW0.size()-1));
	mapW2.insert(std::make_pair(valW2.back(), valW2.size()-1));
	for(size_t iW=1; iW<N_PER_OMEGA; ++iW) {
		valW0.push_back(valW0[iW-1]+STEP_W0);
		valW2.push_back(valW2[iW-1]+STEP_W2);
		mapW0.insert(std::make_pair(valW0.back(), valW0.size()-1));
		mapW2.insert(std::make_pair(valW2.back(), valW2.size()-1));
	}

	// Pairs of W's for class 2A
	for(size_t iW0=0; iW0<N_PER_OMEGA; ++iW0) {
		for(size_t iW2=0; iW2<N_PER_OMEGA; ++iW2) {
			pairOmega.push_back(std::make_pair(valW0[iW0], valW2[iW2]));
			pairOmegaMap.insert( std::make_pair(pairOmega.back(), pairOmega.size()-1) );
		}
	}

	// Triangles id
	for(size_t iPx=0; iPx<N_PER_PROPORTION; ++iPx) {
		for(size_t iPy=0; iPy<N_PER_PROPORTION; ++iPy) {
			trianglePoints.push_back(getIndexTernary(trianglePoints.size()));
			triangleMap.insert( std::make_pair(trianglePoints.back(), trianglePoints.size()-1 ) );
		}
	}

	// Define GridPoints
	for(size_t iW0=0; iW0<N_PER_OMEGA; ++iW0) {
		for(size_t iW2=0; iW2<N_PER_OMEGA; ++iW2) {
			for(size_t iT=0; iT<trianglePoints.size(); ++iT) {
				point_t point;
				// Omegas
				point.w0 = valW0[iW0];
				point.w2 = valW2[iW2];
				// Proportions
				TI_TYPE p0 = trianglePoints[iT].first;
				TI_TYPE p1 = trianglePoints[iT].second;
				point.pClass.push_back(p0);
				point.pClass.push_back(p1);
				TI_TYPE p2 = 1.-(p0+p1);
				TI_TYPE p2a = p2*p0/(p0+p1);
				TI_TYPE p2b = p2*p1/(p0+p1);
				point.pClass.push_back(p2a);
				point.pClass.push_back(p2b);
				// Liks
				point.logLik = point.scaling = 0.;
				// Add to grid
				gridPoints.push_back(point);
			}
		}
	}
}

void PosSelectionBEB::computeBEB() {

	// Compute the 121 required likelihoods on the tree
	computeLikelihoods();
	//std::cout << "Likelihood computed." << std::endl; // TODO DEBUG

	// From there define the grid likelihood, marginal, and evidence (f(X))
	computeGridLikelihood();
	//std::cout << "Grid computed." << std::endl; // TODO DEBUG

	// Normalize marginal
	normalizeMarginals();
	//std::cout << "Marginal computed." << std::endl; // TODO DEBUG

	// Compute site class probability
	computeSiteClassProbability();
	//std::cout << "Site class prob computed." << std::endl; // TODO DEBUG

}

PosSelectionBEB::result_t PosSelectionBEB::getResult() {
	result_t res;

	for(size_t iW=0; iW<margW0.size(); ++iW) {
		res.w0.push_back(exp(margW0[iW]));
	}

	for(size_t iW=0; iW<margW2.size(); ++iW) {
		res.w2.push_back(exp(margW2[iW]));
	}

	for(size_t iP=0; iP<trianglePoints.size(); ++iP) {
		res.Ps.push_back(exp(margP0P1[iP]));
	}

	for(size_t iS=0; iS<(size_t)siteClassProbability.cols(); ++iS) {
		res.siteProb.push_back(exp(siteClassProbability(2,iS)) + exp(siteClassProbability(3,iS)));
	}
	return res;
}

std::string PosSelectionBEB::summarizeResult() {
	std::stringstream sstr;

	sstr << "Marginal distributions : " << std::endl;

	sstr << "W0 : ";
	for(size_t iW=0; iW<margW0.size(); ++iW) {
		sstr << exp(margW0[iW]) << "\t";
	}
	sstr << std::endl;

	sstr << "W2 : ";
	for(size_t iW=0; iW<margW2.size(); ++iW) {
		sstr << exp(margW2[iW]) << "\t";
	}
	sstr << std::endl << std::endl;

	sstr << "P0-P1 : " << std::endl;
	for(size_t iP=0; iP<trianglePoints.size(); ++iP) {
		sstr << std::fixed << std::setw(6) << std::setprecision(3) << exp(margP0P1[iP]);
		int sq = static_cast<int>(sqrt(iP+1.));
		if(fabs(sq*sq-(iP+1.))<1e-5) sstr << std::endl;
	}
	//sstr << std::endl;

	sstr << "Site under positive selection : " << std::endl;
	for(size_t iS=0; iS<(size_t)siteClassProbability.cols(); ++iS) {
		TI_TYPE probClass2 = exp(siteClassProbability(2,iS)) + exp(siteClassProbability(3,iS));
		if(probClass2 > 0.5) {
			sstr << "\t" << 1+iS << "\t" << probClass2 << "\t";
			if(probClass2 >= 0.95 && probClass2 < 0.99) {
				sstr << "*";
			} else if (probClass2 >= 0.99) {
				sstr << "**";
			}
			sstr << std::endl;
		}
	}

	return sstr.str();
}



std::pair<double, double> PosSelectionBEB::getIndexTernary(size_t aTriangleId) {

	size_t ix = static_cast<size_t>(sqrt(static_cast<double>(aTriangleId)));
	size_t iy = aTriangleId - ix*ix;

	double px = (1. + floor(iy/2.)*3. + (iy%2))/(3.*N_PER_PROPORTION);
	double py = (1. + (N_PER_PROPORTION - 1 - ix)*3. + (iy%2))/(3.*N_PER_PROPORTION);

	return std::make_pair(px, py);
}


void PosSelectionBEB::computeLikelihoods() {

	// Offset from K80 or GTR (1 and 5) plus the proportions parameters (2)
	size_t offset = ptrLik->getNucleotideModel() == MU::GTR ? 5+2 : 1+2;
	const size_t POS_W0 = offset;
	const size_t POS_W2 = offset+1;

	// Init the likelihood to keep using the "MLE" matrix scalings
	ptrLik->initForBEB(MLE);

	std::vector<size_t> pIndices;
	pIndices.push_back(POS_W0);
	pIndices.push_back(POS_W2);

	Sampler::Sample sample(MLE);
	for(size_t iW0=0; iW0<N_PER_OMEGA; ++iW0) {
		sample.setDoubleParameter(POS_W0, valW0[iW0]);

		for(size_t iW2=0; iW2<N_PER_OMEGA; ++iW2) {
			sample.setDoubleParameter(POS_W2, valW2[iW2]);

			// Compute the sample
			ptrLik->update(pIndices, sample);

			// Keep for class 2a
			likW2a.push_back( ptrLik->getSiteLikelihoodAtRoot(CLASS_2a) );
			assert(pairOmegaMap[std::make_pair(valW0[iW0], valW2[iW2])] == likW2a.size()-1);

			// Keep for class 2b
			if(iW0 == 0) {
				likW2b.push_back( ptrLik->getSiteLikelihoodAtRoot(CLASS_2b) );
			}
		}

		// Keep for class 0
		likW0.push_back( ptrLik->getSiteLikelihoodAtRoot(CLASS_0) );
	}

	// Keep for class 0
	likW1.push_back( ptrLik->getSiteLikelihoodAtRoot(CLASS_1) );

	ptrLik->resetForBEB();

}


void PosSelectionBEB::computeGridLikelihood() {

	// Init evidence and marginals
	fX = -std::numeric_limits<double>::max();
	margW0.resize(N_PER_OMEGA, -std::numeric_limits<double>::max());
	margW2.resize(N_PER_OMEGA, -std::numeric_limits<double>::max());
	margP0P1.resize(N_PER_PROPORTION*N_PER_PROPORTION, -std::numeric_limits<double>::max());

	// Compute grid liks
	for(size_t iG=0; iG<gridPoints.size(); ++iG) {
		// Get sites likelihood for each classes
		point_t &point = gridPoints[iG];

		std::vector<TI_EigenMatrixDyn_t> classSiteLik;
		classSiteLik.push_back(likW0[mapW0[point.w0]]); 	// Class 0
		classSiteLik.push_back(likW1.back());				// Class 1
		size_t pairIdx = pairOmegaMap[std::make_pair(point.w0, point.w2)];
		classSiteLik.push_back(likW2a[pairIdx]);			// Class 2a
		classSiteLik.push_back(likW2b[mapW2[point.w2]]);	// Class 2b

		if(EdgeNode::SCALING_TYPE == EdgeNode::LOG || EdgeNode::SCALING_TYPE == EdgeNode::LOG_ACCURATE) {
			point.logLik = point.scaling = 0.;

			// Compute weighted mixture of all class for each sites	(not scaled)
			TI_EigenMatrixDyn_t subLiks = TI_EigenMatrixDyn_t::Zero(N_CLASS, classSiteLik.back().cols());
			for(size_t iC=0; iC<N_CLASS; ++iC) {
				subLiks.row(iC) = point.pClass[iC] * classSiteLik[iC].row(0);
			}

			// Compute component wise log
			subLiks = subLiks.array().log();

			// Add scaling
			for(size_t iC=0; iC<N_CLASS; ++iC) {
				subLiks.row(iC) += classSiteLik[iC].row(1);
			}

			// Use "LOG_SUM" to compute the site-wise mixture
			for(size_t iS=0; iS<(size_t)classSiteLik.back().cols(); ++iS) {
				TI_TYPE logLikSite = 0.0;
				for(size_t iC=0; iC<N_CLASS; ++iC) {
					if(iC == 0) {
						logLikSite = subLiks(iC, iS);
					} else {
						logLikSite = LOG_PLUS(logLikSite, subLiks(iC, iS));
					}
				}
				point.logLik += logLikSite;
			}

		} else {
			point.logLik = 0.;

			// Compute weighted mixture of all class for each sites
			TI_EigenVector_t siteLik = point.pClass[CLASS_0] * classSiteLik[CLASS_0].row(0) +
					point.pClass[CLASS_1] * classSiteLik[CLASS_1].row(0) +
					point.pClass[CLASS_2a] * classSiteLik[CLASS_2a].row(0) +
					point.pClass[CLASS_2b] * classSiteLik[CLASS_2b].row(0);

			// sum of site log likelihood
			for(size_t iL=0; iL<(size_t)siteLik.size(); ++iL) {
				point.logLik += log(siteLik(iL));
			}
		}

		// Evidence
		fX = LOG_PLUS(fX, point.logLik);

		// Marginal
		margW0[mapW0[point.w0]] = LOG_PLUS(margW0[mapW0[point.w0]], point.logLik);
		margW2[mapW2[point.w2]] = LOG_PLUS(margW2[mapW2[point.w2]], point.logLik);

		std::pair<TI_TYPE, TI_TYPE> P0P1 = std::make_pair(point.pClass[0], point.pClass[1]);
		margP0P1[triangleMap[P0P1]] = LOG_PLUS(margP0P1[triangleMap[P0P1]], point.logLik);
	}

	// FIXME DEBUG std::cout << "fX = " << fX << std::endl;
}

void PosSelectionBEB::normalizeMarginals() {

	assert(margW0.size() == margW2.size());
	for(size_t iM=0; iM<margW0.size(); ++iM) {
		margW0[iM] -= fX;
		margW2[iM] -= fX;
	}

	for(size_t iM=0; iM<margP0P1.size(); ++iM) {
		margP0P1[iM] -= fX;
	}
}

void PosSelectionBEB::computeSiteClassProbability() {

	size_t nSite = likW0.front().cols();
	siteClassProbability = TI_EigenMatrixDyn_t::Zero(N_CLASS, nSite);

	// For each site
	for(size_t iS=0; iS<nSite; ++iS) {

		// For each grid element
		for(size_t iG=0; iG<gridPoints.size(); ++iG) {

			point_t &point = gridPoints[iG];
			// Get lik of site iS for class iC
			std::vector<TI_EigenMatrixDyn_t> classSiteLik;
			classSiteLik.push_back(likW0[mapW0[point.w0]]); 	// Class 0
			classSiteLik.push_back(likW1.back());				// Class 1
			size_t pairIdx = pairOmegaMap[std::make_pair(point.w0, point.w2)];
			classSiteLik.push_back(likW2a[pairIdx]);			// Class 2a
			classSiteLik.push_back(likW2b[mapW2[point.w2]]);	// Class 2b

			TI_TYPE sumLik = 0.;
			std::vector<TI_TYPE> classLik(4,0.);
			if(EdgeNode::SCALING_TYPE == EdgeNode::LOG || EdgeNode::SCALING_TYPE == EdgeNode::LOG_ACCURATE) {
				// Compute the "term" fh and sum(fh) over class
				for(size_t iC=0; iC<N_CLASS; ++iC) {
					classLik[iC] = log(point.pClass[iC]*classSiteLik[iC](0,iS))+classSiteLik[iC](1,iS);
					if(iC==0) {
						sumLik = classLik[iC];
					} else {
						sumLik = LOG_PLUS(sumLik, classLik[iC]);
					}
				}

				// Apply the correction to point.logLik (f-h) : (fh/sum(fh))*(f-h) in log space
				for(size_t iC=0; iC<N_CLASS; ++iC) {
					TI_TYPE gridSCProb = classLik[iC] - sumLik + point.logLik;
					if(iG==0) {
						siteClassProbability(iC, iS) = gridSCProb;
					} else {
						siteClassProbability(iC, iS) = LOG_PLUS(siteClassProbability(iC, iS), gridSCProb);
					}
				}
			} else {
				// Compute the "term" fh and sum(fh) over class
				for(size_t iC=0; iC<N_CLASS; ++iC) {
					classLik[iC] = point.pClass[iC]*classSiteLik[iC](0,iS);
					sumLik += classLik[iC];
				}

				// Apply the correction to point.logLik (f-h) : (fh/sum(fh))*(f-h) in log space
				for(size_t iC=0; iC<N_CLASS; ++iC) {
					TI_TYPE gridSCProb = log(classLik[iC]/sumLik) + point.logLik;
					if(iG==0) {
						siteClassProbability(iC, iS) = gridSCProb;
					} else {
						siteClassProbability(iC, iS) = LOG_PLUS(siteClassProbability(iC, iS), gridSCProb);
					}
				}
			}
		}
	}
	// Normalize by fX
	siteClassProbability = siteClassProbability.array() - fX;
}

} /* namespace Tests */
} /* namespace Statistics */
} /* namespace Utils */
