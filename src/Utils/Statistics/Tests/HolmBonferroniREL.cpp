//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file HolmBonferroniREL.cpp
 *
 * @date Feb 13, 2017
 * @author meyerx
 * @brief
 */
#include "HolmBonferroniREL.h"

#include "Utils/Code/Algorithm.h" // IWYU pragma: keep

#include <stddef.h>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/detail/derived_accessors.hpp>
#include <boost/math/policies/policy.hpp>

namespace Utils {
namespace Statistics {
namespace Tests {

HolmBonferroniREL::HolmBonferroniREL(double aAlpha, const Sampler::Sample &aMLE, const std::vector<Sampler::Sample> &aNullHypotheses) :
		alpha(aAlpha), MLE(aMLE), nullHypotheses(aNullHypotheses) {
}

HolmBonferroniREL::~HolmBonferroniREL() {
}

void HolmBonferroniREL::compute() {

	boost::math::chi_squared_distribution<> chi1(1);

	// Compute p-Values
	for(size_t iH=0; iH<nullHypotheses.size(); ++iH) {
		double D = 2*(MLE.likelihood - nullHypotheses[iH].likelihood);
		if(D < 0 && fabs(D) < 5e-4) {
			D = 0.;
		} else if(nullHypotheses[iH].likelihood > MLE.likelihood) {
			std::cout << "Error in 'LRT' : Delta likelihod = " << MLE.likelihood - nullHypotheses[iH].likelihood << std::endl;
			D = 0.;
		}
		// Mixture 50%-50% of mass point at 0 and chi squared distribution with 1 degree of freedom
		// pVal is then equals to : pVal = 1 - (50% of point mass at 0 + 50% cdf of D)
		double pVal = 1-(0.5+boost::math::cdf(chi1, D)/2.);
		pValues.push_back(pVal);
	}

	// Sort p-values
	std::vector<size_t> orderedInd = Utils::Algorithm::getOrderedIndices(pValues, true);

	// Select using test
	double m = nullHypotheses.size(); // nb of branches
	rejH0.resize(m);
	for(size_t iP=0; iP<orderedInd.size(); ++iP) {
		double h = iP+1;
		double holmVal = alpha/(m-h+1);
		double pVal = pValues[orderedInd[iP]];

		rejH0[orderedInd[iP]] = (pVal <= holmVal);
	}
}


std::string HolmBonferroniREL::summarizeResult(std::string &strMLE, std::string &treeMLE, std::vector<string> &strNullHypothesis, std::vector<string> &treesNullHypothesis) {
	std::stringstream ss;

	size_t fieldSize = 20;

	ss << "Summary - H1 lik = " << MLE.likelihood << std::endl;
	ss << setw(fieldSize) << "Branch" << setw(fieldSize) << "Likelihood" << setw(fieldSize) << "P-values";
	ss << setw(2*fieldSize) << "Holm-Bonferroni test with a=" << alpha << std::endl;
	for(size_t iH=0; iH<nullHypotheses.size(); ++iH) {
		ss << std::setw(fieldSize) << iH << std::setw(fieldSize) << nullHypotheses[iH].likelihood << std::setw(fieldSize) << pValues[iH];
		if(rejH0[iH]) {
			ss << std::setw(2*fieldSize) << "H0 rejected" << std::endl;
		} else {
			ss << std::setw(2*fieldSize) << "H0 not rejected" << std::endl;
		}
	}
	ss << "-------" << std::endl;

	ss << "Alternative hypothesis (H1) : " << std::endl;
	ss << strMLE << std::endl;
	ss << "Tree : " << treeMLE << std::endl;
	ss << "-------" << std::endl;

	for(size_t iH=0; iH<nullHypotheses.size(); ++iH) {
		if(rejH0[iH]) {
			ss << "[H0 REJECTED] H0 rejected for branch : " << iH;
		} else {
			ss << "[H0 NOT REJECTED] H0 not rejected for branch : " << iH;
		}
		ss << " - with pValues = " << pValues[iH]  << std::endl;
		ss << strNullHypothesis[iH] << std::endl;
		ss << "Tree : " << treesNullHypothesis[iH] << std::endl;
		ss << "-------" << std::endl;
	}
	return ss.str();
}

} /* namespace Tests */
} /* namespace Statistics */
} /* namespace Utils */
