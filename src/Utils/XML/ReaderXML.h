//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderXML.h
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#ifndef READERXML_H_
#define READERXML_H_

#include <stddef.h>
#include <iostream>
#include <string>

#include "../Output/OutputFiles.h"
#include "Model/Model.h"
#include "Parallel/Parallel.h" // IWYU pragma: keep
#include "Tag.h"
#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/Likelihood/ReaderLikelihoodXML.h"
#include "Utils/XML/Maximizer/ReaderMaximizerXML.h"
#include "Utils/Statistics/Tests/PosSelectionBEB.h"
#include "Utils/XML/TinyXML/tinyxml.h"

namespace Sampler { class BaseSampler; }

namespace XML {

class ReaderXML {
private:
	typedef StatisticalModel::Model::sharedPtr_t modelPtr_t;
	typedef enum {UNKNOWN_RUN, MCMC_RUN, ML_RUN, BEB_RUN, HOLM_BONFERRONI_RUN, LOG_ANALYSIS_RUN} run_t;


public:
	ReaderXML(const std::string &fileName);
	ReaderXML(const std::string &fileName, const std::string &xmlContent);
	~ReaderXML();

	void run();

	// These pointers only during the readerXML scope
	ReaderLikelihoodXML* getPtrLikXML();
	StatisticalModel::Model* getPtrModel();

	size_t getNIteration();

private:
	static const size_t DEFAULT_SEED;

	run_t runType;
	size_t seed;
	size_t nProposal, nGradient, nChain, nIteration;

	std::ofstream redirectFile;
	std::streambuf *coutbuf;

	TiXmlDocument doc;
	TiXmlElement *pRoot;

	ReaderLikelihoodXML::sharedPtr_t ptrLikXML;
	ReaderMaximizerXML::sharedPtr_t ptrMaximizerML;

	Utils::OutputFiles::sharedPtr_t ptrOutput;

	void initXML();
	void initXML(const std::string &xmlContent);
	void readXML();

	void readXML_ML();

	void readLogFile();
	void readSeed();

	void readTopologyML();

	void readMaxIteration();

	void readLikelihood();

	void readMaximizer();

	void readOutput();

	void setHypothesisForBEB(std::string Hx);
	void setBranchForHolmBonferroni(int iBranch);
	void setBranchForAllBranchesBEB(int iBranch);

	void runBEB();
	void runHolmBonferroniREL();
	void runOneBranchBEB();
	void runAllBranchesBEB();

	void writeResultBS(std::string branchName, double likH0, std::string strH0, double likH1, std::string strH1, std::string strLRT, std::ofstream &ostream);
	void writeResultBEB(std::string branchName, Utils::Statistics::Tests::PosSelectionBEB::result_t resultBEB, std::ofstream &ostream);

	std::string getStringLRT(Sampler::Sample &mleH0, Sampler::Sample &mleH1);

};

} /* namespace XML */

#endif /* READERXML_H_ */
