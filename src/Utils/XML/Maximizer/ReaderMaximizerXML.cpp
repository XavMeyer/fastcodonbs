//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderMaximizerXML.cpp
 *
 * @date Oct 24, 2016
 * @author meyerx
 * @brief
 */
#include "ReaderMaximizerXML.h"

#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Model.h"
#include "Sampler/Samples/Sample.h"

class TiXmlElement;
namespace StatisticalModel { class Parameters; }

namespace XML {

ReaderMaximizerXML::ReaderMaximizerXML(const std::string &aName, TiXmlElement *aPRoot,
		size_t aSeed, StatisticalModel::Model* aPtrModel) :
				name(aName), pRoot(aPRoot), seed(aSeed), ptrModel(aPtrModel) {
	readMaximizer();
}

ReaderMaximizerXML::~ReaderMaximizerXML() {
}

UMAX::MaximizerInterface::sharedPtr_t ReaderMaximizerXML::getPtrMaximizer() {
	return ptrMaximizer;
}


std::vector<double> ReaderMaximizerXML::drawStartingPoint() const {
	using namespace StatisticalModel::Likelihood;
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrModel->getLikelihood().get());
	Sampler::Sample sample = hlp->defineInitSample(ptrModel->getParams());
	return sample.getDblValues();
}


void ReaderMaximizerXML::readMaximizer() {
	using namespace StatisticalModel::Likelihood;
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrModel->getLikelihood().get());
	StatisticalModel::Parameters &params = ptrModel->getParams();
	Sampler::Sample sample = hlp->defineInitSample(ptrModel->getParams());
	LikelihoodInterface::sharedPtr_t ptrLik = ptrModel->getLikelihood();


	if(name == Tag::LBFGS_B) {
		ptrMaximizer.reset(
				new UMAX::MaximizerLBFGSB(seed, params, sample, ptrLik));
	} else {
		std::stringstream ss;
		ss << "Sampler '" << name << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}
}

} /* namespace XML */
