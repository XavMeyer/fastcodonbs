//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderMaximizerXML.h
 *
 * @date Oct 24, 2016
 * @author meyerx
 * @brief
 */
#ifndef READERMAXIMIZERXML_H_
#define READERMAXIMIZERXML_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>

#include "../Tag.h"
#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Model/Likelihood/Helper/Helpers.h"
#include "Model/Model.h"
#include "Utils/Maximizer/IncMaximizers.h"
#include "Utils/Maximizer/MaxFunctionPLL.h"
#include "Utils/Maximizer/MaximizerInterface.h"
#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/TinyXML/tinyxml.h"

class TiXmlElement;
namespace StatisticalModel { class Model; }

namespace XML {

namespace UMAX = ::Utils::Maximizer;

class ReaderMaximizerXML {
public:
	typedef boost::shared_ptr<ReaderMaximizerXML> sharedPtr_t;

public:
	ReaderMaximizerXML(const std::string &aName, TiXmlElement *aPRoot,
			size_t aSeed, StatisticalModel::Model* aPtrModel);
	~ReaderMaximizerXML();

	UMAX::MaximizerInterface::sharedPtr_t getPtrMaximizer();

	std::vector<double> drawStartingPoint() const;

private:

	static const size_t DEFAULT_SEED;

	const std::string name;
	TiXmlElement *pRoot;
	size_t seed;
	StatisticalModel::Model* ptrModel;

	UMAX::MaximizerInterface::sharedPtr_t ptrMaximizer;

	void readMaximizer();

};

} /* namespace XML */

#endif /* READERMAXIMIZERXML_H_ */
