//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ReaderXML.cpp
 *
 * @date May 9, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderXML.h"

#include <assert.h>

#include "Sampler/Samples/Sample.h"
#include "Parallel/Manager/MpiManager.h"
#include "Model/Likelihood/BranchSiteREL/BranchSiteRELik.h"
#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Utils/Statistics/Tests/HolmBonferroniREL.h"
#include "Utils/Statistics/Tests/PosSelectionBEB.h"

#include <boost/math/distributions/chi_squared.hpp>

#include "../Output/OutputManager.h"
namespace Sampler { class BaseSampler; }
namespace StatisticalModel { class Parameters; }

namespace XML {

const size_t ReaderXML::DEFAULT_SEED = 1337;

ReaderXML::ReaderXML(const std::string &fileName) : doc(fileName) {

	runType = UNKNOWN_RUN;
	seed = DEFAULT_SEED;
	nGradient = nProposal = Parallel::mpiMgr().getNProc();
	nChain = 1;

	coutbuf = NULL;

	initXML();
	readXML();
}

ReaderXML::ReaderXML(const std::string &fileName, const std::string &xmlContent) : doc() {

	runType = UNKNOWN_RUN;
	seed = DEFAULT_SEED;
	nGradient = nProposal = 1;//Parallel::mpiMgr().getNProc();
	nChain = 1;

	coutbuf = NULL;

	initXML(xmlContent);
	readXML();
}


ReaderXML::~ReaderXML() {

	if(coutbuf!=NULL) {
		std::cout.rdbuf(coutbuf);
		redirectFile.close();
	}

}

void ReaderXML::run() {

	if(runType == ML_RUN) {
		ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
		if(Parallel::mpiMgr().isMainProcessor()) {
			ptrMaximizerML->getPtrMaximizer()->printMLE();
		}
	} else if(runType == BEB_RUN) {
		runBEB();
	} else if(runType == HOLM_BONFERRONI_RUN) {
		runHolmBonferroniREL();
	}
}

ReaderLikelihoodXML* ReaderXML::getPtrLikXML() {
	return ptrLikXML.get();
}

StatisticalModel::Model* ReaderXML::getPtrModel() {
	return ptrLikXML->getModelPtr();
}

size_t ReaderXML::getNIteration() {
	return nIteration;
}

void ReaderXML::initXML() {
	if (!doc.LoadFile()) {
		XML::errorMessage(__func__, "Unable to open XML file.");
	}

	pRoot = doc.FirstChildElement(Tag::ROOT_TAG);
	if(!pRoot){
		XML::errorMissingTag(__func__, Tag::ROOT_TAG);
	}
}

void ReaderXML::initXML(const std::string &xmlContent) {

	if (!doc.Parse((const char*)xmlContent.c_str(), 0, TIXML_ENCODING_UTF8)) {
		XML::errorMessage(__func__, "Unable to open XML file.");
	}

	pRoot = doc.FirstChildElement(Tag::ROOT_TAG);
	if(!pRoot){
		XML::errorMissingTag(__func__, Tag::ROOT_TAG);
	}
}

void ReaderXML::readXML() {

	const std::string *pAttrib = pRoot->Attribute(Tag::METHOD_ATT);
	if(!pAttrib){
		XML::errorMissingAttribute(__func__, Tag::METHOD_ATT);
	} else {
		if(*pAttrib == Tag::ML) {
			runType = ML_RUN;
			readXML_ML();
		} else if(*pAttrib == Tag::BEB) {
			runType = BEB_RUN;
			// Ensure it is the correct likelihood
		    const std::string *likName;
		    TiXmlElement *pChild = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG);
			if(!pChild){
				XML::errorMissingTag(__func__, Tag::LIKELIHOOD_TAG);
			} else {
				likName = pChild->Attribute(Tag::NAME_ATT);
				if(!likName){
					XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
				} else {
					assert(*likName == Tag::SEL_POS_LIGHT_NAME);
				}
		    }
		} else if(*pAttrib == Tag::HOLM_BONFERRONI_RUN) {
			runType = HOLM_BONFERRONI_RUN;
			// Ensure it is the correct likelihood
		    const std::string *likName;
		    TiXmlElement *pChild = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG);
			if(!pChild){
				XML::errorMissingTag(__func__, Tag::LIKELIHOOD_TAG);
			} else {
				likName = pChild->Attribute(Tag::NAME_ATT);
				if(!likName){
					XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
				} else {
					assert(*likName == Tag::BRANCH_SITE_REL_NAME);
				}
		    }
		// Other operations are done during the run command
		} else {
			std::stringstream ss;
			ss << "Method '" << *pAttrib << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	}

	readOutput();

}

void ReaderXML::readXML_ML() {

	readLogFile();

	readTopologyML();
	//Parallel::mpiMgr().defineTopology(nProposal, nChain);
	Parallel::mlMgr().deinit();
	Parallel::mlMgr().init(false, 1, nGradient);

	readSeed();
	std::srand(seed); // Every thread as the same std rng
	// During init everybody has the same PRNG
	Parallel::mpiMgr().setSeedPRNG(seed);

	readMaxIteration();

	readLikelihood();
	readMaximizer();
}



void ReaderXML::readLogFile() {
	// Validate if checkpoint are used
	bool isUsingCheckpoint = pRoot->FirstChildElement(Tag::SAMPLER_TAG) != NULL &&
			 pRoot->FirstChildElement(Tag::SAMPLER_TAG)->FirstChildElement(Tag::CHECKPOINT_TAG)!= NULL;

	std::string logFile;

	if(XML::readElement(__func__, Tag::LOG_FILE_TAG, pRoot, logFile, OPTIONAL)) {
		if(Parallel::mpiMgr().isMainProcessor()) {
			logFile.append(".out");
			if(!isUsingCheckpoint) {
				redirectFile.open(logFile.c_str());
			} else {
				redirectFile.open(logFile.c_str(), std::ios::app);
			}
			if(coutbuf == NULL) {
				coutbuf = std::cout.rdbuf();
				std::cout.rdbuf(redirectFile.rdbuf());
			}
		}
	}
}

void ReaderXML::readSeed() {
	XML::readElement(__func__, Tag::SEED_TAG, pRoot, seed, OPTIONAL);
}

void ReaderXML::readTopologyML() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::TOPOLOGY_TAG);
	if(!pChild){
		XML::warningMissingTag(__func__, Tag::TOPOLOGY_TAG);
		return;
	}

	XML::readElement(__func__, Tag::NGRADIENT_TAG, pChild, nGradient, OPTIONAL);

	if(nGradient != (size_t)Parallel::mpiMgr().getNProc()) {
		std::stringstream ssErr;
		ssErr << "MPI number of processor ( " << Parallel::mpiMgr().getNProc();
		ssErr << " ) doesn't correspond to the requested number of gradients ( ";
		ssErr << nGradient << " = " << nGradient << " ).";
		XML::errorMessage(__func__, ssErr.str());
	}
}

void ReaderXML::readMaxIteration() {
	nIteration = 0;
	XML::readElement(__func__, Tag::NITERATION_TAG, pRoot, nIteration, OPTIONAL);
}

void ReaderXML::readLikelihood() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::LIKELIHOOD_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrLikXML.reset(new ReaderLikelihoodXML(*pAttrib, pChild));
		}
	}
}

void ReaderXML::readMaximizer() {
	TiXmlElement *pChild = pRoot->FirstChildElement(Tag::MAXIMIZER_TAG);
	if(!pChild){
		XML::errorMissingTag(__func__, Tag::MAXIMIZER_TAG);
	} else {
		const std::string *pAttrib = pChild->Attribute(Tag::NAME_ATT);
		if(!pAttrib){
			XML::errorMissingAttribute(__func__, Tag::NAME_ATT);
		} else {
			ptrMaximizerML.reset(new ReaderMaximizerXML(*pAttrib, pChild, seed, ptrLikXML->getModelPtr()));
		}
	}
}

void ReaderXML::readOutput() {

	// If there is no output tag we use MCMC class default
	TiXmlElement *pOutput = pRoot->FirstChildElement(Tag::OUTPUT_TAG);
	if(!pOutput){
		warningMissingTag(__func__, Tag::OUTPUT_TAG);
		return;
	}

	// Output File
	std::string outputFile("output");
	if(XML::readElement(__func__, Tag::FILE_TAG, pOutput, outputFile, OPTIONAL) && ptrOutput == NULL) {
		Utils::outputManager().setBaseFileName(outputFile);
		ptrOutput.reset(new Utils::OutputFiles(outputFile));
	}

	// Verbosity
	size_t verbosity = 1;
	if(XML::readElement(__func__, Tag::VERBOSE_TAG, pOutput, verbosity, OPTIONAL)){
		Utils::verboseLevel_t verbLvl = static_cast<Utils::verboseLevel_t>(verbosity);
		Utils::outputManager().setVerbosityThreshold(verbLvl);
	}


}

void ReaderXML::setHypothesisForBEB(std::string Hx) {
    TiXmlElement *hypothesis = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG)->FirstChildElement(Tag::HYPOTHESIS_TAG);
    if(hypothesis == NULL) {
    	hypothesis = new TiXmlElement(Tag::HYPOTHESIS_TAG);
    	pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG)->LinkEndChild(hypothesis);
    }
    hypothesis->Clear();
    hypothesis->LinkEndChild( new TiXmlText( Hx ));
}

void ReaderXML::setBranchForHolmBonferroni(int iBranch) {
    TiXmlElement *fgBranch = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG)->FirstChildElement(Tag::FG_BRANCH_TAG);
    if(fgBranch == NULL) {
    	fgBranch = new TiXmlElement(Tag::FG_BRANCH_TAG);
    	pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG)->LinkEndChild(fgBranch);
    }
    fgBranch->Clear();
    std::stringstream ss;
    ss << iBranch;
    std::string tmp(ss.str());
    fgBranch->LinkEndChild( new TiXmlText( tmp ));
}

void ReaderXML::setBranchForAllBranchesBEB(int iBranch) {
    TiXmlElement *fgBranch = pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG)->FirstChildElement(Tag::FG_BRANCH_TAG);
    if(fgBranch == NULL) {
    	fgBranch = new TiXmlElement(Tag::FG_BRANCH_TAG);
    	pRoot->FirstChildElement(Tag::LIKELIHOOD_TAG)->LinkEndChild(fgBranch);
    }
    fgBranch->Clear();
    std::stringstream ss;
    ss << iBranch;
    std::string tmp(ss.str());
    fgBranch->LinkEndChild( new TiXmlText( tmp ));
}



void ReaderXML::runBEB() {

	setHypothesisForBEB(Tag::H0);
	readXML_ML();

	if(ptrLikXML->doAllBranches()) {
		runAllBranchesBEB();
	} else {
		runOneBranchBEB();
	}

}

void ReaderXML::runOneBranchBEB() {

	std::string strH0, strH1, strLRT;
	Sampler::Sample mleH0, mleH1;

	PositiveSelection::Light::SelPositive* ptrLik = static_cast<PositiveSelection::Light::SelPositive*>(ptrLikXML->getModelPtr()->getLikelihood().get());
	std::vector<size_t> branchLabels = ptrLik->getBranchesLabels();
	std::vector<std::string> branchNames = ptrLik->getBranchNames();

	std::stringstream ssBranchName;
	if(ptrLik->getFGBranch() < 0) {
		ssBranchName << "'File Specified'";
	} else {
		ssBranchName << branchNames[ptrLik->getFGBranch()];
	}
	if(Parallel::mpiMgr().isMainProcessor()) std::cout << "Branch name : " << ssBranchName.str() << std::endl;

	/**************** H0 ****************/
	if(Parallel::mpiMgr().isMainProcessor()) std::cout << ">> H0 <<" << std::endl;
	double maxH0 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
	mleH0.likelihood = maxH0;
	mleH0.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());
	Parallel::mlMgr().deinit();
	if(Parallel::mpiMgr().isMainProcessor()) {
		ptrMaximizerML->getPtrMaximizer()->printMLE();
		strH0 = ptrMaximizerML->getPtrMaximizer()->toStringMLE();
	}

	/**************** H1 ****************/
	if(Parallel::mpiMgr().isMainProcessor()) std::cout << ">> H1 <<" << std::endl;
	setHypothesisForBEB(Tag::H1);
	readXML_ML();

	double maxH1 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
	mleH1.likelihood = maxH1;
	mleH1.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());

	/**************** Check if negative deltaLogLik ****************/
	double deltaLogLik = maxH1-maxH0;
	const size_t MAX_FAIL = 5;
	size_t countFail = 0;

	while(deltaLogLik < 0. && countFail < MAX_FAIL) {
	//if(deltaLogLik < 0.) {
		if(deltaLogLik > -1.e-4) { // Small enough to be an error
			maxH1 = maxH0; // TODO Check if parameters are nearly equal.
			mleH1.likelihood = maxH1;
			deltaLogLik = 0.;
			break;
		} else { // Rerun optimizer
			if(countFail == MAX_FAIL-1) {
				ptrMaximizerML->getPtrMaximizer()->setStartingPoint(mleH0.getDblValues());
			} else {
				std::vector<double> newStartingPoint = ptrMaximizerML->drawStartingPoint();
				ptrMaximizerML->getPtrMaximizer()->setStartingPoint(newStartingPoint);
			}
			maxH1 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
			mleH1.likelihood = maxH1;
			mleH1.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());
		}
		deltaLogLik = maxH1-maxH0;
		countFail++;
	}

	/**************** LRT and BEB ****************/
	if(deltaLogLik >= 0.) {
		if(Parallel::mpiMgr().isMainProcessor()) {
			ptrMaximizerML->getPtrMaximizer()->printMLE();
			strH1 = ptrMaximizerML->getPtrMaximizer()->toStringMLE();
		}
		// Compute p-Values
		double alpha = 0.05;
		double D = 2*(maxH1-maxH0);
		// Mixture 50%-50% of mass point at 0 and chi squared distribution with 1 degree of freedom
		// pVal is then equals to : pVal = 1 - (50% of point mass at 0 + 50% cdf of D)
		boost::math::chi_squared_distribution<> chi1(1);
		double pVal = 1-(0.5+boost::math::cdf(chi1, D)/2.);
		double Z = boost::math::quantile(chi1, 2.*(1-alpha)-1.);
		//std::cout << 1-(0.5+0.5*boost::math::cdf(chi1, 1.92)) << std::endl;

		if(Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "LRT = " << D << " and p-value = " << pVal << std::endl;
			std::cout << "Test is significant (at alpha=" << alpha << " if LRT > " << Z << ")" << std::endl;
			strLRT = getStringLRT(mleH0, mleH1);

			if(ptrOutput != NULL) {
				writeResultBS(ssBranchName.str(), mleH0.likelihood, strH0, mleH1.likelihood, strH1, strLRT, ptrOutput->getLogStream());
			}

			if(pVal <= alpha) {
				std::cout << "Alternative hypothesis significant." << std::endl;
				std::cout << " Applying BEB... (can take several minutes)." << std::endl;

				Utils::Statistics::Tests::PosSelectionBEB BEB(mleH1, static_cast<PositiveSelection::Light::SelPositive*>(ptrLikXML->getModelPtr()->getLikelihood().get()));
				BEB.computeBEB();
				std::cout << BEB.summarizeResult() << std::endl;

				if(ptrOutput != NULL) {
					writeResultBEB(ssBranchName.str(), BEB.getResult(), ptrOutput->getBEBStream());
				}

			}
		}
	} else {
		if(Parallel::mpiMgr().isMainProcessor()) std::cout << "H1 does not seem to be nested inside H0" << std::endl;
	}
	if(Parallel::mpiMgr().isMainProcessor()) std::cout << "-------------------------------------------------------------------------" << std::endl;
}

void ReaderXML::runAllBranchesBEB() {

	PositiveSelection::Light::SelPositive* ptrLik = static_cast<PositiveSelection::Light::SelPositive*>(ptrLikXML->getModelPtr()->getLikelihood().get());
	std::vector<size_t> branchLabels = ptrLik->getBranchesLabels();
	std::vector<std::string> branchNames = ptrLik->getBranchNames();

	for(size_t iB=0; iB<branchLabels.size(); ++iB) {

		std::string strH0, strH1, strLRT;
		Sampler::Sample mleH0, mleH1;

		setHypothesisForBEB(Tag::H0);
		setBranchForAllBranchesBEB(iB);

		readXML_ML();

		std::stringstream ssBranchName;
		if(ptrLik->getFGBranch() < 0) {
			ssBranchName << "'File Specified'";
		} else {
			ssBranchName << branchNames[iB];
		}
		if(Parallel::mpiMgr().isMainProcessor()) std::cout << std::endl <<  "Branch name: " << ssBranchName.str() << std::endl;

		/**************** H0 ****************/
		if(Parallel::mpiMgr().isMainProcessor()) std::cout << std::endl << ">> H0 <<" << std::endl;

		double maxH0 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
		mleH0.likelihood = maxH0;
		mleH0.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());
		if(Parallel::mpiMgr().isMainProcessor()) {
			ptrMaximizerML->getPtrMaximizer()->printMLE();
			strH0 = ptrMaximizerML->getPtrMaximizer()->toStringMLE();
		}
		Parallel::mlMgr().deinit();

		/**************** H1 ****************/
		if(Parallel::mpiMgr().isMainProcessor()) std::cout << std::endl << ">> H1 <<" << std::endl;
		setHypothesisForBEB(Tag::H1);
		setBranchForAllBranchesBEB(iB);

		readXML_ML();
		double maxH1 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
		mleH1.likelihood = maxH1;
		mleH1.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());


		/**************** Check if negative deltaLogLik ****************/
		double deltaLogLik = maxH1-maxH0;
		const size_t MAX_FAIL = 5;
		size_t countFail = 0;

		while(deltaLogLik < 0. && countFail < MAX_FAIL) {
		//if(deltaLogLik < 0.) {
			if(deltaLogLik > -1.e-4) { // Small enough to be an error
				//std::cout << "1 : " << deltaLogLik << std::endl;
				maxH1 = maxH0; // TODO Check if parameters are nearly equal.
				mleH1.likelihood = maxH1;
				deltaLogLik = 0.;
				break;
			} else { // Rerun optimizer
				//std::cout << "2: " << deltaLogLik << std::endl;
				if(countFail == MAX_FAIL-1) {
					ptrMaximizerML->getPtrMaximizer()->setStartingPoint(mleH0.getDblValues());
				} else {
					std::vector<double> newStartingPoint = ptrMaximizerML->drawStartingPoint();
					ptrMaximizerML->getPtrMaximizer()->setStartingPoint(newStartingPoint);
				}
				maxH1 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
				mleH1.likelihood = maxH1;
				mleH1.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());
			}
			deltaLogLik = maxH1-maxH0;
			countFail++;
		}
		//std::cout << "Optimization failed : " << countFail << " times." << std::endl;


		/**************** LRT and BEB ****************/
		if(deltaLogLik >= 0.) {
			if(Parallel::mpiMgr().isMainProcessor()) {
				ptrMaximizerML->getPtrMaximizer()->printMLE();
				strH1 = ptrMaximizerML->getPtrMaximizer()->toStringMLE();
 			}
			// Compute p-Values
			double alpha = 0.05;
			double D = 2*(maxH1-maxH0);
			// Mixture 50%-50% of mass point at 0 and chi squared distribution with 1 degree of freedom
			// pVal is then equals to : pVal = 1 - (50% of point mass at 0 + 50% cdf of D)
			boost::math::chi_squared_distribution<> chi1(1);
			double pVal = 1-(0.5+boost::math::cdf(chi1, D)/2.);
			double Z = boost::math::quantile(chi1, 2.*(1-alpha)-1.);
			//std::cout << 1-(0.5+0.5*boost::math::cdf(chi1, 1.92)) << std::endl;

			if(Parallel::mpiMgr().isMainProcessor()) {
				std::cout <<  std::endl << "LRT = " << D << " and p-value = " << pVal << std::endl;
				std::cout << "Test is significant (at alpha=" << alpha << " if LRT > " << Z << ")" << std::endl;
				strLRT = getStringLRT(mleH0, mleH1);

				if(ptrOutput != NULL) {
					writeResultBS(ssBranchName.str(), mleH0.likelihood, strH0, mleH1.likelihood, strH1, strLRT, ptrOutput->getLogStream());
				}

				if(pVal <= alpha) {
					std::cout << std::endl << "Alternative hypothesis significant." << std::endl;
					std::cout << ">> Applying BEB... (can take several minutes)." << std::endl;

					Utils::Statistics::Tests::PosSelectionBEB BEB(mleH1, static_cast<PositiveSelection::Light::SelPositive*>(ptrLikXML->getModelPtr()->getLikelihood().get()));
					BEB.computeBEB();
					std::cout << BEB.summarizeResult() << std::endl;

					if(ptrOutput != NULL) {
						writeResultBEB(ssBranchName.str(), BEB.getResult(), ptrOutput->getBEBStream());
					}

				}
			}
		} else {
			if(Parallel::mpiMgr().isMainProcessor()) std::cout << "H1 does not seem to be nested inside H0" << std::endl;
		}
		if(Parallel::mpiMgr().isMainProcessor()) std::cout << "-------------------------------------------------------------------------" << std::endl;

		Parallel::mlMgr().deinit();
	}
}

std::string ReaderXML::getStringLRT(Sampler::Sample &mleH0, Sampler::Sample &mleH1) {

	std::stringstream ss;

	// Compute p-Values
	double alpha = 0.05;
	double D = 2*(mleH1.likelihood-mleH0.likelihood);
	// Mixture 50%-50% of mass point at 0 and chi squared distribution with 1 degree of freedom
	// pVal is then equals to : pVal = 1 - (50% of point mass at 0 + 50% cdf of D)
	boost::math::chi_squared_distribution<> chi1(1);
	double pVal = 1-(0.5+boost::math::cdf(chi1, D)/2.);
	double Z = boost::math::quantile(chi1, 2.*(1-alpha)-1.);
	//std::cout << 1-(0.5+0.5*boost::math::cdf(chi1, 1.92)) << std::endl;

	ss << D << "\t" << pVal << "\t" << Z << std::endl;

	return ss.str();
}

void ReaderXML::writeResultBS(std::string branchName, double likH0, std::string strH0, double likH1, std::string strH1, std::string strLRT, std::ofstream &ostream) {
	//std::stringstream ss;
	ostream << branchName << std::endl;
	ostream << likH0 << "\t" << strH0;
	ostream << likH1 << "\t" << strH1;
	ostream << strLRT << std::endl;

}

void ReaderXML::writeResultBEB(std::string branchName, Utils::Statistics::Tests::PosSelectionBEB::result_t resultBEB, std::ofstream &ostream) {
	ostream << branchName << std::endl;

	for(size_t iW=0; iW<resultBEB.w0.size(); ++iW) {
		ostream << resultBEB.w0[iW] << "\t";
	}
	ostream << std::endl;

	for(size_t iW=0; iW<resultBEB.w2.size(); ++iW) {
		ostream << resultBEB.w2[iW] << "\t";
	}
	ostream << std::endl;

	for(size_t iP=0; iP<resultBEB.Ps.size(); ++iP) {
		ostream << resultBEB.Ps[iP] << "\t";
	}
	ostream << std::endl;

	for(size_t iS=0; iS<resultBEB.siteProb.size(); ++iS) {
		ostream << resultBEB.siteProb[iS] << "\t";
	}
	ostream << std::endl;
}

void ReaderXML::runHolmBonferroniREL() {

	// Compute alternative hypotheses
	// Prepare and compute ML
	if(Parallel::mpiMgr().isMainProcessor()) std::cout << "Compute H0" << std::endl;
	setBranchForHolmBonferroni(-1);
	readXML_ML();
	double maxH1 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
	// Save result
	Sampler::Sample MLE;
	MLE.likelihood = maxH1;
	MLE.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());
	//std::string treeMLE = ptrLik->getNewickString();

	// Compute null hypotheses
	using namespace StatisticalModel::Likelihood::BranchSiteREL;
	BranchSiteRELik* ptrLik = dynamic_cast<BranchSiteRELik*>(ptrLikXML->getModelPtr()->getLikelihood().get());
	assert(ptrLik != NULL);
	size_t nBranch = ptrLik->getNBranch();

	std::vector<std::string> nullHypothesesTrees;
	std::vector<Sampler::Sample> nullHypothesesMLE;
	// For each branch, compute the null hypothesis
	for(size_t iH=0; iH<nBranch; ++iH) {
		if(Parallel::mpiMgr().isMainProcessor()) std::cout << "Compute H0 for branch " << iH << std::endl;
		setBranchForHolmBonferroni(iH);
		readXML_ML();
		ptrMaximizerML->getPtrMaximizer()->setStartingPoint(MLE.getDblValues());
		double maxH0 = ptrMaximizerML->getPtrMaximizer()->maximize(nIteration);
		Sampler::Sample result;
		result.likelihood = maxH0;
		result.setDblValues(ptrMaximizerML->getPtrMaximizer()->getValues());
		nullHypothesesMLE.push_back(result);
		//nullHypothesesTrees.push_back(ptrLik->getNewickString());
	}

	if(Parallel::mpiMgr().isMainProcessor()) {

		ptrLik = dynamic_cast<BranchSiteRELik*>(ptrLikXML->getModelPtr()->getLikelihood().get());
		assert(ptrLik != NULL);

		ptrLik->defineDAGNodeToCompute(MLE);
		std::string treeMLE = ptrLik->getNewickString();

		double alpha = 0.05;
		Utils::Statistics::Tests::HolmBonferroniREL hbRel(alpha, MLE, nullHypothesesMLE);
		hbRel.compute();

		// Output
		const StatisticalModel::Parameters &params = getPtrModel()->getParams();
		Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLikXML.get()->getModelPtr()->getLikelihood().get());

		// H1
		std::string strMLE(hlp->sampleToString(params, MLE));

		// H0s
		std::vector<string> strH0s;
		for(size_t iH=0; iH<nullHypothesesMLE.size(); ++iH) {
			strH0s.push_back(hlp->sampleToString(params, nullHypothesesMLE[iH]));
			ptrLik->defineDAGNodeToCompute(nullHypothesesMLE[iH]);
			nullHypothesesTrees.push_back(ptrLik->getNewickString());
		}
		std::cout << hbRel.summarizeResult(strMLE, treeMLE, strH0s, nullHypothesesTrees) << std::endl;
	}
}


} /* namespace XML */
