//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodXML.h
 *
 * @date May 11, 2015
 * @author meyerx
 * @brief
 */
#ifndef LIKELIHOODXML_H_
#define LIKELIHOODXML_H_

#include <boost/shared_ptr.hpp>

#include "../Tag.h"
#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Model/Likelihood/Helper/Helpers.h"
#include "Model/Likelihood/LikelihoodFactory.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Model.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/CodonFrequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/XML/HelpersXML.h"
#include "Utils/XML/TinyXML/tinyxml.h"

class TiXmlElement;
namespace ParameterBlock { class Blocks; }

namespace XML {

using namespace StatisticalModel::Likelihood;
using namespace StatisticalModel::Likelihood::Helper;

class ReaderLikelihoodXML {
public:
	typedef boost::shared_ptr<ReaderLikelihoodXML> sharedPtr_t;
	typedef StatisticalModel::Likelihood::LikelihoodInterface::sharedPtr_t likPtr_t;
	typedef StatisticalModel::Model::sharedPtr_t modelPtr_t;
	typedef boost::shared_ptr<ParameterBlock::Blocks> blocksPtr_t;

	typedef MolecularEvolution::MatrixUtils::nuclModel_t nuclModel_t;
	typedef MolecularEvolution::DataLoader::CodonFrequencies::codonFrequencies_t codonFreq_t;

public:
	ReaderLikelihoodXML(const std::string &aName, TiXmlElement *aPRoot);
	~ReaderLikelihoodXML();

	StatisticalModel::Model* getModelPtr();
	bool doAllBranches() const;

private:

	const std::string name;
	TiXmlElement *pRoot;

	likPtr_t ptrLik;
	modelPtr_t ptrModel;

	bool allBranches;

	void readLikelihood();
	void readSelPosLight();
	void readBranchSiteREL();

	/* Read helpers */
	void readDefaultParametersType(const Helper::HelperInterface::sharedPtr_t &hlp);

	/* Specific read for Codons models */
	bool readIsH1();
	bool readIsCompressed();
	nuclModel_t readNucleotideModel();
	codonFreq_t readCodonFrequencyType();

};

} /* namespace XML */

#endif /* LIKELIHOODXML_H_ */
