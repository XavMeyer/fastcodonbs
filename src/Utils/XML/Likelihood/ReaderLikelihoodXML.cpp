//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodXML.cpp
 *
 * @date May 11, 2015
 * @author meyerx
 * @brief
 */
#include "ReaderLikelihoodXML.h"

#include <stddef.h>

#include "Model/Likelihood/BranchSiteREL/BranchSiteRELik.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeParser/TmpNode.h"

class TiXmlElement;

namespace XML {

ReaderLikelihoodXML::ReaderLikelihoodXML(const std::string &aName, TiXmlElement *aPRoot) :
	name(aName), pRoot(aPRoot) {

	allBranches = false;
	readLikelihood();
}

ReaderLikelihoodXML::~ReaderLikelihoodXML() {
}

StatisticalModel::Model* ReaderLikelihoodXML::getModelPtr() {
	return ptrModel.get();
}

bool ReaderLikelihoodXML::doAllBranches() const {
	return allBranches;
}

void ReaderLikelihoodXML::readLikelihood() {
	if (name == Tag::SEL_POS_LIGHT_NAME) {
		readSelPosLight();
	} else if (name == Tag::BRANCH_SITE_REL_NAME) {
		readBranchSiteREL();
	} else {
		std::stringstream ss;
		ss << "Likelihood '" << name << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}

}

void ReaderLikelihoodXML::readSelPosLight() {

	bool isH1 = readIsH1();

	long int fgBranch=0;
	std::string strFgBranch;
	XML::readElement(__func__, Tag::FG_BRANCH_TAG, pRoot, strFgBranch, MANDATORY);
	//std::cout << strFgBranch << std::endl;
	if(strFgBranch == "all") {
		allBranches = true;
	} else if(strFgBranch == "file") {
		fgBranch = -1;
	} else {
		std::istringstream iss(strFgBranch);
		iss >> fgBranch;
		//std::cout << fgBranch << std::endl;
		assert(!iss.fail());
	}


	size_t nThread;
	XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	nuclModel_t nuclModel = readNucleotideModel();
	codonFreq_t cFreqType = readCodonFrequencyType();

	std::string alignName, treeName;
	XML::readElement(__func__, Tag::ALIGN_FILE_TAG, pRoot, alignName, MANDATORY);
	XML::readElement(__func__, Tag::TREE_FILE_TAG, pRoot, treeName, MANDATORY);

	bool isUsingCompression = readIsCompressed();

	// Likelihood
	ptrLik = LikelihoodFactory::createSelPositiveLight(isH1, isUsingCompression, fgBranch, nThread, nuclModel, cFreqType, alignName, treeName);
	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	std::string paramType;
	if(XML::readElement(__func__, Tag::PARAMETERS_TAG, pRoot, paramType, OPTIONAL)) {
		if(paramType == Tag::DEFAULT) {
			hlp->defineParameters(ptrModel->getParams());
		} else {
			std::stringstream ss;
			ss << "Parameter type '" << paramType << "' does not exists";
			XML::errorMessage(__func__, ss.str());
		}
	} else {
		hlp->defineParameters(ptrModel->getParams());
	}

}

void ReaderLikelihoodXML::readBranchSiteREL() {
	int fgBranch = -1;
	XML::readElement(__func__, Tag::FG_BRANCH_TAG, pRoot, fgBranch, MANDATORY);

	size_t nThread;
	XML::readElement(__func__, Tag::NTHREAD_TAG, pRoot, nThread, MANDATORY);

	nuclModel_t nuclModel = readNucleotideModel();
	codonFreq_t cFreqType = readCodonFrequencyType();

	std::string alignName, treeName;
	XML::readElement(__func__, Tag::ALIGN_FILE_TAG, pRoot, alignName, MANDATORY);
	XML::readElement(__func__, Tag::TREE_FILE_TAG, pRoot, treeName, MANDATORY);

	bool isUsingCompression = readIsCompressed();

	// Likelihood
	ptrLik = LikelihoodFactory::createBranchSiteREL(isUsingCompression, fgBranch, nThread, nuclModel, cFreqType, alignName, treeName);
	ptrModel.reset(new StatisticalModel::Model(ptrLik));
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());

	readDefaultParametersType(hlp);
}

/* Read helpers */
void ReaderLikelihoodXML::readDefaultParametersType(const Helper::HelperInterface::sharedPtr_t &hlp) {
	std::string paramType = Tag::DEFAULT;
	//if(XML::readElement(__func__, Tag::PARAMETERS_TAG, pRoot, paramType, OPTIONAL)) {
	if(paramType == Tag::DEFAULT) {
		hlp->defineParameters(ptrModel->getParams());
	} else {
		std::stringstream ss;
		ss << "Parameters '" << paramType << "' does not exists";
		XML::errorMessage(__func__, ss.str());
	}
	/*} else {
		hlp->defineParameters(ptrModel->getParams());
	}*/

}

bool ReaderLikelihoodXML::readIsH1() {
	bool isH1 = true;
	std::string hyp;
	XML::readElement(__func__, Tag::HYPOTHESIS_TAG, pRoot, hyp, MANDATORY);
	if(hyp == Tag::H0) {
		isH1 = false;
		//std::cout << "is H0" << std::endl; // FIXME REMOVE
	} else if(hyp == Tag::H1) {
		isH1 = true;
		//std::cout << "is H1" << std::endl; // FIXME REMOVE
	} else {
		std::stringstream ss;
		ss << "hypothesis must be (H0/H1) : '" << hyp << "' is not a correct value.";
		XML::errorMessage(__func__, ss.str());
	}
	return isH1;
}

bool ReaderLikelihoodXML::readIsCompressed() {
	bool isUsingCompression = true;
	std::string strUseCompression;
	if(XML::readElement(__func__, Tag::USE_COMPRESSION_TAG, pRoot, strUseCompression, OPTIONAL)) {
		if(strUseCompression == Tag::TRUE) {
			isUsingCompression = true;
		} else if(strUseCompression == Tag::FALSE) {
			isUsingCompression = false;
		}
	}
	return isUsingCompression;
}

ReaderLikelihoodXML::nuclModel_t ReaderLikelihoodXML::readNucleotideModel() {
	size_t iNuclModel = 2;
	//XML::readElement(__func__, Tag::NUCL_MODEL_TAG, pRoot, iNuclModel, MANDATORY);
	StatisticalModel::Likelihood::BranchSiteREL::nuclModel_t nuclModel;
	nuclModel = static_cast<StatisticalModel::Likelihood::BranchSiteREL::nuclModel_t>(2);
	return nuclModel;
}

ReaderLikelihoodXML::codonFreq_t ReaderLikelihoodXML::readCodonFrequencyType() {
	size_t iCFreqType = 0;
	XML::readElement(__func__, Tag::CODON_FREQ_TYPE_TAG, pRoot, iCFreqType, MANDATORY);
	StatisticalModel::Likelihood::BranchSiteREL::codonFreq_t cFreqType;
	cFreqType = static_cast<StatisticalModel::Likelihood::BranchSiteREL::codonFreq_t>(iCFreqType);
	return cFreqType;
}


} /* namespace XML */
