//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IDInterval.cpp
 *
 * @date Apr 7, 2017
 * @author meyerx
 * @brief
 */
#include "IDInterval.h"

namespace Utils {
namespace IDManagement {

IDInterval::IDInterval(size_t aLower, size_t aUpper) :
		invervalRange(aLower, aUpper) {
}

IDInterval::~IDInterval() {
}

bool IDInterval::operator<(const IDInterval& other) const {
	bool isSmaller = (invervalRange.lower() < other.invervalRange.lower()) &&
	              (invervalRange.upper() < other.invervalRange.lower());
	return isSmaller;
}

size_t IDInterval::getLowerValue() const {
	return invervalRange.lower();
}

size_t IDInterval::getUpperValue() const {
	return invervalRange.upper();
}

} /* namespace IDManagement */
} /* namespace Utils */
