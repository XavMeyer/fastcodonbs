//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file IDManager.cpp
 *
 * @date Apr 7, 2017
 * @author meyerx
 * @brief
 */
#include "IDManager.h"

#include <assert.h>

#include "Utils/Code/IDManagement/IDInterval.h"

namespace Utils {
namespace IDManagement {

IDManager::IDManager() {
	freeIntervals.insert(IDInterval(0, std::numeric_limits<size_t>::max()));
}

IDManager::IDManager(size_t minValue, size_t maxValue) {
	assert(minValue < maxValue);
	freeIntervals.insert(IDInterval(minValue, maxValue));
}

IDManager::~IDManager() {
}

size_t IDManager::allocateId() {

	IDInterval first = *(freeIntervals.begin());
	size_t freeId = first.getLowerValue();
	freeIntervals.erase(freeIntervals.begin());
	// If the interval is not empty without the first id, create a new one
	if (first.getLowerValue() + 1 <= first.getUpperValue()) {
		freeIntervals.insert(freeIntervals.begin(), IDInterval(first.getLowerValue() + 1 , first.getUpperValue()));
	}

	return freeId;
}

void IDManager::freeId(size_t id) {

	// If already there, nothing to do
	idIntervals_t::iterator it = freeIntervals.find(IDInterval(id,id));

    if (it != freeIntervals.end() && it->getLowerValue() <= id && it->getUpperValue() >= id) {
    	std::cout << "Already here" << std::endl;
    	assert(false && "Freeing an ID that is already free'd.");
        return ;
    }

    // Get the iterator preceding the element
    // i+n < id < j : | [i...i+n] | id | iterator->[j...j+n] |
    it = freeIntervals.upper_bound(IDInterval(id,id));
    if (it == freeIntervals.end()) { // not existing, id is probably out of range (?)
    	assert(false && "Freeing an ID that is out of range.");
    	return ;
        return ;
    } else { // If not, insert it
        IDInterval freeInterval = *(it);
        // The current id is not the first of the next interval ( id+1 != j )
        if (id + 1 != freeInterval.getLowerValue()) {
            freeIntervals.insert(IDInterval(id, id));
        } else { // ( id+1 == j )
        	// We are not inserting in front of the first interval
            if (it != freeIntervals.begin()) {
            	idIntervals_t::iterator it2 = it;
                std::advance(it2, -1);
                //  id+1 == j AND id == i+n+1 : Merge both intervals
                if (it2->getUpperValue() + 1 == id ) {
                    IDInterval freeInterval2 = *(it2);
                    freeIntervals.erase(it);
                    freeIntervals.erase(it2);
                    freeIntervals.insert(freeIntervals.begin(),
                        IDInterval(freeInterval2.getLowerValue(),
                        		freeInterval.getUpperValue()));
                } else { // id+1==j AND id != i+n+1 : change the interval
                	// Optimisation
                	idIntervals_t::iterator prevIt = it;
                	if(prevIt != freeIntervals.begin()) {
                		std::advance(prevIt, -1);
                	} else {
                		prevIt = freeIntervals.begin();
                	}

                    freeIntervals.erase(it);
                    freeIntervals.insert(prevIt, IDInterval(id, freeInterval.getUpperValue()));
                }
			} else {  // id+1==j AND id != i+n+1 : change the interval
				freeIntervals.erase(it);
				freeIntervals.insert(freeIntervals.begin(), IDInterval(id, freeInterval.getUpperValue()));
            }
        }
    }

}

bool IDManager::markAsUsed(size_t id) {
	idIntervals_t::iterator it = std::find_if(freeIntervals.begin(), freeIntervals.end(), IDIntervalContains(id));
	if (it == freeIntervals.end()) { // Already being used
		return false;
	} else { // Not used
		IDInterval freeInterval = *(it);
		// Remove the interval
		freeIntervals.erase(it);
		// Create the new ones
		if (freeInterval.getLowerValue() < id) {
			freeIntervals.insert(IDInterval(freeInterval.getLowerValue(), id-1));
		}
		if (id + 1 <= freeInterval.getUpperValue() ) {
			freeIntervals.insert(IDInterval(id+1, freeInterval.getUpperValue()));
		}
		return true;
	}
}

size_t IDManager::getBiggestAllocatedId() {
	if(freeIntervals.rbegin()->getLowerValue() == 0) return 0;
	else return freeIntervals.rbegin()->getLowerValue()-1;
}

} /* namespace IDManagement */
} /* namespace Utils */
