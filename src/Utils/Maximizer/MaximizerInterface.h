//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaximizerInterface.h
 *
 * @date Oct 24, 2016
 * @author meyerx
 * @brief
 */
#ifndef MAXIMIZERINTERFACE_H_
#define MAXIMIZERINTERFACE_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>

#include "MaxFunctionLB.h"
#include "MaxFunctionPLL.h"
#include "MaximizerFunction.h"
#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Parameter/Parameters.h"
#include "Sampler/Samples/Sample.h"
#include "Utils/Code/CustomProfiling.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Utils {
namespace Maximizer {

namespace SM = ::StatisticalModel;
namespace SM_Lik = ::StatisticalModel::Likelihood;

class MaximizerInterface {
public:
	typedef boost::shared_ptr< MaximizerInterface > sharedPtr_t;
public:
	MaximizerInterface(const size_t aSeed, const SM::Parameters &aParams,
			const Sampler::Sample &aSample, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik);
	virtual ~MaximizerInterface();

	void setSeed(const size_t aSeed);
	void setSequence(const std::vector<size_t> &seq);
	void setStartingPoint(const std::vector<double> &aValues);

	size_t getSeed() const;
	size_t getNFunctionCall() const;
	const std::vector<double>& getValues() const;
	double getExecTime() const;

	void printMLE() const;
	std::string toStringMLE() const;

	virtual double maximize(size_t aMaxEval=0) = 0;

protected:

	size_t seed, nFuncCall;
	const SM::Parameters &params;
	SM_Lik::LikelihoodInterface::sharedPtr_t ptrLik;
	CustomProfiling cp;

	std::vector<double> values;
	std::vector<double> lBound;
	std::vector<double> uBound;
	std::vector<size_t> sequence;
};

} /* namespace Maximizer */
} /* namespace Utils */

#endif /* MAXIMIZERINTERFACE_H_ */
