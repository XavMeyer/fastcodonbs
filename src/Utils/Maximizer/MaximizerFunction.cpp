//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaximizerFunction.cpp
 *
 * @date May 27, 2015
 * @author meyerx
 * @brief
 */
#include "MaximizerFunction.h"

#include <assert.h>

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Parameter/Parameters.h"
#include "Sampler/Samples/Sample.h"

namespace Utils {
namespace Maximizer {

// See Chapter 8 of -> Nocedal, Jorge, and Stephen J. Wright. Numerical Optimization. 2nd ed. Springer Series in Operations Research. New York: Springer, 2006.
const double MaximizerFunction::DELTA_GRADIENT = std::pow(std::numeric_limits<double>::epsilon(), 1./3.);//(sqrt(std::numeric_limits<double>::epsilon()));

MaximizerFunction::MaximizerFunction(const SM::Parameters &aParams, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
	first(true), nCall(0), params(aParams), ptrLik(aPtrLik){

	// init pIndFullUpd
	for(size_t i=0; i<params.getNBaseParameters(); ++i) {
		if(params.getBoundMin(i) != params.getBoundMax(i)) {
			pIndFullUpd.push_back(i);
		}
	}
	gradientTime.resize(params.getNBaseParameters()+1);
}

MaximizerFunction::MaximizerFunction(const std::vector<size_t> &aSequence,
		const SM::Parameters &aParams, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
	first(true), nCall(0), params(aParams), ptrLik(aPtrLik), pIndFullUpd(aSequence) {

	// init pIndFullUpd
	for(size_t i=0; i<pIndFullUpd.size(); ++i) {
		size_t pId = pIndFullUpd[i];
		//std::cout << pId << "\t" << params.getBoundMin(pId) << "\t" << params.getBoundMax(pId) << std::endl;
		assert(params.getBoundMin(pId) != params.getBoundMax(pId));
	}
	gradientTime.resize(params.getNBaseParameters()+1);
}



MaximizerFunction::~MaximizerFunction() {
}

std::string MaximizerFunction::getTimeReport() const {
	std::stringstream ssTR;
	ssTR << "Likelihood f0 = " << boost::accumulators::mean(gradientTime.back());
	ssTR << std::endl;
	for(size_t iG=0; iG<gradientTime.size()-1; ++iG) {
		ssTR << "Params [ " << iG << " ]\t Average time : " << boost::accumulators::mean(gradientTime[iG]);
		ssTR << std::endl;
	}

	ssTR << boost::accumulators::mean(gradientTime.back()) << "\t";
	for(size_t iG=0; iG<gradientTime.size()-1; ++iG) {
		ssTR <<  boost::accumulators::mean(gradientTime[iG]) << "\t";
	}
	ssTR << std::endl;

	return ssTR.str();
}


size_t MaximizerFunction::getNCall() const {
	return nCall;
}

double MaximizerFunction::operator()(const std::vector<double>& aValues, std::vector<double>& aGrad) {
	double res = defaultComputation(aValues, aGrad);

	//std::vector<double> gradient(ptrLik->getGradient());

	/*std::cout << "f0 = " << res << std::endl;
	std::cout << aValues.size() << " - " << aGrad.size() << std::endl;
	for(size_t i=0; i<aValues.size(); ++i) {
		std::cout << aValues[i] << "\t:\t" << aGrad[i] << std::endl;
	}*/

	return res;
}

double MaximizerFunction::computeLikelihood(const std::vector<size_t> &pInd, const std::vector<double>& aValues) {
	++nCall;
	//std::cout << "nCall : " << nCall << std::endl;

	Sampler::Sample sample;
	sample.setDblValues(aValues);

	if(ptrLik->isUpdatable() && !first) {
		return ptrLik->update(pInd, sample);
		//return ptrLik->processLikelihood(sample);
	} else {
		first = false;
		double lik = ptrLik->processLikelihood(sample);

		/*std::vector<double> gradient(ptrLik->getGradient());

		std::cout << "STAN GRADIENT = ";
		for(double grad : gradient) {
			std::cout << grad << "\t";
		}
		std::cout << std::endl;*/

		return lik;
	}

}

double MaximizerFunction::computeGradient(const double f0, const size_t pId, const std::vector<double>& aValues) {
	//++nCall;

	//std::cout << params.getName(pId) << " = " << aValues[pId] << "\t"; // TODO

	//std::cout << "[0] MaximizerFunction::computeGradient" << std::endl;


	Sampler::Sample sample;
	sample.setDblValues(aValues);

	//std::cout << "[1] MaximizerFunction::computeGradient" << std::endl;

	double dx = DELTA_GRADIENT * (aValues[pId]+1.0);
	//double dx = DELTA_GRADIENT;
	sample.incParameter(pId, dx);

	if(sample.getDoubleParameter(pId) >= params.getBoundMax(pId)) {
		sample.incParameter(pId, -2*dx);
		dx = -dx;
	}

	//std::cout << "[2] MaximizerFunction::computeGradient" << std::endl;

	double f1;
	std::vector<size_t> pIndices(1, pId);
	if(ptrLik->isUpdatable()) {
		f1 = ptrLik->update(pIndices, sample);
		//f1 = ptrLik->processLikelihood(sample);
	} else {
		f1 = ptrLik->processLikelihood(sample);
	}
	//if(isnan(f1)) f1 = -1e10; // TODO HORRIBLE FIX

	//std::cout << "f1 = " << f1 << "\t delta = " << (f1-f0) << "\t dx = " << dx << "\t res = " << (f1-f0)/dx <<  std::endl; // TODO


		//std::cout << "[3] MaximizerFunction::computeGradient" << std::endl;

	double gradient = (f1-f0)/dx;

	//std::cout << "f1 = " << f1 << "\t gradient = " << gradient << std::endl; // TODO

	return gradient;
}


double MaximizerFunction::defaultComputation(const std::vector<double>& aValues, std::vector<double>& aGrad) {



#if TI_USE_STAN
	++nCall;
	Sampler::Sample sample;
	sample.setDblValues(aValues);

	cp.startTime();
	double f0 = ptrLik->processLikelihood(sample);
	aGrad = ptrLik->getGradient();
	cp.endTime();

	#if MAX_STD_NICE_OUTPUT
		std::cout << '\r' << "                                                                 ";
		std::cout << '\r' << nCall << " => f0 = " << std::fixed << std::setprecision(8) << f0;
	#endif

	/*std::cout << "STAN GRADIENT = ";
	for(double grad : gradient) {
		std::cout << grad << "\t";
	}
	std::cout << std::endl;*/

#else

	cp.startTime();
	double f0 = computeLikelihood(pIndFullUpd, aValues);
	cp.endTime();
	gradientTime.back()(cp.duration());

#if MAX_STD_NICE_OUTPUT
	std::cout << '\r' << "                                                                 ";
	std::cout << '\r' << nCall << " => f0 = " << std::fixed << std::setprecision(8) << f0;
#endif

	/*std::cout << std::fixed << std::setprecision(6) << "f0 = " << f0 << std::endl;
	for(size_t i=0; i<aValues.size(); ++i) {
			std::cout << aValues[i] <<"\t";
	}
	std::cout << std::endl << "******" << std::endl;*/

	if(!aGrad.empty()) {
		aGrad.assign(aGrad.size(), 0.); // init aGrad to 0;

		for(size_t i=0; i<pIndFullUpd.size(); ++i) {
			size_t pId = pIndFullUpd[i];
			cp.startTime();
			aGrad[pId] = computeGradient(f0, pId, aValues);
			cp.endTime();
			gradientTime[i](cp.duration());
		}
	}

	//std::cout << "-------------------------------------------------------" << std::endl;

#endif


	return f0;
}

} /* namespace Maximizer */
} /* namespace Utils */
