//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaxFunctionPLL.h
 *
 * @date Aug 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef MAXFUNCTIONPLL_H_
#define MAXFUNCTIONPLL_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <stddef.h>
#include <iomanip>

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Parameter/Parameters.h"
#include "Model/Prior/PriorInterface.h"
#include "Utils/Code/CustomProfiling.h"
#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

namespace Parallel { class MLManager; }
namespace StatisticalModel { class Parameters; }
namespace boost { namespace accumulators { namespace tag { struct mean; } } }

#define MAX_PLL_NICE_OUTPUT 0

namespace Utils {
namespace Maximizer {

namespace SM = ::StatisticalModel;
namespace SM_Lik = ::StatisticalModel::Likelihood;

class MaxFunctionPLL {
public:
	MaxFunctionPLL(const SM::Parameters &aParams, SM_Lik::LikelihoodInterface::sharedPtr_t &aPtrLik);
	MaxFunctionPLL(const std::vector<size_t> &aSequence, const SM::Parameters &aParams, SM_Lik::LikelihoodInterface::sharedPtr_t &aPtrLik);
	~MaxFunctionPLL();

	std::string getTimeReport() const;
	size_t getNCall() const;

	double launchWorker();
	void stopWorkers(const double maxLik);

	static double callManager(const std::vector<double> &x, std::vector<double> &grad, void *data) {
		return (*reinterpret_cast<MaxFunctionPLL*>(data))(x, grad);
	}

private:
	static const double DELTA_GRADIENT;

	bool first;
	size_t nCall, nLocalGradients;
	const SM::Parameters &params;
	SM_Lik::LikelihoodInterface::sharedPtr_t ptrLik;
	Parallel::MLManager &mlMgr;
	CustomProfiling cp;

	std::vector<size_t> pIndFullUpd;
	std::vector<int> myGradientsId;
	std::vector< std::pair<int, int> > liksAssignment;

	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accTime_t;
	accTime_t accTime;
	std::vector<accTime_t> localLikTime;

	double operator()(const std::vector<double>& aValues, std::vector<double>& aGrad);

	double computeLikelihood(const std::vector<size_t> &pInd, const std::vector<double>& aValues);
	double computeGradient(const double f0, const size_t pId, const std::vector<double>& aValues);

	double defaultComputation(const std::vector<double>& aValues, std::vector<double>& aGrad);

	double launchManager(const std::vector<double>& aValues, std::vector<double>& aGrad);
	void computeLocalLikelihoods(const std::vector<double> &aValues, std::vector<double> &localLikelihoods);
	void computeGradients(const double f0, const std::vector<double>& aValues,
						  const std::vector<double> &gatherLiks, std::vector<double>& aGrad);

};

} /* namespace Maximizer */
} /* namespace Utils */

#endif /* MAXIMIZERFUNCTION_H_ */

