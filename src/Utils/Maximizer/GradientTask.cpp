//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GradientTask.cpp
 *
 * @date Aug 23, 2015
 * @author meyerx
 * @brief
 */
#include "GradientTask.h"

namespace Utils {
namespace Maximizer {

GradientTask::GradientTask(const bool aF0, const size_t aIDParam) :
		idParam(aIDParam), f0(aF0) {
	avgTime = 0.;
}

GradientTask::~GradientTask() {
}


bool GradientTask::isF0() const {
	return f0;
}

size_t GradientTask::getIDParam() const {
	return idParam;
}

void GradientTask::addTimeMeasure(const double aTime) {
	accTime(aTime);
}

void GradientTask::setTimeMeasure(const double aTime) {
	avgTime = aTime;
	accTime = accTime_t();
}

double GradientTask::getAverageTime() const {
	if(boost::accumulators::count(accTime) > 0) {
		return boost::accumulators::mean(accTime);
	} else {
		return avgTime;
	}
}

void GradientTask::resetTimeMeasure() {
	avgTime = 0.;
	accTime = accTime_t();
}

} /* namespace Maximizer */
} /* namespace Utils */
