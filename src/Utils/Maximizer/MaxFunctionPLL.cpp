//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaxFunctionPLL.cpp
 *
 * @date Aug 18, 2015
 * @author meyerx
 * @brief
 */
#include "MaxFunctionPLL.h"

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Parameter/Parameters.h"
#include "Parallel/Parallel.h"
#include "Parallel/Manager/MLManager.h"
#include "Sampler/Samples/Sample.h"

namespace Utils {
namespace Maximizer {

// See Chapter 8 of -> Nocedal, Jorge, and Stephen J. Wright. Numerical Optimization. 2nd ed. Springer Series in Operations Research. New York: Springer, 2006.
const double MaxFunctionPLL::DELTA_GRADIENT = std::pow(std::numeric_limits<double>::epsilon(), 1./3.);//(sqrt(std::numeric_limits<double>::epsilon()));

MaxFunctionPLL::MaxFunctionPLL(const SM::Parameters &aParams, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
								first(true), nCall(0), params(aParams), ptrLik(aPtrLik), mlMgr(Parallel::mlMgr()) {

	//Parallel::mpiMgr().print("HERE!"); // FIXME

	// init pIndFullUpd
	//std::vector<size_t> tmpPId; // FIXME BAD CASE
	for(size_t i=0; i<params.getNBaseParameters(); ++i) {
		if(params.getBoundMin(i) != params.getBoundMax(i)) {
			pIndFullUpd.push_back(i);
		}
		//tmpPId.push_back(i); // FIXME BAD CASE
	}
	//Parallel::mpiMgr().getPRNG()->randomShuffle(tmpPId); // FIXME BAD CASE

	// init myGradientsId
	nLocalGradients = ceil((double)params.getNBaseParameters()/(double)mlMgr.getNProcGradient());
	myGradientsId.assign(nLocalGradients, -1);
	liksAssignment.assign(params.getNBaseParameters(), std::make_pair(-1, -1));
	/* DEFAULT
	for(size_t iP=0, iG=0; iP<params.size(); ++iP) {
		double assignToProc = (iP+1) % mlMgr.getNProcGradient();

		//double tmp = tmpPId[iP];  // FIXME BAD CASE

		if(assignToProc == mlMgr.getMyRankGradient()) {
			myGradientsId[iG] = iP;
			//myGradientsId[iG] = tmp;  // FIXME BAD CASE
			++iG;
		}
		liksAssignment[iP] = assignToProc;
		//liksAssignment[tmp] = assignToProc;  // FIXME BAD CASE
	}*/
	/* SEL POS */
	size_t iG = 0;
	std::vector<size_t> cntElem(mlMgr.getNProcGradient(), 0);
	for(size_t iP=0; iP<5; ++iP) {
		double assignToProc = (iP+1) % mlMgr.getNProcGradient();

		if(assignToProc == mlMgr.getMyRankGradient()) {
			myGradientsId[iG] = iP;
			++iG;
		}
		liksAssignment[iP] = std::make_pair(assignToProc, cntElem[assignToProc]);
		cntElem[assignToProc]++;
	}

	size_t ptrBranch = params.getNBaseParameters();
	for(int iT=mlMgr.getNProcGradient()-1; iT>=0; --iT) {
		size_t startB = std::max(5, (int)ptrBranch - (int)(nLocalGradients-cntElem[iT]));
		size_t endB = ptrBranch;
		//if(Parallel::mpiMgr().isMainProcessor())
		std::stringstream ss; ss << iT << "\t" << ptrBranch << "\t" << startB << "\t" << endB << std::endl; Parallel::mpiMgr().print(ss.str()); // FIXME

		ptrBranch = startB;
		for(size_t iB=startB; iB < endB; ++iB) {
			if(iT == mlMgr.getMyRankGradient()) {
				myGradientsId[iG] = iB;
				++iG;
			}
			liksAssignment[iB] = std::make_pair(iT, cntElem[iT]);
			//liksAssignment[iB] = iT;
			cntElem[iT]++;
		}
	}


	/*std::stringstream ss;
	ss << "[ " << mlMgr.getMyRankGradient() << " ]";
	for(size_t iG=0; iG<myGradientsId.size(); ++iG) {
		ss << "\t" << myGradientsId[iG];
	}
	ss << std::endl;
	std::cout << ss.str();

	if(Parallel::mpiMgr().isMainProcessor()) {
		for(size_t iP=0; iP<params.size(); ++iP) {
			std::cout << "[ " << iP << " ]\t" << liksAssignment[iP].first << "\t" << liksAssignment[iP].second << std::endl;
		}
		getchar();
	}*/

	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		localLikTime.resize(nLocalGradients+1);
	} else if(mlMgr.checkRoleGradient(mlMgr.Worker)) {
		localLikTime.resize(nLocalGradients);
	}

}

MaxFunctionPLL::MaxFunctionPLL(const std::vector<size_t> &aSequence, const SM::Parameters &aParams,
		SM_Lik::LikelihoodInterface::sharedPtr_t &aPtrLik) :
				first(true), nCall(0), params(aParams), ptrLik(aPtrLik), mlMgr(Parallel::mlMgr()) {

	// init myGradientsId
	nLocalGradients = params.getNBaseParameters();
	myGradientsId.assign(nLocalGradients, -1);
	liksAssignment.assign(params.getNBaseParameters(), std::make_pair(-1, -1));

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		for(size_t iP=0; iP<aSequence.size(); ++iP) {
			std::cout << "[ " << iP << " ]\t" << aSequence[iP] << std::endl;
		}
		getchar();
	}*/

	size_t iPart = 0, iG = 0;
	for(size_t iS=1; iS<aSequence.size(); ++iS) {
		if(aSequence[iS] != std::numeric_limits<size_t>::max()) {
			if((size_t)mlMgr.getMyRankGradient() == iPart) {
				myGradientsId[iG] = aSequence[iS];
			}
			liksAssignment[aSequence[iS]] = std::make_pair(iPart, iG);
			++iG;
		} else {
			iG = 0;
			++iPart;
		}
	}

	/*std::stringstream ss;
	ss << "[ " << mlMgr.getMyRankGradient() << " ]";
	for(size_t iG=0; iG<myGradientsId.size(); ++iG) {
		ss << "\t" << myGradientsId[iG];
	}
	ss << std::endl;
	std::cout << ss.str();

	if(Parallel::mpiMgr().isMainProcessor()) {
		for(size_t iP=0; iP<params.size(); ++iP) {
			std::cout << "[ " << iP << " ]\t" << liksAssignment[iP].first << "\t" << liksAssignment[iP].second << std::endl;
		}
		getchar();
	}*/

	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		localLikTime.resize(nLocalGradients+1);
	} else if(mlMgr.checkRoleGradient(mlMgr.Worker)) {
		localLikTime.resize(nLocalGradients);
	}

}

MaxFunctionPLL::~MaxFunctionPLL() {
}

std::string MaxFunctionPLL::getTimeReport() const {
	std::stringstream ssTR;
	ssTR << "My local rank is [ " << mlMgr.getMyRankGradient() << " ]" << std::endl;
	ssTR << "Average time : " << boost::accumulators::mean(accTime);
	ssTR << std::endl;
	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		ssTR << "-> Lik f0  \t Average time : " << boost::accumulators::mean(localLikTime.back());
		ssTR << std::endl;
	}
	for(size_t iG=0; iG<myGradientsId.size(); ++iG) {
		if(myGradientsId[iG] < 0) break;
		ssTR << "-> Params [ " << myGradientsId[iG] << " ]\t Average time : " << boost::accumulators::mean(localLikTime[iG]);
		ssTR << std::endl;
	}

	return ssTR.str();
}

size_t MaxFunctionPLL::getNCall() const {
	return nCall;
}

double MaxFunctionPLL::operator()(const std::vector<double>& aValues, std::vector<double>& aGrad) {
	double res = launchManager(aValues, aGrad);
	return res;
}

double MaxFunctionPLL::computeLikelihood(const std::vector<size_t> &pInd, const std::vector<double>& aValues) {
	Sampler::Sample sample;
	sample.setDblValues(aValues);

	if(ptrLik->isUpdatable() && !first) {
		return ptrLik->update(pInd, sample);
	} else {
		first = false;
		return ptrLik->processLikelihood(sample);
	}
}

double MaxFunctionPLL::launchManager(const std::vector<double>& aValues, std::vector<double>& aGrad) {
	double f0;
	bool isFinished = false;

	// Process f0 and others
	if(!aGrad.empty()) {
		mlMgr.broadcastValuesMgr(isFinished, aValues);
		cp.startTime(0);

		cp.startTime(1);
		f0 = computeLikelihood(pIndFullUpd, aValues);
		cp.endTime(1);
		localLikTime.back()(cp.duration(1));
		//std::cout << "[0] Computed f0" << std::endl; // fixme

		std::vector<double> localLikelihoods(nLocalGradients, 0.);
		computeLocalLikelihoods(aValues, localLikelihoods);
		//std::cout << "[0] Computed local" << std::endl; // fixme

		// Gather likelihoods (receiver from workers)
		std::vector<double> gatherLiks(nLocalGradients*mlMgr.getNProcGradient(), 0.);
		mlMgr.gatherLikelihoods(localLikelihoods, gatherLiks);
		//std::cout << "[0] Recv gradient" << std::endl; // fixme
		computeGradients(f0, aValues, gatherLiks, aGrad);
		//std::cout << "[0] Computed gradient" << std::endl; // fixme

		cp.endTime(0);
		accTime(cp.duration(0));
	} else {
		f0 = computeLikelihood(pIndFullUpd, aValues);
	}

	nCall += 1 + aGrad.size();
#if MAX_PLL_NICE_OUTPUT
	std::cout << '\r' << "                                                                 ";
	std::cout << '\r' << nCall << " => f0 = " << std::fixed << std::setprecision(8) << f0;
#endif

	return f0;
}

double MaxFunctionPLL::launchWorker() {
	std::vector<double> dummyVec(1), values(params.getNBaseParameters(), 0.);
	bool isFinished = false;

	// Starting
	mlMgr.broadcastValuesWrk(isFinished, values);
	while(!isFinished) {
		cp.startTime(0);

		// Process local likelihoods
		std::vector<double> localLikelihoods(nLocalGradients, 0.);
		computeLocalLikelihoods(values, localLikelihoods);
		//std::cout << "[1] Computed local" << std::endl; // fixme

		// Gather likelihoods (send to manager)
		mlMgr.gatherLikelihoods(localLikelihoods, dummyVec);
		//std::cout << "[1] Sent local" << std::endl; // fixme

		cp.endTime(0);
		accTime(cp.duration(0));

		// Wait for next iteration
		//std::cout << "[1] wait bcast" << std::endl; // fixme
		mlMgr.broadcastValuesWrk(isFinished, values);
		//std::cout << "[1] after bcast" << std::endl; // fixme
	}

	//std::cout << getTimeReport() << std::endl;

	return values[0]; // contains max lik
}

void MaxFunctionPLL::stopWorkers(const double maxLik) {
	bool isFinished = true;
	std::vector<double> values(params.getNBaseParameters(), 0.);
	values[0] = maxLik;

	mlMgr.broadcastValuesMgr(isFinished, values);

	//std::cout << getTimeReport() << std::endl;
}

void MaxFunctionPLL::computeLocalLikelihoods(const std::vector<double> &aValues, std::vector<double> &localLikelihoods) {

	for(size_t iG=0; iG<nLocalGradients; ++iG) {
		//std::cout << "[" << Parallel::mpiMgr().getRank() << "] Start computing\t -> " << iG << "\t -> " << myGradientsId[iG] << std::endl; // fixme
		if(myGradientsId[iG] < 0) break; // We have processed all our gradients, quit

		// Process our gradient
		size_t pId = myGradientsId[iG];
		Sampler::Sample sample;
		sample.setDblValues(aValues);

		double dx = DELTA_GRADIENT * (aValues[pId]+1.0);
		sample.incParameter(pId, dx);

		double paramValue = sample.getDoubleParameter(pId);
		if(paramValue >= params.getBoundMax(pId)) {
			sample.incParameter(pId, -2*dx);
			dx = -dx;
		}

		cp.startTime(1);
		double f1;
		std::vector<size_t> pIndices(1, pId);
		if(ptrLik->isUpdatable() && !first) {
			f1 = ptrLik->update(pIndices, sample);
		} else {
			first = false;
			f1 = ptrLik->processLikelihood(sample);
		}
		cp.endTime(1);
		localLikTime[iG](cp.duration(1));


		// save local likelihood
		localLikelihoods[iG] = f1;
		//std::cout << "[" << Parallel::mpiMgr().getRank() << "] Stop computing\t -> " << iG << "\t -> " << myGradientsId[iG] << std::endl; // fixme
	}
}

void MaxFunctionPLL::computeGradients(const double f0, const std::vector<double>& aValues,
											const std::vector<double> &gatherLiks, std::vector<double>& aGrad) {

	std::vector<size_t> procCounter(mlMgr.getNProcGradient(), 0);

	for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
		//std::cout << iP << "\t"; // FIXME
		//std::cout.flush(); // FIXME

		if(liksAssignment[iP].first < 0) continue;

		double dx = DELTA_GRADIENT * (aValues[iP]+1.0);
		double tmpVal = aValues[iP] + dx;

		// check if out of bound
		if(tmpVal >= params.getBoundMax(iP)) {
			dx = -dx;
		}

		//std::cout << liksAssignment[iP].first << "\t" << liksAssignment[iP].second << "\t"; // FIXME
		//std::cout.flush(); // FIXME

		// Get the likelihood position in the gather vector
		size_t iProc = liksAssignment[iP].first;
		//size_t iLik = iProc*nLocalGradients+procCounter[iProc];
		size_t iLik = iProc*nLocalGradients+liksAssignment[iP].second;
		//std::cout << iLik << "\t" << gatherLiks.size() << "\t"; // FIXME
		//std::cout.flush(); // FIXME
		double f1 = gatherLiks[iLik];
		procCounter[iProc]++;

		//std::cout << f1  << "\t"; // FIXME
		//std::cout.flush(); // FIXME

		// Compute the gradient
		aGrad[iP] = (f1-f0)/dx;

		//std::cout << aGrad[iP] << std::endl; // FIXME
	}
}

} /* namespace Maximizer */
} /* namespace Utils */
