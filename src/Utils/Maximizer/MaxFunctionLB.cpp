//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaxFunctionLB.cpp
 *
 * @date Aug 18, 2015
 * @author meyerx
 * @brief
 */
#include "MaxFunctionLB.h"

#include <assert.h>

#include "Utils/Maximizer/GradientTask.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Parameter/Parameters.h"
#include "Parallel/Manager/MLManager.h"
#include "Sampler/Samples/Sample.h"

namespace Utils {
namespace Maximizer {

// See Chapter 8 of -> Nocedal, Jorge, and Stephen J. Wright. Numerical Optimization. 2nd ed. Springer Series in Operations Research. New York: Springer, 2006.
const double MaxFunctionLB::DELTA_GRADIENT = std::pow(std::numeric_limits<double>::epsilon(), 1./3.);//(sqrt(std::numeric_limits<double>::epsilon()));
const size_t MaxFunctionLB::LOAD_BALANCING_FREQ = 5;
const size_t MaxFunctionLB::LOAD_BALANCING_MAX_PAIR = 10;
const size_t MaxFunctionLB::LOAD_BALANCING_CONV_TH = 3;
const double MaxFunctionLB::LOAD_BALANCING_SWAP_RATIO = 0.4;
const double MaxFunctionLB::LOAD_BALANCING_DIV_TOL = 0.99;

MaxFunctionLB::MaxFunctionLB(const SM::Parameters &aParams, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
								first(true), nCall(0), params(aParams), ptrLik(aPtrLik), mlMgr(Parallel::mlMgr()) {

	itCounter = cntConv = 0;
	bestTime = std::numeric_limits<double>::infinity();

	// init pIndFullUpd
	for(size_t i=0; i<params.getNBaseParameters(); ++i) {
		if(params.getBoundMin(i) != params.getBoundMax(i)) {
			pIndFullUpd.push_back(i);
		}
	}

	initTaskList(); // FIXME
	//initTaskListBad();
	//if(Parallel::mpiMgr().isMainProcessor()) std::cout << getAssignementReport() << std::endl;

}

MaxFunctionLB::~MaxFunctionLB() {
}


void MaxFunctionLB::initTaskList() {

	taskCnt.assign(mlMgr.getNProcGradient(), 0);
	taskAssignement.resize(params.getNBaseParameters());

	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		myTasks.push_back(GradientTask(true, 0));
	}
	taskCnt[0]++;

	for(size_t iP=0; iP<5; ++iP) {
		double assignToProc = (iP+1) % mlMgr.getNProcGradient();

		if(assignToProc == mlMgr.getMyRankGradient()) {
			myTasks.push_back(GradientTask(false, iP));
		}

		taskAssignement[iP] = assignToProc;
		taskCnt[assignToProc]++;
	}

	size_t ptrBranch = params.getNBaseParameters();
	size_t nLocalGradients = ceil((double)(1+params.getNBaseParameters())/(double)mlMgr.getNProcGradient());
	for(int iT=mlMgr.getNProcGradient()-1; iT>=0; --iT) {
		size_t startB = std::max(5, (int)ptrBranch - (int)(nLocalGradients-taskCnt[iT]));
		size_t endB = ptrBranch;
		ptrBranch = startB;
		for(size_t iB=startB; iB < endB; ++iB) {
			if(iT == mlMgr.getMyRankGradient()) {
				myTasks.push_back(GradientTask(false, iB));
			}
			taskAssignement[iB] = iT;
			taskCnt[iT]++;
		}
	}

	for(int iP=0; iP<mlMgr.getNProcGradient(); ++iP) {
		if(iP==0) {
			paramsBufferOrder.push_back(-1);
		}

		for(size_t iT=0; iT<taskAssignement.size(); ++iT) {
			if((int)taskAssignement[iT] == iP) {
				paramsBufferOrder.push_back(iT);
			}
		}
	}
}


void MaxFunctionLB::initTaskListBad() {

	taskCnt.assign(mlMgr.getNProcGradient(), 0);
	taskAssignement.resize(params.getNBaseParameters());

	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		myTasks.push_back(GradientTask(true, 0));
	}
	taskCnt[0]++;

	std::vector<size_t> tmpPId;
	for(size_t i=0; i<params.getNBaseParameters(); ++i) {
		tmpPId.push_back(i);
	}
	//Parallel::mpiMgr().getPRNG()->randomShuffle(tmpPId);

	size_t ptrBranch = params.getNBaseParameters();
	size_t nLocalGradients = ceil((double)(1+params.getNBaseParameters())/(double)mlMgr.getNProcGradient());
	for(int iT=mlMgr.getNProcGradient()-1; iT>=0; --iT) {
		size_t startB = std::max(0, (int)ptrBranch - (int)(nLocalGradients-taskCnt[iT]));
		size_t endB = ptrBranch;
		ptrBranch = startB;
		for(size_t iB=startB; iB < endB; ++iB) {
			size_t badPID = tmpPId[iB];
			if(iT == mlMgr.getMyRankGradient()) {
				myTasks.push_back(GradientTask(false, badPID));
			}
			taskAssignement[badPID] = iT;
			taskCnt[iT]++;
		}
	}

	for(int iP=0; iP<mlMgr.getNProcGradient(); ++iP) {
		if(iP==0) {
			paramsBufferOrder.push_back(-1);
		}

		for(size_t iT=0; iT<taskAssignement.size(); ++iT) {
			if((int)taskAssignement[iT] == iP) {
				paramsBufferOrder.push_back(iT);
			}
		}
	}
}

void MaxFunctionLB::initTaskList(const std::vector<size_t> &assignement) {

	myTasks.clear();
	taskCnt.assign(mlMgr.getNProcGradient(), 0);
	taskAssignement = assignement;

	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		myTasks.push_back(GradientTask(true, 0));
	}
	taskCnt[0]++;

	for(size_t iParam=0; iParam<taskAssignement.size(); ++iParam) {
		int iProc = taskAssignement[iParam];
		if(iProc == mlMgr.getMyRankGradient()) {
			myTasks.push_back(GradientTask(false, iParam));
		}
		taskCnt[iProc]++;
	}

	paramsBufferOrder.clear();
	for(int iP=0; iP<mlMgr.getNProcGradient(); ++iP) {
		if(iP==0) {
			paramsBufferOrder.push_back(-1);
		}

		for(size_t iT=0; iT<taskAssignement.size(); ++iT) {
			if((int)taskAssignement[iT] == iP) {
				paramsBufferOrder.push_back(iT);
			}
		}
	}
}

std::string MaxFunctionLB::getTimeReport() const {
	std::stringstream ssTR;
	ssTR << "My local rank is [ " << mlMgr.getMyRankGradient() << " ]" << std::endl;
	ssTR << "Average time : " << boost::accumulators::mean(accTime);
	ssTR << std::endl;
	if(mlMgr.checkRoleGradient(mlMgr.Manager)) {
		ssTR << "-> Lik f0  \t Average time : " << myTasks.front().getAverageTime() << std::endl;
	}

	for(itCstListGradTask_t itT=myTasks.begin(); itT != myTasks.end(); itT++) {
		ssTR << "-> Params [ " << itT->getIDParam() << " ]\t Average time : " << itT->getAverageTime() << std::endl;
	}

	return ssTR.str();
}

std::string MaxFunctionLB::getAssignementReport() const {
	std::stringstream ssTR;
	ssTR << "My local rank is [ " << mlMgr.getMyRankGradient() << " ]" << std::endl;

	ssTR << "My params :\t";
	for(itCstListGradTask_t itT=myTasks.begin(); itT != myTasks.end(); itT++) {
		ssTR << itT->getIDParam() << ", ";
	}
	ssTR << std::endl;

	ssTR << "Task cnt :\t";
	for(size_t iC=0; iC < taskCnt.size(); iC++) {
		ssTR << taskCnt[iC] << " ";
	}
	ssTR << std::endl;

	ssTR << "task assignement :\t";
	for(size_t iC=0; iC < taskAssignement.size(); iC++) {
		ssTR << taskAssignement[iC] << ", ";
	}
	ssTR << std::endl;

	ssTR << "PBO :\t";
	for(size_t iC=0; iC < paramsBufferOrder.size(); iC++) {
		ssTR << paramsBufferOrder[iC] << ", ";
	}
	ssTR << std::endl;

	return ssTR.str();
}

size_t MaxFunctionLB::getNCall() const {
	return nCall;
}

double MaxFunctionLB::operator()(const std::vector<double>& aValues, std::vector<double>& aGrad) {
	double res = launchManager(aValues, aGrad);
	return res;
}

double MaxFunctionLB::computeLikelihood(const std::vector<size_t> &pInd, const std::vector<double>& aValues) {
	Sampler::Sample sample;
	sample.setDblValues(aValues);

	if(ptrLik->isUpdatable() && !first) {
		return ptrLik->update(pInd, sample);
	} else {
		first = false;
		return ptrLik->processLikelihood(sample);
	}
}

double MaxFunctionLB::launchManager(const std::vector<double>& aValues, std::vector<double>& aGrad) {
	double f0;

	// Process f0 and others
	if(!aGrad.empty()) {
		if(itCounter > 0 && cntConv < LOAD_BALANCING_CONV_TH && itCounter % LOAD_BALANCING_FREQ == 0) {
			mlMgr.broadcastValuesMgr(mlMgr.LB_COMPUTE, aValues);
			balanceLoadMgr();
		} else {
			mlMgr.broadcastValuesMgr(mlMgr.COMPUTE, aValues);
		}

		cp.startTime(0);

		std::vector<double> localLikelihoods(myTasks.size(), 0.);

		cp.startTime(1);
		f0 = computeLikelihood(pIndFullUpd, aValues);
		localLikelihoods.front() = f0;
		cp.endTime(1);
		myTasks.front().addTimeMeasure(cp.duration(1));

		computeLocalLikelihoods(aValues, localLikelihoods);

		// Gather likelihoods (receiver from workers)
		std::vector<double> gatherLiks(params.getNBaseParameters()+1, 0.);
		mlMgr.gathervDoubleMgr(localLikelihoods, taskCnt, gatherLiks);
		computeGradients(f0, aValues, gatherLiks, aGrad);

		cp.endTime(0);
		accTime(cp.duration(0));

		itCounter++;
	} else {
		f0 = computeLikelihood(pIndFullUpd, aValues);
	}

	nCall += 1 + aGrad.size();
#if MAX_PLL_LB_NICE_OUTPUT
	std::cout << '\r' << "                                                                 ";
	std::cout << '\r' << nCall << " => f0 = " << std::fixed << std::setprecision(8) << f0;
#endif

	return f0;
}

double MaxFunctionLB::launchWorker() {
	std::vector<double>values(params.getNBaseParameters(), 0.);
	Parallel::MLManager::gradient_signal_t signal;

	// Starting
	mlMgr.broadcastValuesWrk(signal, values);
	while(signal != mlMgr.TERMINATE) {

		if(signal == mlMgr.LB_COMPUTE) {
			balanceLoadWkr();
		}

		cp.startTime(0);

		// Process local likelihoods
		std::vector<double> localLikelihoods(myTasks.size(), 0.);
		computeLocalLikelihoods(values, localLikelihoods);

		// Gather likelihoods (send to manager)
		mlMgr.gathervDoubleWrk(localLikelihoods);

		cp.endTime(0);
		accTime(cp.duration(0));

		// Wait for next iteration
		mlMgr.broadcastValuesWrk(signal, values);

		itCounter++;
	}


	return values[0]; // contains max lik
}

void MaxFunctionLB::stopWorkers(const double maxLik) {
	std::vector<double> values(params.getNBaseParameters(), 0.);
	values[0] = maxLik;

	mlMgr.broadcastValuesMgr(mlMgr.TERMINATE, values);

}

void MaxFunctionLB::computeLocalLikelihoods(const std::vector<double> &aValues, std::vector<double> &localLikelihoods) {
	size_t cnt = 0;

	/*std::vector<GradientTask> myVec; // FIXME
	std::copy(myTasks.begin(), myTasks.end(), std::back_inserter(myVec)); // FIXME
	if(!myTasks.front().isF0()) {
		std::random_shuffle(myVec.begin(), myVec.end()); // FIXME
	}*/

	//for(std::vector<GradientTask>::iterator itT=myVec.begin(); itT != myVec.end(); ++itT) { // FIXME
	for(itListGradTask_t itT=myTasks.begin(); itT != myTasks.end(); ++itT) {
		if(itT->isF0()) {
			++cnt;
			continue; // f0 already processed
		}

		// Process our gradient
		size_t pId = itT->getIDParam();
		Sampler::Sample sample;
		sample.setDblValues(aValues);

		double dx = DELTA_GRADIENT * (aValues[pId]+1.0);
		sample.incParameter(pId, dx);

		double paramValue = sample.getDoubleParameter(pId);
		if(paramValue >= params.getBoundMax(pId)) {
			sample.incParameter(pId, -2*dx);
			dx = -dx;
		}

		cp.startTime(1);
		double f1;
		std::vector<size_t> pIndices(1, pId);
		if(ptrLik->isUpdatable() && !first) {
			f1 = ptrLik->update(pIndices, sample);
		} else {
			first = false;
			f1 = ptrLik->processLikelihood(sample);
		}
		cp.endTime(1);
		itT->addTimeMeasure(cp.duration(1));


		// save local likelihood
		localLikelihoods[cnt] = f1;
		++cnt;
	}

	/*
	myTasks.clear(); // FIXME
	std::copy(myVec.begin(), myVec.end(), std::back_inserter(myTasks)); // FIXME
	myTasks.sort(SmallestIDParamFirst()); // FIXME

	size_t iC=0;
	std::vector<double> tmpLik = localLikelihoods;
	for(itListGradTask_t itT=myTasks.begin(); itT != myTasks.end(); ++itT) {
		if(itT->isF0()) {
			iC++;
			continue;
		}
		for(size_t iV=0; iV < myVec.size(); ++iV) {
			if(itT->getIDParam() == myVec[iV].getIDParam()) {
				localLikelihoods[iC] = tmpLik[iV];
				break;
			}
		}
		iC++;
	}*/

}

void MaxFunctionLB::computeGradients(const double f0, const std::vector<double>& aValues,
											const std::vector<double> &gatherLiks, std::vector<double>& aGrad) {

	// We use the paramsOrderBuffer that give us the wich parameter to encouter next in the gatherLiks vector
	for(size_t iPBO=0; iPBO<paramsBufferOrder.size(); ++iPBO) {
		if(paramsBufferOrder[iPBO] < 0) continue;
		size_t iP = paramsBufferOrder[iPBO];

		double dx = DELTA_GRADIENT * (aValues[iP]+1.0);
		double tmpVal = aValues[iP] + dx;

		// check if out of bound
		if(tmpVal >= params.getBoundMax(iP)) {
			dx = -dx;
		}

		double f1 = gatherLiks[iPBO];
		//if(isnan(f1) || f1 < -1e10) f1 = -1e10; // TODO HORRIBLE FIX

		// Compute the gradient
		aGrad[iP] = (f1-f0)/dx;
	}
}

std::vector<double> MaxFunctionLB::compileTimeStats() {
	std::vector<double> localTimes;
	for(itListGradTask_t it=myTasks.begin(); it != myTasks.end(); ++it) {
		localTimes.push_back(it->getAverageTime());
	}

	return localTimes;
}

void MaxFunctionLB::balanceLoadMgr() {
	// Gather times (receiver from workers)
	std::vector<double> localTimes(compileTimeStats());
	std::vector<double> gatherTimes(params.getNBaseParameters()+1, 0.);
	mlMgr.gathervDoubleMgr(localTimes, taskCnt, gatherTimes);

	// Use swap variant
	std::vector<size_t> assignement = swapSomeLB(gatherTimes);
	assert(assignement.size() == params.getNBaseParameters());
	mlMgr.broadcastAssignement(assignement);

	// Create task data structures
	initTaskList(assignement);
}


void MaxFunctionLB::balanceLoadWkr() {
	std::vector<double> localTimes(compileTimeStats());
	mlMgr.gathervDoubleWrk(localTimes);

	// Wait for new assignment
	std::vector<size_t> assignement(params.getNBaseParameters());
	mlMgr.broadcastAssignement(assignement);

	// Create task data structures
	initTaskList(assignement);
}

void MaxFunctionLB::fillTaskAssignement(std::vector<listGradTask_t> &tasksPerProc, std::vector<size_t> &assignement) {
	// Create task assignement
	assignement.assign(params.getNBaseParameters(), 0);
	for(size_t iP=0; iP<tasksPerProc.size(); ++iP) {
		tasksPerProc[iP].sort(SmallestIDParamFirst()); //FIXME
		for(itListGradTask_t it=tasksPerProc[iP].begin(); it != tasksPerProc[iP].end(); ++it) {
			if(!it->isF0()) {
				assignement[it->getIDParam()] = iP;
			}
		}
	}
}


std::vector<size_t> MaxFunctionLB::assignLongerFirstLB(std::vector<double> &gatherTimes){

	listGradTask_t tasks;
	std::vector<double> timePerProc(mlMgr.getNProcGradient(), 0.);
	std::vector<listGradTask_t> tasksPerProc(mlMgr.getNProcGradient());

	// Define the current state
	for(size_t iPBO=0; iPBO<paramsBufferOrder.size(); ++iPBO) {
		int pId = paramsBufferOrder[iPBO];
		if(pId < 0) {
			tasks.push_back(GradientTask(true, 0));
		} else {
			tasks.push_back(GradientTask(false, pId));
		}
	}

	// Order all task by time (with f0 first)
	tasks.sort(BiggestTimeFirst());

	// while there are task
	while(!tasks.empty()) {
		// find first proc ready
		std::vector<double>::iterator it = std::min_element(timePerProc.begin(), timePerProc.end());
		size_t idFirstRdyProc = std::distance(timePerProc.begin(), it);

		// Push task into first rdy proc and pop task
		tasksPerProc[idFirstRdyProc].push_back(tasks.front());
		tasks.pop_front();

	}

	// Create task assignement
	std::vector<size_t> assignement;
	fillTaskAssignement(tasksPerProc,  assignement);
	return assignement;

}



double MaxFunctionLB::swapTask(double deltaT, listGradTask_t &slowProcTasks, listGradTask_t &fastProcTasks) {

	// Find closest task
	itListGradTask_t itClosest = slowProcTasks.end();
	double dist = std::numeric_limits<double>::infinity();
	for(itListGradTask_t it=slowProcTasks.begin(); it != slowProcTasks.end(); ++it) {
		double taskDist = fabs(it->getAverageTime() - (deltaT/2.));
		if(!it->isF0() && taskDist < dist) {
			itClosest = it;
			dist = taskDist;
		}
	}

	// do swap
	if(dist < (LOAD_BALANCING_SWAP_RATIO*deltaT)) {
		double taskTime = itClosest->getAverageTime();
		fastProcTasks.push_back(*itClosest);
		slowProcTasks.erase(itClosest);
		return taskTime;
	} else {
		return -1.;
	}
}


std::vector<size_t> MaxFunctionLB::swapSomeLB(std::vector<double> &gatherTimes) {
	std::vector<double> timePerProc(mlMgr.getNProcGradient(), 0.);
	std::vector<listGradTask_t> tasksPerProc(mlMgr.getNProcGradient());

	// Define the current state
	for(size_t iPBO=0; iPBO<paramsBufferOrder.size(); ++iPBO) {
		int pId = paramsBufferOrder[iPBO];
		if(pId < 0) {
			tasksPerProc[0].push_back(GradientTask(true, 0));
			tasksPerProc[0].back().setTimeMeasure(gatherTimes[iPBO]);
			timePerProc[0] += gatherTimes[iPBO];
		} else {
			size_t iProc = taskAssignement[pId];
			tasksPerProc[iProc].push_back(GradientTask(false, pId));
			tasksPerProc[iProc].back().setTimeMeasure(gatherTimes[iPBO]);
			timePerProc[iProc] += gatherTimes[iPBO];
		}
	}

	std::vector<double>::iterator itTmp;
	itTmp = std::max_element(timePerProc.begin(), timePerProc.end());
	size_t idTmp = std::distance(timePerProc.begin(), itTmp);
#if MAX_PLL_LB_INFO
	std::cout << "Max time : " << 1.e3*timePerProc[idTmp] << std::endl;
#endif

	double newTime = timePerProc[idTmp];
	if(bestTime < LOAD_BALANCING_DIV_TOL*newTime) {
		cntConv++;
		return bestAssignement;
	}

	// keep track of best assignement
	bestTime = newTime;
	fillTaskAssignement(tasksPerProc,  bestAssignement);

	// While max swap not reached
	for(size_t iIter=0; iIter < LOAD_BALANCING_MAX_PAIR; ++iIter) {
		std::vector<double>::iterator it;
		// Max bin
		it = std::max_element(timePerProc.begin(), timePerProc.end());
		size_t idSlowProc = std::distance(timePerProc.begin(), it);

		// Min bin
		it = std::min_element(timePerProc.begin(), timePerProc.end());
		size_t idFastProc = std::distance(timePerProc.begin(), it);

		double diffTime = timePerProc[idSlowProc] - timePerProc[idFastProc];
		double taskTime = swapTask(diffTime, tasksPerProc[idSlowProc], tasksPerProc[idFastProc]);
		// If there was not task to swap, we are done
		if(taskTime <= 0) {
			if(iIter == 0) {
				cntConv++;
			} else {
				cntConv = 0;
			}
#if MAX_PLL_LB_INFO
			std::cout << "N swap : " << iIter << std::endl;
#endif
			break;
		}
		// Else we update times
		timePerProc[idSlowProc] -= taskTime;
		timePerProc[idFastProc] += taskTime;

#if MAX_PLL_LB_INFO
		if(iIter == LOAD_BALANCING_MAX_PAIR-1) std::cout << "N swap : " << LOAD_BALANCING_MAX_PAIR << std::endl;
#endif
	}

	// Create task assignement
	std::vector<size_t> assignement;
	fillTaskAssignement(tasksPerProc,  assignement);

	return assignement;
}


} /* namespace Maximizer */
} /* namespace Utils */
