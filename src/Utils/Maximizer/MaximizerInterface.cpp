//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaximizerInterface.cpp
 *
 * @date Oct 24, 2016
 * @author meyerx
 * @brief
 */
#include "MaximizerInterface.h"

#include <assert.h>

#include "Model/Likelihood/Helper/HelperInterface.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { class Parameters; }

namespace Utils {
namespace Maximizer {

MaximizerInterface::MaximizerInterface(const size_t aSeed, const SM::Parameters &aParams,
		const Sampler::Sample &aSample, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
		seed(aSeed), nFuncCall(0), params(aParams), ptrLik(aPtrLik), values(aSample.getDblValues()){
}

MaximizerInterface::~MaximizerInterface() {
}

void MaximizerInterface::setSeed(const size_t aSeed) {
	seed = aSeed;
}

void MaximizerInterface::setSequence(const std::vector<size_t> &seq) {
	sequence = seq;
}

void MaximizerInterface::setStartingPoint(const std::vector<double> &aValues) {
	assert(values.size() == aValues.size());
	values = aValues;

	for(size_t i=0; i<params.getNBaseParameters(); ++i) {
		if(params.getBoundMin(i) > values[i] || params.getBoundMax(i) < values[i]) {
			values[i] = params.getBoundMin(i) + (params.getBoundMax(i) - params.getBoundMin(i))/2.0;
		}
	}

}

size_t MaximizerInterface::getSeed() const {
	return seed;
}

size_t MaximizerInterface::getNFunctionCall() const {
	return nFuncCall;
}

const std::vector<double>& MaximizerInterface::getValues() const {
	return values;
}

double MaximizerInterface::getExecTime() const {
	return cp.duration();
}

void MaximizerInterface::printMLE() const {
	using namespace StatisticalModel::Likelihood;
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());
	Sample MLE;
	MLE.setDblValues(values);
	std::vector<size_t> pInd;
	ptrLik->update(pInd, MLE);
	hlp->printSample(params, MLE);
}

std::string MaximizerInterface::toStringMLE() const {
	using namespace StatisticalModel::Likelihood;
	Helper::HelperInterface::sharedPtr_t hlp = Helper::HelperInterface::createHelper(ptrLik.get());
	Sample MLE;
	MLE.setDblValues(values);
	std::vector<size_t> pInd;
	ptrLik->update(pInd, MLE);
	return hlp->sampleToString(params, MLE);
}

} /* namespace Maximizer */
} /* namespace Utils */
