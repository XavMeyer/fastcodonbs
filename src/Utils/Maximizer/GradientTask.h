//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GradientTask.h
 *
 * @date Aug 23, 2015
 * @author meyerx
 * @brief
 */
#ifndef GRADIENTTASK_H_
#define GRADIENTTASK_H_

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <cstddef>
#include <list>
#include <vector>

#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>

namespace boost { namespace accumulators { namespace tag { struct mean; } } }

namespace Utils {
namespace Maximizer {

class GradientTask {
public:
	GradientTask(const bool aF0, const size_t aIDParam);
	~GradientTask();

	bool isF0() const;
	size_t getIDParam() const;

	void addTimeMeasure(const double);
	void setTimeMeasure(const double);
	double getAverageTime() const;
	void resetTimeMeasure();

private:

	size_t idParam;
	bool f0;
	double avgTime;
	typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::mean> > accTime_t;
	accTime_t accTime;

};

//gradList.sort(SmallestIDParamFirst());
struct SmallestIDParamFirst {
	bool operator() (const GradientTask& lhs, const GradientTask& rhs) const {
		if(lhs.isF0()) {
			return true;
		} else if(rhs.isF0()) {
			return false;
		}
		return lhs.getIDParam() < rhs.getIDParam();
	}
};

//gradList.sort(BiggestTimeFirst());
struct BiggestTimeFirst {
	bool operator() (const GradientTask& lhs, const GradientTask& rhs) const {
		if(lhs.isF0()){
			return true;
		} else if(rhs.isF0()) {
			return false;
		}

		return lhs.getAverageTime() > rhs.getAverageTime();
	}
};

//std::find_if( gradList.begin(), gradList.end(), GradientTaskWithIDParam( 5 ) );
struct GradientTaskWithIDParam {
	GradientTaskWithIDParam(const size_t aIParam) : idParam(aIParam) {};
	~GradientTaskWithIDParam();

    bool operator () ( const GradientTask &gradTask ) {
    	return gradTask.getIDParam() == idParam;
    }
private:
    double idParam;
};

typedef std::list<GradientTask> listGradTask_t;
typedef listGradTask_t::iterator itListGradTask_t;
typedef listGradTask_t::const_iterator itCstListGradTask_t;

} /* namespace Maximizer */
} /* namespace Utils */

#endif /* GRADIENTTASK_H_ */
