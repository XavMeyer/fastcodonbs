//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaximizerLBFGSB.h
 *
 * @date Oct 18, 2016
 * @author meyerx
 * @brief Maximizer using LFBFGS-B(Morales & Nocedale) and C-Wrapper (https://github.com/stephenbeckr/L-BFGS-B-C)
 *
 * J.L. Morales and J. Nocedal.
 * L-BFGS-B: Remark on Algorithm 778: L-BFGS-B, FORTRAN routines for large scale bound constrained optimization (2011),
 * ACM Transactions on Mathematical Software, Vol 38, Num. 1.
 *
 *
 */
#ifndef MAXIMIZER_LBFGSB_H_
#define MAXIMIZER_LBFGSB_H_

#include <stddef.h>

#include "../MaximizerInterface.h"
#include "Model/Likelihood/LikelihoodInterface.h"

namespace Sampler { class Sample; }
namespace StatisticalModel { class Parameters; }

namespace Utils {
namespace Maximizer {

namespace SM = ::StatisticalModel;
namespace SM_Lik = ::StatisticalModel::Likelihood;

class MaximizerLBFGSB : public MaximizerInterface {
public:
	MaximizerLBFGSB(const size_t aSeed, const SM::Parameters &aParams, const Sampler::Sample &aSample, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik);
	~MaximizerLBFGSB();

	double maximize(size_t aMaxEval=0);

private:

	typedef double (&wrapperFunc_t)(const std::vector<double>&, std::vector<double>&, void*);

	static const double DEFAULT_FACTR, DEFAULT_PGTOL;

	long int result;

	double maximizeSequential(size_t aMaxEval);
	double maximizeParallel(size_t aMaxEval);

	double optimize(size_t MAX_EVAL, wrapperFunc_t wrap, void *compute);
	void printResult(const double maxLik, const long int result) const;

};

} /* namespace Maximizer */
} /* namespace Utils */

#endif /* MAXIMIZER_LBFGSB_H_ */
