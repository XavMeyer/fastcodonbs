//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MaximizerLBFGSB.cpp
 *
 * @date Oct 18, 2016
 * @author meyerx
 * @brief
 */
#include "MaximizerLBFGSB.h"

#include <assert.h>

#include "Utils/Maximizer/MaxFunctionPLL.h"
#include "Utils/Maximizer/MaximizerFunction.h"
#include "Utils/Maximizer/MaximizerInterface.h"
#include "Model/Parameter/Parameters.h"
#include "Parallel/Manager/MLManager.h"
#include "Parallel/Manager/ParallelLikManager.h"
#include "lbfgsb.h"
#include "new"

#include "../../Output/OutputManager.h"
namespace Sampler { class Sample; }
/*extern "C" {
	  int setulb_(long int *, long int *, double *,
			  double *, double *, long int *, double *, double *,
			  double *, double *, double *, long int *, char *,
			  long int *, char *, long int *, long int *, double *);

}*/

namespace Utils {
namespace Maximizer {

const double MaximizerLBFGSB::DEFAULT_FACTR = 1.e4;
const double MaximizerLBFGSB::DEFAULT_PGTOL = 1.e-9;

MaximizerLBFGSB::MaximizerLBFGSB(const size_t aSeed, const SM::Parameters &aParams,
		const Sampler::Sample &aSample, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
		MaximizerInterface(aSeed, aParams, aSample, aPtrLik) {
	result = 0;
}

MaximizerLBFGSB::~MaximizerLBFGSB() {
}

double MaximizerLBFGSB::maximize(size_t aMaxEval){

	if(Parallel::mlMgr().checkRoleGradient(Parallel::pllMgr().Sequential)) {
		return maximizeSequential(aMaxEval);
	} else {
		return maximizeParallel(aMaxEval);
	}
}

double MaximizerLBFGSB::maximizeSequential(size_t aMaxEval) {

	// Set bounds
	for(size_t i=0; i<params.getNBaseParameters(); ++i) {
		lBound.push_back(params.getBoundMin(i));
		uBound.push_back(params.getBoundMax(i));
		if(params.getBoundMin(i) > values[i] || params.getBoundMax(i) < values[i]) {
			std::cerr << "[LikelihoodMaximizer] Proposed values is outside bounds : params [" <<i << "]  val =" << std::setprecision(3) << values[i] << "\t bounds = {" << params.getBoundMin(i) << ", " << params.getBoundMax(i) << "}" << std::endl;
			values[i] = params.getBoundMin(i);
		}
	}

	cp.startTime();
	//double start = MPI_Wtime();

	// Bind function
	//MaximizerFunction compute(params, ptrLik);
	MaximizerFunction *compute = NULL;
	if(sequence.empty()) {
		compute = new MaximizerFunction(params, ptrLik);
	} else {
		compute = new MaximizerFunction(sequence, params, ptrLik);
	}

	double maxLik = optimize(aMaxEval, MaximizerFunction::wrapFunction, reinterpret_cast<void*>(compute));

#if MAX_STD_NICE_OUTPUT
	std::cout << std::endl;
#endif
	nFuncCall = compute->getNCall();

	//double end = MPI_Wtime();
	//std::cout << "MPI duration : " << end-start << std::endl;
	cp.endTime();

	//std::cout << compute->getTimeReport() << std::endl;

	if(Parallel::mpiMgr().isMainProcessor()) printResult(maxLik, result);

	delete compute;

	return maxLik;
}



double MaximizerLBFGSB::maximizeParallel(size_t aMaxEval) {
	double maxLik = 0.;

	if(Parallel::mlMgr().checkRoleGradient(Parallel::pllMgr().Manager)) {

		// Set bounds
		for(size_t i=0; i<params.getNBaseParameters(); ++i) {
			lBound.push_back(params.getBoundMin(i));
			uBound.push_back(params.getBoundMax(i));
			if(params.getBoundMin(i) > values[i] || params.getBoundMax(i) < values[i]) {
				std::cerr << "[LikelihoodMaximizer] Proposed values is outside bounds : params [" <<i << "]  val =" << std::setprecision(3) << values[i] << "\t bounds = {" << params.getBoundMin(i) << ", " << params.getBoundMax(i) << "}" << std::endl;
				values[i] = params.getBoundMin(i);
			}
		}

		cp.startTime();

		// Bind function
		//MaxFunctionPLL computePLL(params, ptrLik); // FIXME
		if(sequence.empty()) {
			MaxFunctionLB computePLL(params, ptrLik); // FIXME

			maxLik = optimize(aMaxEval, MaxFunctionLB::callManager, reinterpret_cast<void*>(&computePLL));

#if MAX_PLL_NICE_OUTPUT
			std::cout << std::endl;
#endif
			nFuncCall = computePLL.getNCall();

			cp.endTime();
			computePLL.stopWorkers(maxLik);

			if(Parallel::mpiMgr().isMainProcessor()) printResult(maxLik, result);
		} else {
			MaxFunctionPLL computePLL(sequence, params, ptrLik); // FIXME

			maxLik = optimize(aMaxEval, MaxFunctionPLL::callManager, reinterpret_cast<void*>(&computePLL));

#if MAX_PLL_NICE_OUTPUT
			std::cout << std::endl;
#endif
			nFuncCall = computePLL.getNCall();

			cp.endTime();
			computePLL.stopWorkers(maxLik);

			if(Parallel::mpiMgr().isMainProcessor()) printResult(maxLik, result);
		}
	} else if(Parallel::mlMgr().checkRoleGradient(Parallel::pllMgr().Worker)) {
		//MaxFunctionPLL computePLL(params, ptrLik); // FIXME
		if(sequence.empty()) {
			MaxFunctionLB computePLL(params, ptrLik); // FIXME
			maxLik = computePLL.launchWorker();
		} else {
			MaxFunctionPLL computePLL(sequence, params, ptrLik); // FIXME
			maxLik = computePLL.launchWorker();
		}
	}

	return maxLik;
}

/*void LBFGSB::printResult(const double maxLik, const nlopt::result &result) const {
	switch(result)
	{
	case nlopt::SUCCESS:
		break;

	case nlopt::STOPVAL_REACHED:
		std::cout << "Optimization stopped because stopval was reached." << std::endl;
		break;

	case nlopt::FTOL_REACHED:
		std::cout << "Optimization stopped because ftol_rel or ftol_abs was reached." << std::endl;
		break;

	case nlopt::XTOL_REACHED:
		std::cout << "Optimization stopped because xtol_rel or xtol_abs was reached." << std::endl;
		break;

	case nlopt::MAXEVAL_REACHED:
		std::cout << "Optimization stopped because maxeval was reached." << std::endl;
		break;

	case nlopt::MAXTIME_REACHED:
		std::cout << "Optimization stopped because maxtime was reached." << std::endl;
		break;

	case nlopt::EXTRAPOLATION_FAIL :
		std::cout << "Intra-Extrapolation limits reached." << std::endl;
		break;

	default:
		std::cout << "Other reason: " << static_cast<unsigned int>(result) << std::endl;
		break;
	}
	std::cout << "------------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Final log-likelihood value : " << std::fixed << maxLik << "  - after " << nFuncCall << " function calls ( ";
	std::cout << (1000.*cp.duration())/(double)nFuncCall << " ms per call)." << std::endl;
	std::cout << "------------------------------------------------------------------------------------------------" << std::endl;

}*/



double MaximizerLBFGSB::optimize(const size_t MAX_EVAL, wrapperFunc_t wrap, void *compute) {

	// Not so clean but working.
	// **** Variables definitions **** //
	int cntErr = 0;
	bool hasFinished = false;

	long int nMax = 3*params.getNBaseParameters()/2;
	long int mMax = 25;

	long int n = params.getNBaseParameters();							// Nb of parameters
	long int m = 20;									// Stored vector

	std::vector<double> initVal(values), bestX(values);
	double *x = values.data();							// Initial value
	double *l = lBound.data();							// Lower bound
	double *u = uBound.data();							// Upper bound

	std::vector<long int> boundType(n, 2);				// 2 means upper and lower bounded
	long int *nbd = boundType.data();

	double f;											// f(x) value
	double bestF = -std::numeric_limits<double>::infinity();
	std::vector<double> grad(n, 0);						// Gradients value
	//double *g = grad.data();
	double factr = DEFAULT_FACTR;								// Precision for convergence (1e12 low, 1e7 moderate, 1e1 high)
	double pgtol = DEFAULT_PGTOL;								// Tolerance on the gradient vector projection (??)

	size_t sizeWA = (2*mMax+5)*nMax+12*mMax*mMax+12*mMax;
	std::vector<double> wa(sizeWA);						// working array
	double *ptrWA = wa.data();
	std::vector<long int> iwa(3*nMax);					// Working array
	long int *ptrIWA = iwa.data();
	long int task = START;								// Task to accomplish
	long int iprint = -1;								// verbosity

    long int csave;										// Working array
    double dsave[29];									// Working array
    long int isave[44];									// Working array
    long int lsave[4];									// Working array


    // **** Main loop, calling the lbfgs-b algorithm **** //
    size_t cntIter = 0;
    // Stoping criterion is convergence or MAX_EVAL reached
	while(!hasFinished && (MAX_EVAL == 0 || cntIter < MAX_EVAL)) {

		// Call Fortran -> C function
		setulb(&n, &m, x, l, u, nbd, &f, grad.data(), &factr, &pgtol, ptrWA, ptrIWA, &task,
						&iprint, &csave, lsave, isave, dsave);

		if (IS_FG(task)) { //
			// The minimization routine ask f(x) and delta f(x) at x

			// We compute it and change f sign (minimize -> maximize)
			f = -wrap(values, grad, compute);

			// Change the gradient direction (minimize -> maximize)
			// Check for error in gradient computation and reduce bounds if needed
			bool hasEncouteredAnError = false;
			for(size_t iG=0; iG<grad.size(); ++iG) {
				grad[iG] = -grad[iG]/5000.0;
				if((cntErr >= 0 && cntErr < 6) &&
						(grad[iG] == std::numeric_limits<double>::infinity() ||
						 grad[iG] == -std::numeric_limits<double>::infinity() ||
						 grad[iG] != grad[iG])) {
					if(x[iG] == l[iG]) {
						l[iG] += (u[iG]-l[iG])/10.;
						//std::cout << "Parameter " << params.getName(iG) << " new bound : "<< l[iG] << " :: " << u[iG] << std::endl;
						assert(l[iG] <= u[iG]);
					} else if (x[iG] == u[iG]) {
						u[iG] -= (u[iG]-l[iG])/10.;
						//std::cout << "Parameter " << params.getName(iG) << " new bound : "<< l[iG] << " :: " << u[iG] << std::endl;
						assert(l[iG] <= u[iG]);
					}
					hasEncouteredAnError = true;
				}
			}

			// If an error as been encountered, restarted with reduce bound
			if(hasEncouteredAnError && cntErr >= 0) {
				task = STOP; // stop
				factr = 1.e5; // Reduce convergence criterion temporarily
				pgtol = 1.e-4; // Reduce convergence criterion temporarily
				setulb(&n, &m, x, l, u, nbd, &f, grad.data(), &factr, &pgtol, ptrWA, ptrIWA, &task,
								&iprint, &csave, lsave, isave, dsave);
				//std::cout << " ----- Restart " << std::endl;

				task = START; // restart
				values = initVal;

				if(bestF > -f) {
					f = -bestF;
					values = bestX;
				}

				cntErr++;
			}

		} else if (task == NEW_X) {
			//std::cout << " ----- " <<  f << std::endl;
			if(-f > bestF) {
				bestF = -f;
				bestX = values;
			}
			// New point reached. Do whatever you want. Add +1 to iteraion nb
			++cntIter;
		} else {
			// Algorithm has ended
			if(cntErr > 0) {
				// We add some boundary issues, we restart from current MLE with extended boundaries.
				// std::cout << "here ?" << std::endl;
				lBound.clear();
				uBound.clear();
				for(size_t i=0; i<params.getNBaseParameters(); ++i) {
					lBound.push_back(params.getBoundMin(i));
					uBound.push_back(params.getBoundMax(i));
				}
				cntErr = -1;
				task = STOP; // stop
				factr = DEFAULT_FACTR;
				pgtol = DEFAULT_PGTOL;
				setulb(&n, &m, x, l, u, nbd, &f, grad.data(), &factr, &pgtol, ptrWA, ptrIWA, &task,
								&iprint, &csave, lsave, isave, dsave);
				task = START; // restart

				if(bestF > -f) {
					f = -bestF;
					values = bestX;
				}

			} else {
				// We add no errors, we end and return the MLE
				hasFinished = true;
			}
		}
	}

	result = task;

	// Signal to the algorithm that we are done.
	task = STOP; // stop
	setulb(&n, &m, x, l, u, nbd, &f, grad.data(), &factr, &pgtol, ptrWA, ptrIWA, &task,
					&iprint, &csave, lsave, isave, dsave);

	return -f;
}

void MaximizerLBFGSB::printResult(const double maxLik, const long int result) const {

	if(IS_CONVERGED(result)) {
		if(result == CONV_GRAD) {
			if(Utils::outputManager().check(Utils::HIGH_VERB) && Parallel::mpiMgr().isMainProcessor()) {
				std::cout << "[Convergence] Optimization stopped because ||p grad|| <=PGTOL was reached." << std::endl;
			}
		} else if (result == CONV_F) {
			if(Utils::outputManager().check(Utils::HIGH_VERB) && Parallel::mpiMgr().isMainProcessor()) {
				std::cout << "[Convergence] Optimization stopped because relative reduction of f(x) <=FACTR*EPSMCH was reached." << std::endl;
			}
		} else {
			if(Utils::outputManager().check(Utils::HIGH_VERB) && Parallel::mpiMgr().isMainProcessor()) {
				std::cout << "[Convergence] Optimization stopped." << std::endl;
			}

		}
	} else {
		if(Utils::outputManager().check(Utils::HIGH_VERB) && Parallel::mpiMgr().isMainProcessor()) {
			std::cout << "[Error] abnormal termination with code = " << result << std::endl;
		}
	}
	//std::cout << "------------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "LogLik = " << std::fixed << maxLik << "  - after " << nFuncCall << " function calls ( ";
	std::cout << (1000.*cp.duration())/(double)nFuncCall << " ms per call)." << std::endl;
	//std::cout << "------------------------------------------------------------------------------------------------" << std::endl;
}

} /* namespace Maximizer */
} /* namespace Utils */
