//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodFunction.h
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef LIKELIHOODFUNCTION_H_
#define LIKELIHOODFUNCTION_H_

#include <stddef.h>

#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Parameter/Parameters.h"
#include "Model/Prior/PriorInterface.h"
#include "ObjectiveFunctionInterface.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { class Parameters; }

namespace Utils {
namespace Maximizer {
namespace Custom {

namespace SM = ::StatisticalModel;
namespace SM_Lik = ::StatisticalModel::Likelihood;


class LikelihoodFunction: public ObjectiveFunctionInterface {
public:
	LikelihoodFunction(const SM::Parameters &aParams, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik);
	~LikelihoodFunction();

	double getLowerBound(size_t idxParameter) const;
	double getUpperBound(size_t idxParameter) const;

private:

	bool first;
	const SM::Parameters &params;
	SM_Lik::LikelihoodInterface::sharedPtr_t ptrLik;

	double doComputeObjectiveValue(const Eigen::VectorXd &values);
	void doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient);
};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* LIKELIHOODFUNCTION_H_ */
