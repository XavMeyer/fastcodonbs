//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TestFunctions.h
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef TESTFUNCTIONS_H_
#define TESTFUNCTIONS_H_

#include <stddef.h>

#include "ObjectiveFunctionInterface.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

class TestFunctions : public ObjectiveFunctionInterface {
public:
	typedef enum {XSQUARE_1D, XSQUARE_ND_INDEP, XSQUARE_ND_CORREL} function_t;

public:
	TestFunctions(const size_t aNParams, const function_t aFunction);
	~TestFunctions();

	double getLowerBound(size_t idxParameter) const;
	double getUpperBound(size_t idxParameter) const;

private:

	const function_t function;

	double doComputeObjectiveValue(const Eigen::VectorXd &values);
	void doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient);
};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* TESTFUNCTIONS_H_ */
