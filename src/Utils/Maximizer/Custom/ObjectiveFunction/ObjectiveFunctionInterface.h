//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ObjectiveFunctionInterface.h
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef OBJECTIVEFUNCTIONINTERFACE_H_
#define OBJECTIVEFUNCTIONINTERFACE_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <vector>

#include "Eigen/Core"
#include "Eigen/Dense"

#define DEBUG 0

namespace Utils {
namespace Maximizer {
namespace Custom {

class ObjectiveFunctionInterface {
public:

	static const double EPSILON_BOUND;
	typedef enum {MAXIMIZE, MINIMIZE} optimize_t;
	typedef boost::shared_ptr< ObjectiveFunctionInterface > sharedPtr_t;

public:
	ObjectiveFunctionInterface(const size_t aNParams, optimize_t aOptiType);
	virtual ~ObjectiveFunctionInterface();

	double computeObjectiveValue(const Eigen::VectorXd &values);
	void computeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient);

	virtual double getLowerBound(size_t idxParameter) const = 0;
	virtual double getUpperBound(size_t idxParameter) const = 0;

	size_t getNParameters() const;
	size_t getNFunctionCall() const;
	optimize_t getOptimizeType() const;

	static ObjectiveFunctionInterface::sharedPtr_t createXSquareFunction();
	static ObjectiveFunctionInterface::sharedPtr_t createXSquareIndepNDFunction(const size_t aNParams);
	static ObjectiveFunctionInterface::sharedPtr_t createXSquareCorrelNDFunction(const size_t aNParams);


protected:

	const size_t N_PARAMETERS;
	optimize_t optiType;
	static const double DELTA_GRADIENT;
	size_t nFuncCall;

	double signum(const double val) const;
	void defaultComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient);

	virtual double doComputeObjectiveValue(const Eigen::VectorXd &values) = 0;
	virtual void doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient) = 0;

};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* OBJECTIVEFUNCTIONINTERFACE_H_ */
