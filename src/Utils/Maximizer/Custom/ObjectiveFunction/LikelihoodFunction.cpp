//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelihoodFunction.cpp
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#include "LikelihoodFunction.h"

#include "Utils/Maximizer/Custom/ObjectiveFunction/ObjectiveFunctionInterface.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Model/Parameter/Parameters.h"
#include "Sampler/Samples/Sample.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

LikelihoodFunction::LikelihoodFunction(const SM::Parameters &aParams, SM::Likelihood::LikelihoodInterface::sharedPtr_t &aPtrLik) :
		ObjectiveFunctionInterface(aParams.getNBaseParameters(), MAXIMIZE), params(aParams), ptrLik(aPtrLik) {
	first = true;
}

LikelihoodFunction::~LikelihoodFunction() {
}

double LikelihoodFunction::doComputeObjectiveValue(const Eigen::VectorXd &values) {

	nFuncCall++;

	std::vector<size_t> pInd;
	Sampler::Sample sample;
	for(size_t i=0; i<(size_t)values.size(); ++i){
		//std::cout << params.getName(i) << " = " << values(i) << "\t";
		pInd.push_back(i);
		sample.getDblValues().push_back(values(i));
	}
	//std::cout << std::endl;

	if(ptrLik->isUpdatable() && !first) {
		return ptrLik->update(pInd, sample);
	} else {
		first = false;
		return ptrLik->processLikelihood(sample);
	}

}

void LikelihoodFunction::doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient) {

	Sampler::Sample sample;
	std::vector<double> tmpDbl;
	for(size_t i=0; i<(size_t)values.size(); ++i){
		tmpDbl.push_back(values(i));
	}
	sample.setDblValues(tmpDbl);

	gradient.resize(values.size());

	for(size_t iP=0; iP<(size_t)values.size(); ++iP) {

		// Constant parameter or wrong bounds
		if(getUpperBound(iP) <= getLowerBound(iP)) {
			gradient(iP) = 0.;
			continue;
		} // Otherwise we compute gradient

		nFuncCall++;

		// Add epsilon to variable iP (from wikipedia)
		double paramValue = sample.getDoubleParameter(iP);
		double perturb = DELTA_GRADIENT*std::max(fabs(paramValue), 1.);
		sample.setDoubleParameter(iP, paramValue+perturb);
		// Get DX
		double dx = paramValue - values(iP);


		// Compute the new objective value
		double f1;
		std::vector<size_t> pIndices(1, iP);
		if(ptrLik->isUpdatable()) {
			f1 = ptrLik->update(pIndices, sample);
		} else {
			f1 = ptrLik->processLikelihood(sample);
		}

		// Compute the gradient
		gradient(iP) = (f1-objective)/dx;


		// Reset the value of variable iP
		sample.setDoubleParameter(iP, paramValue);
	}
}

double LikelihoodFunction::getLowerBound(size_t idxParameter) const {
	return params.getBoundMin(idxParameter);
}

double LikelihoodFunction::getUpperBound(size_t idxParameter) const {
	return params.getBoundMax(idxParameter);
}

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
