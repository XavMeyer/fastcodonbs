//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ObjectiveFunctionInterface.cpp
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#include "ObjectiveFunctionInterface.h"
#include "TestFunctions.h"


namespace Utils {
namespace Maximizer {
namespace Custom {

const double ObjectiveFunctionInterface::EPSILON_BOUND = 1e-7;
const double ObjectiveFunctionInterface::DELTA_GRADIENT = sqrt(std::numeric_limits<double>::epsilon());

ObjectiveFunctionInterface::ObjectiveFunctionInterface(const size_t aNParams, optimize_t aOptiType) :
		N_PARAMETERS(aNParams), optiType(aOptiType) {
	nFuncCall = 0;
}

ObjectiveFunctionInterface::~ObjectiveFunctionInterface() {
}

double ObjectiveFunctionInterface::computeObjectiveValue(const Eigen::VectorXd &values) {
	double obj = doComputeObjectiveValue(values);

	if(optiType == MAXIMIZE) {
		return -obj;
	} else {
		return obj;
	}
}

void ObjectiveFunctionInterface::computeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient) {

	double tmpObjective = objective;
	if(optiType == MAXIMIZE) {
		tmpObjective = -tmpObjective;
	}

	doComputeGradient(tmpObjective, values, gradient);

	if(optiType == MAXIMIZE) {
		gradient *= -1.;
	}
}


size_t ObjectiveFunctionInterface::getNParameters() const {
	return N_PARAMETERS;
}

size_t ObjectiveFunctionInterface::getNFunctionCall() const {
	return nFuncCall;
}

ObjectiveFunctionInterface::optimize_t ObjectiveFunctionInterface::getOptimizeType() const {
	return optiType;
}

ObjectiveFunctionInterface::sharedPtr_t ObjectiveFunctionInterface::createXSquareFunction() {
	return sharedPtr_t(new TestFunctions(1, TestFunctions::XSQUARE_1D));
}

ObjectiveFunctionInterface::sharedPtr_t ObjectiveFunctionInterface::createXSquareIndepNDFunction(const size_t aNParams) {
	return sharedPtr_t(new TestFunctions(aNParams, TestFunctions::XSQUARE_ND_INDEP));
}

ObjectiveFunctionInterface::sharedPtr_t ObjectiveFunctionInterface::createXSquareCorrelNDFunction(const size_t aNParams) {
	return sharedPtr_t(new TestFunctions(aNParams, TestFunctions::XSQUARE_ND_CORREL));
}

double ObjectiveFunctionInterface::signum(const double val) const {
	return val >= 0. ? +1.0 : -1.0;
}

void ObjectiveFunctionInterface::defaultComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient) {

	Eigen::VectorXd tmpVals = values;
	gradient.resize(values.size());

	for(size_t iP=0; iP<(size_t)values.size(); ++iP) {

		// Constant parameter or wrong bounds
		if(getUpperBound(iP) <= getLowerBound(iP)) {
			gradient(iP) = 0.;
			continue;
		} // Otherwise we compute gradient

		// Add epsilon to variable iP (from wikipedia)
		tmpVals(iP) += DELTA_GRADIENT*std::max(fabs(tmpVals(iP)), 1.);
		// Get DX
		double dx = tmpVals(iP) - values(iP);
		// Compute the new objective value
		double f1 = doComputeObjectiveValue(tmpVals);
		// Compute the gradient
		gradient(iP) = (f1-objective)/dx;

		// Reset the value of variable iP
		tmpVals(iP) = values(iP);

		nFuncCall++;
	}
}

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
