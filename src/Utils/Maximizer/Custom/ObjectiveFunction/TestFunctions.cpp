//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TestFunctions.cpp
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#include "TestFunctions.h"

#include "Utils/Maximizer/Custom/ObjectiveFunction/ObjectiveFunctionInterface.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

TestFunctions::TestFunctions(const size_t aNParams, const function_t aFunction) :
		ObjectiveFunctionInterface(aNParams, MINIMIZE), function(aFunction) {
}

TestFunctions::~TestFunctions() {
}

double TestFunctions::doComputeObjectiveValue(const Eigen::VectorXd &values) {
	nFuncCall++;
	if(function == XSQUARE_1D) {
		return values(0)*values(0);
	} else if (function == XSQUARE_ND_INDEP) {
		return values.dot(values);
	} else if (function == XSQUARE_ND_CORREL) {
		double logRes = 0;
		for(size_t iV=0; iV<(size_t)values.size(); ++iV) {
			logRes += log(100.+values(iV)*values(iV));
		}
		return logRes;

		//return -values.cwiseProduct(values).prod();
	}

	return 0.;
}

void TestFunctions::doComputeGradient(const double objective, const Eigen::VectorXd &values, Eigen::VectorXd &gradient) {
	defaultComputeGradient(objective, values, gradient);
}

double TestFunctions::getLowerBound(size_t idxParameter) const {
	return -std::numeric_limits<double>::max();
}

double TestFunctions::getUpperBound(size_t idxParameter) const {
	return std::numeric_limits<double>::max();
}



} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
