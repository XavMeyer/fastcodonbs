//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LineSearchInterface.cpp
 *
 * @date Jul 6, 2015
 * @author meyerx
 * @brief
 */
#include "LineSearchInterface.h"

#include <stddef.h>

#include "Utils/Maximizer/Custom/LineSearch/../ObjectiveFunction/ObjectiveFunctionInterface.h"
#include "WolfeRule.h"
#include "cmath"

namespace Utils {
namespace Maximizer {
namespace Custom {

const double LineSearchInterface::EPSILON_ALPHA = 1e-3;

LineSearchInterface::LineSearchInterface(ObjectiveFunctionInterface::sharedPtr_t aObjFunction) :
	objFunction(aObjFunction) {
}

LineSearchInterface::~LineSearchInterface() {
}

LineSearchInterface::sharedPtr_t LineSearchInterface::createWolfeRule(ObjectiveFunctionInterface::sharedPtr_t aObjFunction) {
	return LineSearchInterface::sharedPtr_t(new WolfeRule(aObjFunction));
}


double LineSearchInterface::bissection(const Point &p0, const Point &p1) const {
	return (p0.alpha+p1.alpha)/2.;
}

double LineSearchInterface::quadraticInterpolation(const Point &initP, const Point &p) const {
	//std::cout << "Quadratic" << std::endl;
	double newAlpha = (-initP.slope*p.alpha*p.alpha)/(2.*(p.objVal-initP.objVal-initP.slope*p.alpha));
	return newAlpha;
}

double LineSearchInterface::cubicInterpolation(const Point &initP, const Point &p0, const Point &p1) const {
	//std::cout << "Cubic" << std::endl;
	const double a0Sq = pow(p0.alpha,2);
	const double a1Sq = pow(p1.alpha,2);

	const double scalar = 1./(a0Sq*a1Sq*(p1.alpha-p0.alpha));

	Eigen::Matrix2d mat1;
	mat1 << a0Sq, -a1Sq, -a0Sq*p0.alpha, a1Sq*p1.alpha;

	Eigen::Vector2d vec1, vec2;
	vec1 << p1.objVal - initP.objVal - initP.slope*p1.alpha, p0.objVal - initP.objVal - initP.slope*p0.alpha;

	vec2 = scalar * mat1 * vec1;
	double a = vec2(0);
	double b = vec2(1);

	double newAlpha = (-b+sqrt(b*b-3*a*initP.slope))/(3*a);

	return newAlpha;
}

double LineSearchInterface::cubicInterpolation(const Point &p0, const Point &p1) const {
	//std::cout << "Cubic" << std::endl;
	const double d1 = p0.slope + p1.slope - 3.*(p0.objVal-p1.objVal)/(p0.alpha-p1.alpha);
	const double signum = ((p1.alpha-p0.alpha) >= 0.) ? 1.0 : -1.0;
	const double d2 = signum * sqrt(pow(d1,2)-p0.slope*p1.slope);

	double newAlpha = p1.alpha - (p1.alpha-p0.alpha)*(p1.slope+d2-d1)/(p1.slope-p0.slope+2*d2);

	return newAlpha;
}

double LineSearchInterface::checkAlphaDecrease(const double alpha0, const double alpha1) const {
	//std::cout << "Chek alpha decrease : " << alpha0 << " vs " << alpha1;
	if(fabs(1.-(alpha1/alpha0)) < EPSILON_ALPHA) { // If a1 is nearly the same as a0
		//std::cout << "\t nearly same : " << fabs(1.-(alpha1/alpha0)) << "\t reset : " << alpha0/2. << std::endl;
		return alpha0/2.; // we reset
	} else if ((alpha1/alpha0) < EPSILON_ALPHA) { // If a1 is too small compared to a0
		//std::cout << "\t too far : " << (alpha1/alpha0) << "\t reset : " << alpha0/2. << std::endl;
		return alpha0/2.; // we reset
	} else if ((alpha1) < 0.) { // If a1 is too small compared to a0
		std::cout << "Negative alpha : " << alpha1 << "\t reset : " << alpha0/2. << std::endl;
		return alpha0/2.; // we reset
	}

	//std::cout << "\t unchanged " << std::endl;

	return alpha1;
}

void LineSearchInterface::projectValuesIntoBound(Eigen::VectorXd &values) const {
	for(size_t iV=0; iV<(size_t)values.size(); ++iV) {
		values(iV) = std::max(values(iV), objFunction->getLowerBound(iV));
		values(iV) = std::min(values(iV), objFunction->getUpperBound(iV));
	}
}



} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
