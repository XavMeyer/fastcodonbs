//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LineSearchInterface.h
 *
 * @date Jul 6, 2015
 * @author meyerx
 * @brief
 */
#ifndef LINESEARCHINTERFACE_H_
#define LINESEARCHINTERFACE_H_

#include <boost/shared_ptr.hpp>
#include <iostream>

#include "../ObjectiveFunction/ObjectiveFunctionInterface.h"
#include "Eigen/Core"
#include "Eigen/Dense"

namespace Utils {
namespace Maximizer {
namespace Custom {

class LineSearchInterface {
public:
	typedef boost::shared_ptr< LineSearchInterface > sharedPtr_t;
public:
	LineSearchInterface(ObjectiveFunctionInterface::sharedPtr_t aObjFunction);
	virtual ~LineSearchInterface();

	virtual bool search(double &alpha, double &objective, Eigen::VectorXd &values, Eigen::VectorXd &gradient, const Eigen::VectorXd &direction) = 0;

	static LineSearchInterface::sharedPtr_t createWolfeRule(ObjectiveFunctionInterface::sharedPtr_t aObjFunction);

protected:
	static const double EPSILON_ALPHA;

	ObjectiveFunctionInterface::sharedPtr_t objFunction;

	struct Point {
		double alpha, objVal, slope;
		Eigen::VectorXd values, gradient;

		Point(double aAlpha, double aObjVal=0., double aSlope=0.) : alpha(aAlpha), objVal(aObjVal), slope(aSlope) {};
		Point(double aAlpha, double aObjVal, double aSlope, Eigen::VectorXd &aValues, Eigen::VectorXd &aGradient) :
		alpha(aAlpha), objVal(aObjVal), slope(aSlope), values(aValues), gradient(aGradient) {};
		~Point(){}
	};


	double bissection(const Point &p0, const Point &p1) const;

	double quadraticInterpolation(const Point &initP, const Point &p) const;

	double cubicInterpolation(const Point &initP, const Point &p0, const Point &p1) const;

	double cubicInterpolation(const Point &p0, const Point &p1) const;

	double checkAlphaDecrease(const double alpha0, const double alpha1) const;

	void projectValuesIntoBound(Eigen::VectorXd &values) const;

};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* LINESEARCHINTERFACE_H_ */
