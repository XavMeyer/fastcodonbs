//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WolfeRule.h
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef WOLFERULE_H_
#define WOLFERULE_H_

#include <stddef.h>

#include "Utils/Maximizer/Custom/LineSearch/../ObjectiveFunction/ObjectiveFunctionInterface.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "LineSearchInterface.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

class WolfeRule: public LineSearchInterface {
public:
	WolfeRule(ObjectiveFunctionInterface::sharedPtr_t aObjFunction);
	~WolfeRule();

	bool search(double &alpha, double &objective, Eigen::VectorXd &values, Eigen::VectorXd &gradient, const Eigen::VectorXd &direction);

private:
	static const size_t MAX_STEP;
	static const double C1, C2, ALPHA_MAX;

	bool zoom(Point &curP, Point &lowP, Point &highP, const Point &initP, const Eigen::VectorXd &direction) const;
	void copyFoundPoint(const Point &point, double &alpha, double &objective, Eigen::VectorXd &values, Eigen::VectorXd &gradient) const;

};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* WOLFERULE_H_ */
