//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file WolfeRule.cpp
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief Implementation of Wolfe Rule following Numerical Optimization by Nocedal an Wright
 */
#include "WolfeRule.h"

#include "Utils/Maximizer/Custom/LineSearch/LineSearchInterface.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

const size_t WolfeRule::MAX_STEP = 20;
const double WolfeRule::C1 = 0.3; // Advised value : 1e-4
const double WolfeRule::C2 = 0.9; // Advised value : 0.9
const double WolfeRule::ALPHA_MAX = 50.;

WolfeRule::WolfeRule(ObjectiveFunctionInterface::sharedPtr_t aObjFunction) :
	LineSearchInterface(aObjFunction) {

}

WolfeRule::~WolfeRule() {
}

bool WolfeRule::search(double &alpha, double &objective, Eigen::VectorXd &values, Eigen::VectorXd &gradient, const Eigen::VectorXd &direction) {

	bool first = true;

	Point initP(0., objective, direction.dot(gradient), values, gradient);
	Point oldP = initP;
	Point curP(alpha);

#if DEBUG > 1
	std::cout << "Val = " << values(4) << "\tGrad = " << gradient(0)  << "\tSlope = " << initP.slope << "\t Obj = " << objective << std::endl;
#endif

	// Try at most MAX_STEP
	for (size_t iT = 0; iT < MAX_STEP; ++iT) {

#if  DEBUG > 1
		std::cout << "[" << iT << "]" << std::endl;
#endif

		// Evaluate theta(alpha)
		curP.values = initP.values + curP.alpha * direction;
		projectValuesIntoBound(curP.values);
		double tmpObjVal = objFunction->computeObjectiveValue(curP.values);
		if(tmpObjVal != tmpObjVal) {
			//std::cout << " curP.alpha = " << curP.alpha << "\t tmp OV = " << tmpObjVal << std::endl;
			curP.alpha = curP.alpha/2.;
			continue;
		}
		curP.objVal = tmpObjVal;

#if  DEBUG > 1
		std::cout << "Val = " << curP.values(4) << "\t Obj = " << curP.objVal << std::endl;
#endif

		// Check 1) Sufficient increase ==> if not we zoom
		if((curP.objVal > (initP.objVal + C1 * curP.alpha * initP.slope)) ||
		   (iT > 0 && curP.objVal >= oldP.objVal)) {
			double tmpAlpha = (first) ?
					quadraticInterpolation(initP, curP) : cubicInterpolation(initP, oldP, curP);
			Point newP(tmpAlpha);
			newP.alpha = checkAlphaDecrease(curP.alpha, newP.alpha);
#if  DEBUG > 1
			std::cout << "Insufficient increase : new Alpha = " << newP.alpha << std::endl;
#endif
			bool found = zoom(newP, oldP, curP, initP, direction);
			if(found) copyFoundPoint(newP, alpha, objective, values, gradient);
			return found;
		}

		// Evaluate theta'(alpha)
		curP.gradient.resize(direction.size());
		objFunction->computeGradient(curP.objVal, curP.values, curP.gradient);
		curP.slope = direction.dot(curP.gradient);

#if  DEBUG > 1
		std::cout << "Val = " << ", " << curP.values(0) << "\tGrad = " << curP.gradient(0)  << "\tSlope = " << curP.slope << "\tObj = " << curP.objVal << std::endl;
#endif

		// Check curvature condition
		if(fabs(curP.slope) <=  - C2 * initP.slope) { // it hold
#if  DEBUG > 1
			std::cout << "All good" << std::endl;
#endif
			copyFoundPoint(curP, alpha, objective, values, gradient);
			return true;
		}

		if(curP.slope >= 0) {
#if  DEBUG > 1
			std::cout << "Slope greater than 0" << std::endl;
#endif
			Point newP(cubicInterpolation(oldP, curP));
			newP.alpha = checkAlphaDecrease(curP.alpha, newP.alpha);
			bool found = zoom(newP, curP, oldP, initP, direction);
			if(found) copyFoundPoint(newP, alpha, objective, values, gradient);
			return found;
		}

#if  DEBUG > 1
		std::cout << "Increase alpha" << std::endl;
#endif

		// Propose new alpha
		alpha = bissection(curP.alpha, ALPHA_MAX);

		// Keep track of old point and init new alpha
		oldP = curP;
		curP.alpha = alpha;
		first = false;
	}

	return false;
}



bool WolfeRule::zoom(Point &curP, Point &lowP, Point &highP, const Point &initP, const Eigen::VectorXd &direction) const {

	bool first = true;

	//std::cout << "InitP objVal : " << initP.objVal  << std::endl;

	//for (size_t iT = 0; iT < MAX_STEP; ++iT) {
	while((fabs(highP.alpha-lowP.alpha)/std::max(highP.alpha,lowP.alpha)) > 5e-2) {

		if(!first) { // Define new alpha BUT not for the first phase, its already done
			//double oldAlpha = curP.alpha;
			curP.alpha = cubicInterpolation(lowP, highP);
			if(curP.alpha < 0.) {
				curP.alpha = std::max(lowP.alpha, highP.alpha)/2.;
			}
			//curP.alpha = checkAlphaDecrease(oldAlpha, curP.alpha);
		}

		// Evaluate theta(alpha)
		curP.values = initP.values + curP.alpha * direction;
		projectValuesIntoBound(curP.values);
		double tmpObjVal = objFunction->computeObjectiveValue(curP.values);
		if(tmpObjVal != tmpObjVal) {
			//std::cout << " curP.alpha = " << curP.alpha << "\t tmp OV = " << tmpObjVal << std::endl;
			curP.alpha = curP.alpha/2.;
			continue;
		}
		curP.objVal = tmpObjVal;

		//std::cout << "LowP alpha : " << lowP.alpha << " -- HighP alpha : " << highP.alpha << " -- Proposed Alpha : " << curP.alpha << std::endl;
		//std::cout << "Cur Obj Val : " << curP.objVal << " --  initP objVal : " << initP.objVal  << std::endl;
		//std::cout << lowP.objVal << "\t::\t" << initP.objVal + C1 * curP.alpha * initP.slope << std::endl;

#if  DEBUG > 1
		std::cout << "Alpha = " << curP.alpha << "\tVal = " << curP.values(4) << "\t Obj = " << curP.objVal << std::endl;
#endif

		// Check 1) Sufficient increase
		if((curP.objVal > (initP.objVal + C1 * curP.alpha * initP.slope))
		 || (curP.objVal > lowP.objVal)) {
			//std::cout << "Increase insufficient." << std::endl;
			highP = curP; // Point not found we reduce the interval
		} else {
			// Evaluate theta'(alpha)
			curP.gradient.resize(direction.size());
			objFunction->computeGradient(curP.objVal, curP.values, curP.gradient);
			curP.slope = direction.dot(curP.gradient);

#if  DEBUG > 1
			std::cout << "Val = " << ", " << curP.values(4) << "\tGrad = " << curP.gradient(4)  << "\tSlope = " << curP.slope << "\tObj = " << curP.objVal << std::endl;
#endif

			//std::cout << fabs(curP.slope) << " <= " << - C2 * initP.slope << std::endl;

			// Check curvature condition
			if(fabs(curP.slope) <= - C2 * initP.slope) { // it hold
				//std::cout << "Curvature ok" << std::endl;
				return true; // Point found
			}

			if(curP.slope*(highP.alpha-lowP.alpha) >= 0) {
				//std::cout << "Slope vs alpha increase NOK, invert." << std::endl;
				highP = lowP; // Point not found we reduce the interval
			}

			lowP = curP;
		}
		first = false;
		//std::cout << "LowP alpha : " << lowP.alpha << " -- HighP alpha : " << highP.alpha << " -- Score : " << (fabs(highP.alpha-lowP.alpha)/std::max(highP.alpha,lowP.alpha)) << std::endl;
	}

	if(lowP.alpha == 0.) {
		return false;
	} else {
		curP = lowP;
		return true;
	}
}

void WolfeRule::copyFoundPoint(const Point &point, double &alpha, double &objective, Eigen::VectorXd &values, Eigen::VectorXd &gradient) const {
	alpha = point.alpha;
	objective = point.objVal;
	values = point.values;
	gradient = point.gradient;
}

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
