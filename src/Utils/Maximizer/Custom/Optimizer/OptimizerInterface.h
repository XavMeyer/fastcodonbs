//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimizerInterface.h
 *
 * @date Jul 6, 2015
 * @author meyerx
 * @brief
 */
#ifndef OPTIMIZERINTERFACE_H_
#define OPTIMIZERINTERFACE_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>
#include <set>
#include <vector>

#include "../EndPredicate/EndPredicate.h"
#include "../ObjectiveFunction/ObjectiveFunctionInterface.h"
#include <Eigen/Core>
#include <Eigen/Dense>
#include "Utils/Code/CustomProfiling.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

class OptimizerInterface {
public:
	typedef boost::shared_ptr< OptimizerInterface > sharedPtr_t;
	static const double MAX_EPSILON_BOUND;
	typedef enum {SUCCESS, CONVERGENCE_PREDICATE, LINE_SEARCH_FAIL, FAIL, NONE} status_t;

public:
	OptimizerInterface(const size_t aSeed, ObjectiveFunctionInterface::sharedPtr_t aObjFunction, EndPredicate::sharedPtr_t aEndPredicate, const size_t aVLevel=1);
	virtual ~OptimizerInterface();

	void setSeed(const size_t aSeed);
	size_t getSeed() const;
	double getExecTime() const;

	double optimize(std::vector<double> &aValues);

protected:

	size_t seed, vLevel;
	status_t status;
	std::vector<double> EPSILON_BOUND;
	ObjectiveFunctionInterface::sharedPtr_t objFunction;
	EndPredicate::sharedPtr_t endPred;
	CustomProfiling cp;

	virtual double doOptimize(std::vector<double> &aValues) = 0;

	typedef std::set<size_t> idSet_t;
	typedef idSet_t::iterator itIdSet_t;
	typedef idSet_t::const_iterator cstItIdSet_t;

	void defineEpsilonBound(const size_t N);
	void defineActiveVariables(const Eigen::VectorXd &values, const Eigen::VectorXd &gradients,
							   std::set<size_t> &setA1, std::set<size_t> &setA2,
							   std::set<size_t> &setA3, std::set<size_t> &setB) const;
	void createSetVector(const size_t N, const idSet_t &set, Eigen::VectorXd &mat) const;

	void defineLambda(const idSet_t &setA3, const Eigen::VectorXd &values,
					  const Eigen::VectorXd &gradients, Eigen::VectorXd &lambda) const;

	void defineDirection(const Eigen::VectorXd &values, const Eigen::VectorXd &gradients,
						 const Eigen::MatrixXd &B,  Eigen::VectorXd &direction) const;


private:
	void printResult(const double max) const;



};

} /* namespace Custom */
} /* namespace Optimizer */
} /* namespace Utils */

#endif /* OPTIMIZERINTERFACE_H_ */
