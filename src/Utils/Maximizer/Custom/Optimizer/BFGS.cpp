//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BFGS.cpp
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief Implementation of BFGS following Numerical Optimization by Nocedal an Wright
 *
 * Implementation of BFGS following Numerical Optimization by Nocedal an Wright.
 * In order to take into account of the potential bounded variables, the direction is
 * computed as described by Ni and Yuan 1997 :
 * "A Subspace Limited memory Quasi-Newton Algorithm for Large-SCale Nonlinear Bound Constrained Optimization"
 *
 */
#include "BFGS.h"

#include "Utils/Maximizer/Custom/Optimizer/../ObjectiveFunction/ObjectiveFunctionInterface.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

BFGS::BFGS(const size_t aSeed, ObjectiveFunctionInterface::sharedPtr_t aObjFunction, EndPredicate::sharedPtr_t aEndPredicate, const size_t aVLevel) :
	OptimizerInterface(aSeed, aObjFunction, aEndPredicate, aVLevel), lineSearch(LineSearchInterface::createWolfeRule(objFunction)) {

}

BFGS::BFGS(const size_t aSeed, LineSearchInterface::sharedPtr_t aLineSearch,
     ObjectiveFunctionInterface::sharedPtr_t aObjFunction, EndPredicate::sharedPtr_t aEndPredicate, const size_t aVLevel) :
		OptimizerInterface(aSeed, aObjFunction, aEndPredicate, aVLevel), lineSearch(aLineSearch) {
}


BFGS::~BFGS() {
}

double BFGS::doOptimize(std::vector<double> &aValues) {
	size_t iT = 0;
	double alpha = 1.;
	const size_t N = aValues.size();

	// Initialize Eigen Vector and Matrices
	// approximation of inverse Hessian matrix (identity)
	Eigen::MatrixXd B = Eigen::MatrixXd::Identity(N,N);
	// Vectors
	Eigen::VectorXd values(N), gradient(N), direction(N), oldValues(N), oldGradient(N), s(N), y(N);
	//values.resize(N);
	for(size_t i=0; i<N; ++i) {
		values(i) = aValues[i];
	}

	// Compute objective value
	double objVal = objFunction->computeObjectiveValue(values);

	// Compute gradient
	objFunction->computeGradient(objVal, values, gradient);

	// while we haven't reached convergence
	while(!endPred->hasConverged(iT, objVal, values)) {

		// Compute direction
		//direction = -B * gradient;
		defineDirection(values, gradient, B, direction);

#if DEBUG > 0
		std::cout << iT << " > Obj val : " << objVal << "\tvalues : " << values(0) << "\tGradient : " << gradient(0) << "\tdirection : " << direction(0) << std::endl;
#endif

		// Move along the direction, upon success alpha, objVal, mapValues and gradient have their new values
		oldValues = values; // Keep track of vales and gradient
		oldGradient = gradient;
		alpha = 1.0;
		if(!lineSearch->search(alpha, objVal, values, gradient, direction)) { // if line search fail, we stops
			status = LINE_SEARCH_FAIL;
			// Copy values
			for(size_t i=0; i<N; ++i) {
				aValues[i] = values(i);
			}
			return objVal;
		}

		// Get s and y
		s = alpha * direction;
		y = gradient - oldGradient;

		// Pre compute some scalar
		const double YS = y.dot(s);
		if(iT == 0) {
			B = (YS / y.dot(y)) *  Eigen::MatrixXd::Identity(N, N);
		}
		// (s^t * y + y^t * B * y ) * (s*s^t) OVER (s^t * y)^2
		Eigen::MatrixXd term1 = ((YS + (y.transpose()*B * y)) * (s*s.transpose())) / (YS*YS);
		// ( B*y*s^t + s*y^t*B ) / (s^t * y)
		Eigen::MatrixXd term2 = ((B*y)*s.transpose() + s*(y.transpose() * B)) / YS;
		// Update Hessian inverse
		B = B + term1 - term2;

		iT++;
	}

	status = CONVERGENCE_PREDICATE;

	// Copy values
	for(size_t i=0; i<N; ++i) {
		aValues[i] = values(i);
	}

	return objVal;
}

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
