//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file OptimizerInterface.cpp
 *
 * @date Jul 6, 2015
 * @author meyerx
 * @brief
 */
#include "OptimizerInterface.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep

namespace Utils {
namespace Maximizer {
namespace Custom {

const double OptimizerInterface::MAX_EPSILON_BOUND = 10.;

OptimizerInterface::OptimizerInterface(const size_t aSeed, ObjectiveFunctionInterface::sharedPtr_t aObjFunction,
									   EndPredicate::sharedPtr_t aEndPredicate, const size_t aVLevel) :
		seed(aSeed), vLevel(aVLevel), status(NONE), objFunction(aObjFunction), endPred(aEndPredicate)
{
	Parallel::mpiMgr().setSeedPRNG(seed);
	//EPSILON_BOUND = 0.;
}

OptimizerInterface::~OptimizerInterface() {
}

void OptimizerInterface::setSeed(const size_t aSeed) {
	seed = aSeed;
	Parallel::mpiMgr().setSeedPRNG(seed);
}

size_t OptimizerInterface::getSeed() const {
	return seed;
}

double OptimizerInterface::getExecTime() const {
	return cp.duration();
}

double OptimizerInterface::optimize(std::vector<double> &aValues){

	cp.startTime();
	defineEpsilonBound(aValues.size());
	double maxLik = doOptimize(aValues);
	cp.endTime();

	if(objFunction->getOptimizeType() == ObjectiveFunctionInterface::MAXIMIZE) {
		maxLik *= -1.;
	}

	if(vLevel >= 1) {
		printResult(maxLik); // TODO ADD VERBOSE
	}

	return maxLik;
}

void OptimizerInterface::defineEpsilonBound(const size_t N) {

	EPSILON_BOUND.resize(N, MAX_EPSILON_BOUND);

	for(size_t iV=0; iV<N; ++iV) {
		//double newEps = (1.e-3)*std::min(fabs(objFunction->getUpperBound(iV)), fabs(objFunction->getLowerBound(iV)));
		double range = objFunction->getUpperBound(iV) - objFunction->getLowerBound(iV);
		double newEps = range/50.;
		/*if(newEps > 5000.*absMin) {
			newEps = 5000.*absMin;
		}*/

		//newEps = 0.;
		EPSILON_BOUND[iV] = std::min(EPSILON_BOUND[iV], newEps);
		//std::cout << "[" << iV << "] = " << EPSILON_BOUND[iV] << "\t";
	}
	//std::cout << std::endl;
	//epsilon = 0.;
}


void OptimizerInterface::defineActiveVariables(const Eigen::VectorXd &values, const Eigen::VectorXd &gradients,
											   idSet_t &setA1, idSet_t &setA2,
											   idSet_t &setA3, idSet_t &setB) const {

	for(size_t iV=0; iV<(size_t)values.size(); ++iV) {
		const double val = values(iV);
		const double grd = gradients(iV);
		const double loB = objFunction->getLowerBound(iV);
		const double upB = objFunction->getUpperBound(iV);

		bool isAtLowB = val == loB;
		bool isAtUpB = val == upB;
		double SCALE_EPSILON = std::min(1.0, fabs(gradients(iV))/250.0);
		SCALE_EPSILON = SCALE_EPSILON > 1.e-1 ? SCALE_EPSILON : 0.;
		//std::cout << "[" << iV << "] = " << EPSILON_BOUND[iV] * SCALE_EPSILON << "\t";
		bool isCloseToLowB = (values(iV)-loB) <= (EPSILON_BOUND[iV] * SCALE_EPSILON);
		bool isCloseToUpB = (upB-values(iV)) <= (EPSILON_BOUND[iV]  * SCALE_EPSILON);

		if(!(isCloseToLowB || isCloseToUpB)) { // Inactive variable
			setB.insert(iV); // B : Is inside the feasible region
		} else { // Active variable

			// A1 : Variable moving outside the feasible region
			bool isMovingTowardBound = (loB+upB-2*val)*grd >= 0;
			if(( isAtLowB || isAtUpB ) && isMovingTowardBound) {
				//std::cout << "Set A1 : " << iV << std::endl;
				setA1.insert(iV);
				continue;
			}

			// A2 : Variable moving inside the feasible region
			if(!isMovingTowardBound) {
				setA2.insert(iV);
				continue;
			}

			// A3 : Moving toward bounds
			if(isMovingTowardBound) {
				//std::cout << "Set A3 : " << iV << std::endl;
				setA3.insert(iV);
				continue;
			}
		}
	}
	//std::cout << std::endl;
}


void OptimizerInterface::createSetVector(const size_t N, const idSet_t &set, Eigen::VectorXd &vec) const {

	vec = Eigen::VectorXd::Zero(N);
	for(cstItIdSet_t it = set.begin(); it != set.end(); it++) {
		size_t id = *it;
		vec(id) = 1.0;
	}
}

void OptimizerInterface::defineLambda(const idSet_t &setA3, const Eigen::VectorXd &values,
									  const Eigen::VectorXd &gradients, Eigen::VectorXd &lambda) const {

	lambda = Eigen::VectorXd::Zero(values.size());

	//std::cout << "Lambda :\t";
	for(size_t iV=0; iV<(size_t)values.size(); ++iV) {
		if(setA3.find(iV) == setA3.end()) continue; // If the value is not in set A3

		const double val = values(iV);
		const double grd = gradients(iV);
		const double loB = objFunction->getLowerBound(iV);
		const double upB = objFunction->getUpperBound(iV);

		double SCALE_EPSILON = std::min(1.0, fabs(gradients(iV))/250.0);
		SCALE_EPSILON = SCALE_EPSILON > 1.e-1 ? SCALE_EPSILON : 0.;
		bool isCloseToLowB = (values(iV)-loB) <= (EPSILON_BOUND[iV] * SCALE_EPSILON);
		bool isCloseToUpB = (upB-values(iV)) <= (EPSILON_BOUND[iV] * SCALE_EPSILON);

		if(isCloseToLowB && (val-grd <= loB)) {
			lambda(iV) = (val - loB) / grd;//std::max(5.e-3, (val - loB) / grd);
		} else if(isCloseToUpB && (val-grd >= upB)) {
			lambda(iV) = (val - upB) / grd;//std::max(5.e-3, (val - upB) / grd);
		} else {
			lambda(iV) = 1.0;
		}
		//std::cout << "[" << iV << "] = " << lambda[iV] << "\t";
	}
	//std::cout << std::endl;

}


void OptimizerInterface::defineDirection(const Eigen::VectorXd &values, const Eigen::VectorXd &gradients,
					 	 	 	 	     const Eigen::MatrixXd &B,  Eigen::VectorXd &direction) const {

	const size_t N = values.size();

	// Define active variables
	idSet_t setA1, setA2, setA3, setB;
	defineActiveVariables(values, gradients, setA1, setA2, setA3, setB);

	//std::cout << setA1.size() << "\t" << setA2.size() << "\t" << setA3.size() << "\t" << setB.size() << std::endl;

	// Compute direction terms
	Eigen::MatrixXd sumTerm = Eigen::MatrixXd::Zero(N, N);
	//std::cout << "setB" << std::endl;
	if(setB.size() > 0) {
		Eigen::VectorXd vecB; // Compute matrix PB
		createSetVector(values.size(), setB, vecB);
		//std::cout << PB.rows() << "\t" << PB.cols() << std::endl;
		//std::cout << B.rows() << "\t" << B.cols() << std::endl;
		sumTerm += vecB.asDiagonal()*B*vecB.asDiagonal(); // Update sum term
	}

	//std::cout << "setA2" << std::endl;
	if(setA2.size() > 0) {
		Eigen::VectorXd vecPA2; // Compute matrix PA2
		createSetVector(values.size(), setA2, vecPA2);
		sumTerm += vecPA2.asDiagonal()*B*vecPA2.asDiagonal();
	}

	//std::cout << "setA3" << std::endl;
	if(setA3.size() > 0) {
		Eigen::VectorXd vecPA3, lambda; // Compute matrix PA3
		createSetVector(values.size(), setA3, vecPA3);
		defineLambda(setA3, values, gradients, lambda);
		sumTerm += vecPA3.cwiseProduct(lambda).asDiagonal(); // Update sum term
	}
	direction = -sumTerm*gradients;
}


void OptimizerInterface::printResult(const double maxLik) const {
	//std::cout << "------------------------------------------------------------------------------------------------" << std::endl;
	if(status == CONVERGENCE_PREDICATE) {
		std::cout << "Convergence predicate reached" << std::endl;
	}
	if(status == LINE_SEARCH_FAIL) {
		std::cout << "Line search couldn't improve values in 10 steps" << std::endl;
	}
	std::cout << "LogLik value : " << maxLik << "  - after " << objFunction->getNFunctionCall() << " function calls ( ";
	std::cout << (1000.*cp.duration())/(double)objFunction->getNFunctionCall() << " ms per call)." << std::endl;
	//std::cout << "------------------------------------------------------------------------------------------------" << std::endl;

}

} /* namespace Custom */
} /* namespace Optimizer */
} /* namespace Utils */
