//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BFGS.h
 *
 * @date Jul 7, 2015
 * @author meyerx
 * @brief
 */
#ifndef BFGS_H_
#define BFGS_H_

#include <stddef.h>

#include "../LineSearch/WolfeRule.h"
#include "Utils/Maximizer/Custom/Optimizer/../EndPredicate/EndPredicate.h"
#include "Utils/Maximizer/Custom/Optimizer/../LineSearch/LineSearchInterface.h"
#include "OptimizerInterface.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

class BFGS: public OptimizerInterface {
public:
	BFGS(const size_t aSeed, ObjectiveFunctionInterface::sharedPtr_t aObjFunction, EndPredicate::sharedPtr_t aEndPredicate, const size_t aVLevel=1);
	BFGS(const size_t aSeed, LineSearchInterface::sharedPtr_t aLineSearch,
	     ObjectiveFunctionInterface::sharedPtr_t aObjFunction, EndPredicate::sharedPtr_t aEndPredicate, const size_t aVLevel=1);
	~BFGS();

private:

	LineSearchInterface::sharedPtr_t lineSearch;
	double doOptimize(std::vector<double> &aValues);

};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* BFGS_H_ */
