//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AbsoluteValuesPred.h
 *
 * @date Jul 8, 2015
 * @author meyerx
 * @brief
 */
#ifndef ABSOLUTEVALUESPRED_H_
#define ABSOLUTEVALUESPRED_H_

#include <Utils/Maximizer/Custom/EndPredicate/EndPredicate.h>
#include <stddef.h>

namespace Utils {
namespace Maximizer {
namespace Custom {

class AbsoluteValuesPred: public EndPredicate {
public:
	AbsoluteValuesPred(const double aTHRESHOLD);
	 ~AbsoluteValuesPred();

	bool hasConverged(size_t iT, double likelihood, Eigen::VectorXd &values);

private:
	const double THRESHOLD;
	Eigen::VectorXd prevValues;

};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* ABSOLUTEVALUESPRED_H_ */
