//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EndPredicate.h
 *
 * @date Jul 6, 2015
 * @author meyerx
 * @brief
 */
#ifndef ENDPREDICATE_H_
#define ENDPREDICATE_H_

#include <boost/shared_ptr.hpp>
#include <stddef.h>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace Utils {
namespace Maximizer {
namespace Custom {

class EndPredicate {
public:
	typedef boost::shared_ptr< EndPredicate > sharedPtr_t;
public:
	EndPredicate();
	EndPredicate(const size_t aMAX_ITERATION);
	virtual ~EndPredicate();

	virtual bool hasConverged(size_t iT, double likelihood, Eigen::VectorXd &values) = 0;

	static sharedPtr_t createRelativeValues(const double tolerance);
	static sharedPtr_t createAbsoluteValues(const double tolerance);

protected:

	const size_t MAX_ITERATION;

private:

	static const size_t DEFAULT_MAX_ITERATION;

};

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */

#endif /* ENDPREDICATE_H_ */
