//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EndPredicate.cpp
 *
 * @date Jul 6, 2015
 * @author meyerx
 * @brief
 */
#include "EndPredicate.h"
#include "RelativeValuesPred.h"
#include "AbsoluteValuesPred.h"

namespace Utils {
namespace Maximizer {
namespace Custom {

const size_t EndPredicate::DEFAULT_MAX_ITERATION = 1000000;

EndPredicate::EndPredicate() : MAX_ITERATION(DEFAULT_MAX_ITERATION) {
}

EndPredicate::EndPredicate(const size_t aMAX_ITERATION) : MAX_ITERATION(aMAX_ITERATION) {
}

EndPredicate::~EndPredicate() {
}


EndPredicate::sharedPtr_t EndPredicate::createRelativeValues(const double tolerance) {
	return EndPredicate::sharedPtr_t(new RelativeValuesPred(tolerance));
}

EndPredicate::sharedPtr_t EndPredicate::createAbsoluteValues(const double tolerance) {
	return EndPredicate::sharedPtr_t(new AbsoluteValuesPred(tolerance));
}

} /* namespace Custom */
} /* namespace Maximizer */
} /* namespace Utils */
