//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GradientManager.cpp
 *
 * @date Jan 18, 2017
 * @author meyerx
 * @brief
 */
#include "GradientManager.h"

#include <stddef.h>

#include "mpi.h"

namespace Parallel {

GradientManager::GradientManager(){
	context = NONE_CONTEXT;
	role = None;
	layer = NULL;
}

GradientManager::~GradientManager() {
}

void GradientManager::init(context_t aContext, CommunicationLayer* aLayer) {

	context = aContext;
	layer = aLayer;
	role = defineRole(layer);

	setActive(true);
}

void GradientManager::deinit() {

	setActive(false);

	// Deinit roles
	context = NONE_CONTEXT;
	role = None;
	layer = NULL;
}

void GradientManager::gathervDoubleMgr(std::vector<double> &localLikelihoods, std::vector<int> &recvCount, std::vector<double> &gatherResults) const {
	int myManagerLocalRank = layer->getMyMasterLocalRank();
	MPI_Comm myCom = layer->getLocalCommMPI();

	std::vector<int> displs(recvCount.size(), 0);

	for(size_t i=1; i<recvCount.size(); ++i) {
		displs[i] = displs[i-1] + recvCount[i-1];
	}

	MPI_Gatherv(localLikelihoods.data(), localLikelihoods.size(), MPI_DOUBLE,
				gatherResults.data(), recvCount.data(), displs.data(), MPI_DOUBLE,
				myManagerLocalRank, myCom);
}

void GradientManager::gathervDoubleWrk(std::vector<double> &localLikelihoods) const {
	int myGradManagerLocalRank = layer->getMyMasterLocalRank();
	MPI_Comm myGradCom = layer->getLocalCommMPI();
	double *dummyDblPtr = NULL;
	int *dummyIntPtr = NULL;

	MPI_Gatherv(localLikelihoods.data(), localLikelihoods.size(), MPI_DOUBLE,
				dummyDblPtr, dummyIntPtr, dummyIntPtr, MPI_DOUBLE,
				myGradManagerLocalRank, myGradCom);
}

void GradientManager::bcastSample(Utils::Serialize::buffer_t &data, const int root) const {
	const CommunicationLayer *commL = layer;
	BaseManager::broadcastSerializedObject<DYNAMIC_SIZE>(data, root, commL);
}

bool GradientManager::checkRole(role_t aRole) const {
	return role == aRole;
}

int GradientManager::getMyRank() const {
	return layer->getMyLocalRank();
}

int GradientManager::getManagerRank() const {
	return layer->getMyMasterLocalRank();
}

int GradientManager::getNProc() const {
	return layer->getLocalNProc();
}

bool GradientManager::isSequential() const {
	return role == Sequential;
}

bool GradientManager::isManager() const {
	return (checkRole(Manager) ||
			checkRole(Sequential));
}

} /* namespace Parallel */
