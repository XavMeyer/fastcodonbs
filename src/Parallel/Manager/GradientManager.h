//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GradientManager.h
 *
 * @date Jan 18, 2017
 * @author meyerx
 * @brief
 */
#ifndef GRADIENTMANAGER_H_
#define GRADIENTMANAGER_H_

#include <Eigen/Core>

#include "../Topology/BaseTopology.h"
#include "BaseManager.h"
#include "MpiManager.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Parallel {

class MCMCManager;
class MLManager;

class GradientManager : public BaseManager {
public:
	enum context_t {MCMC_CONTEXT, ML_CONTEXT, NONE_CONTEXT};
public:

	void gathervDoubleWrk(std::vector<double> &localLikelihoods) const;
	void gathervDoubleMgr(std::vector<double> &localLikelihoods, std::vector<int> &recvCount, std::vector<double> &gatherResults) const;

	template <typename T>
	void bcastArray(const int size, T* data, const int root) const {
		const CommunicationLayer *commL = layer;
		Parallel::mpiMgr().bcast<T>(data, size, root, commL->getLocalCommMPI());
	}

	void bcastSample(Utils::Serialize::buffer_t &data, const int root) const;

	bool checkRole(role_t aRole) const;

	int getMyRank() const;
	int getManagerRank() const;
	int getNProc() const;

	bool isSequential() const;
	bool isManager() const;

private:

	enum tag_t {PARALLEL_GRADIENT_TAG=101, DEFAULT_TAG=100};
	role_t role;

	void init(context_t aContext, CommunicationLayer* aLayer);
	void deinit();

private:

	context_t context;
	CommunicationLayer* layer;

	// Defined in the body
	GradientManager();
	~GradientManager();
	// Not defined to avoid call
	GradientManager(const GradientManager&);
	GradientManager& operator=(const GradientManager&);

	friend GradientManager& gradientMgr();
	friend class MCMCManager;
	friend class MLManager;
};

inline GradientManager& gradientMgr() {
    static GradientManager instance;
    return instance;
}


} /* namespace Parallel */

#endif /* GRADIENTMANAGER_H_ */
