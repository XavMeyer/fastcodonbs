//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * RNG.cpp
 *
 *  Created on: 20 sept. 2013
 *      Author: meyerx
 */

#include "RNG.h"

#include "Sitmo11RNG.h"

#include <cmath>

const long unsigned int RNG::DEFAULT_SEED=5;

RNG* RNG::createSitmo11RNG(){
	return new Sitmo11RNG();
}

RNG* RNG::createSitmo11RNG(unsigned long int aSeed){
	return new Sitmo11RNG(aSeed);
}

vector<double> RNG::pdfMVN(const vector< vector<double> > &points, const vector<double> &mu, const vector<double> &sigma) const {

	// Follow matlab mvnpdf

	int n = mu.size();
	int nrhs = points.size();
	vector<double> res(nrhs, 0);

	// X minus mu
	Eigen::MatrixXd eigenRHS(n, nrhs);
	for(int i=0; i<nrhs; ++i){
		for(int j=0; j<n; ++j){
			eigenRHS(j, i) = points[i][j] - mu[j];
		}
	}

	// Get Cholesky decomp of Sigma
	Eigen::Map<const Eigen::MatrixXd> mappedSigma(sigma.data(), n, n);
	Eigen::LLT<Eigen::MatrixXd> choleskySigma(mappedSigma);
	Eigen::MatrixXd L = choleskySigma.matrixL();

	// sum of log trace of inverse matrix
	double logSqrtDetSigma = 0.;
	for(int i=0; i<n; ++i){
		logSqrtDetSigma += log(L(i,i));
	}

	// Solve S*x=rhs
	Eigen::LDLT<Eigen::MatrixXd> choleskyDecomp(L);
	if(choleskyDecomp.info() == Eigen::NumericalIssue) { cerr << "error in cholesky decomposition." << endl; abort();}
	if(choleskyDecomp.info() == Eigen::NoConvergence) { cerr << "error in cholesky decomposition." << endl; abort();}
	if(choleskyDecomp.info() == Eigen::InvalidInput) { cerr << "error in sigma input." << endl; abort();}

	//Eigen::Map<Eigen::MatrixXd> mappedRHS(rhs, nrhs, n);
	choleskyDecomp.solveInPlace(eigenRHS);
	Eigen::Map<Eigen::VectorXd> mappedRes(res.data(), nrhs);
	mappedRes = eigenRHS.cwiseProduct(eigenRHS).colwise().sum();

	// Compute pdf of each points
	for(int i=0; i<nrhs; ++i){
		res[i] = exp(-0.5*res[i] - logSqrtDetSigma - (double)n*log(2.*M_PI)/2.);
		//cout << res[i] << "\t";
	}
	//cout << endl;
	//getchar();
	return res;
}

long int RNG::drawFrom(const std::vector<float> &probs) const {
	Eigen::VectorXd eigenProbs(probs.size());
	for(size_t i=0; i<probs.size(); ++i) {
		eigenProbs(i) = probs[i];
	}
	return drawFrom(eigenProbs);
}

long int RNG::drawFrom(const std::vector<double> &probs) const {
	Eigen::VectorXd eigenProbs(probs.size());
	for(size_t i=0; i<probs.size(); ++i) {
		eigenProbs(i) = probs[i];
	}
	return drawFrom(eigenProbs);
}

long int RNG::drawFrom(const Eigen::VectorXd &probs) const {
	double sum=0.;
	double scaling = probs.sum(); // insure that probs sums to 1.
	double rnd = genUniformDbl();
	for(size_t iP=0; iP<(size_t)probs.size(); ++iP) {
		sum += probs(iP) / scaling; // Upper range
		if(rnd <= sum) { // If rnd falls into the range, then found
			return iP;
		}
	}
	return -1;
}

long int RNG::drawFromLargeNormalizedVector(const std::vector<float> &probs) const {
	double sum=0.;
	double rnd = genUniformDbl();
	for(size_t iP=0; iP<probs.size(); ++iP) {
		sum += probs[iP]; // Upper range
		if(rnd <= sum) { // If rnd falls into the range, then found
			return iP;
		}
	}
	assert(false && "This vector is not normalized.");
	return -1;
}

long int RNG::drawFromLargeNormalizedVector(const std::vector<double> &probs) const {
	double sum=0.;
	double rnd = genUniformDbl();
	for(size_t iP=0; iP<probs.size(); ++iP) {
		sum += probs[iP]; // Upper range
		if(rnd <= sum) { // If rnd falls into the range, then found
			return iP;
		}
	}
	assert(false && "This vector is not normalized.");
	return -1;
}

long int RNG::drawFromLargeUnormalizedVector(const double normalizingConstant, const std::vector<float> &probs) const {
	double sum=0.;
	double rnd = normalizingConstant*genUniformDbl();
	for(size_t iP=0; iP<probs.size(); ++iP) {
		sum += probs[iP]; // Upper range
		if(rnd <= sum) { // If rnd falls into the range, then found
			return iP;
		}
	}
	assert(false && "The normalizingConstant is not correct.");
	return -1;
}

long int RNG::drawFromLargeUnormalizedVector(const double normalizingConstant, const std::vector<double> &probs) const {
	double sum=0.;
	double rnd = normalizingConstant*genUniformDbl();
	for(size_t iP=0; iP<probs.size(); ++iP) {
		sum += probs[iP]; // Upper range
		if(rnd <= sum) { // If rnd falls into the range, then found
			return iP;
		}
	}
	assert(false && "The normalizingConstant is not correct.");
	return -1;
}
