# Installing FastCodonBS
[Back to main page](../README.md)

FastCodonBS is implemented in C++ and uses multiple libraries to enable efficient computations.

## Requirements
All these libraries must be installed before FastCodonBS compilation.

* [SCons](http://scons.org/doc/production/HTML/scons-user.html#idm139933257250608) is employed to ease the compilation of the code.
* [MPI](https://www.mpich.org/documentation/guides/) enables the use of parallel MC3.
* [Boost](https://www.boost.org) provides many useful tools.
* [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) for fast numeric computations.

## Configuring CoevRJ makefile

The next steps requires you to know the location of

* the **FastCodonBSRoot** folder where you downloaded (and extracted if needed) the FastCodonBS code.
* the **boostRoot** where you installed the Boost library.
* the **eigenRoot** where you extracted the Eigen library.

### Modifying the `makefile`
Once you have noted these three locations, go into the **FastCodonBSRoots** and open the `build/makefile`. Few lines must be updated to reflect your installation.

* `FastCodonBS_Root` must be set to the **FastCodonBSRoots** folder where the `src`, `build`, `data`, etc. folders are located.
* `libraryPaths` must indicate the folder where the Boost libraries are located. This should be the `lib` folder inside the **boostRoot** folder (e.g. where `libboost_regexp.a` is located on your system).
* `includePaths` must indicate the folders where
	1. the Boost headers are located. This should be the `include` folder inside the **boostRoot** folder
	2. the Eigen headers are located. This should be the **eigenRoot** folder.
* (Optional) You can tune the `nbProc` variable to define the number of processors employed for the compilation of the code.


### Compiling the code
Save the changes made to `build/makefile` and launch a terminal inside the `build/` folder. Launch the compilation by typing

	> make

Upon a successful compilation, SCons should report

	> scons: done building targets.
Additionally an executable `fastCodonBS` should be located within your `build/` folder.
