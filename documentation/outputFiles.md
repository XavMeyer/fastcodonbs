#Output files
[Back to main page](../README.md)

Given an output file base name (e.g. `path/to/out`) two files will be created:

 * A result file (e.g. `path/to/out.txt`)
 * A BEB file (e.g. `path/to/out_BEB.txt`)

## Result file

The result file contain the MLE for H0 and H1 as well as the result of the likelihood ratio test for each of the branches.

```
#9
-3495.59        2.10185 0       0       0.596233        0.403767        0.345817        1
((1:0.19934, 2:0.294852)*#9:0.429019, (3:0.263755, 4:0.261562)10:0.357767, ((5:0.251968, 6:0.225589)11:0.26221, (7:0.24879, 8:0.209012)12:0.289074)13:0.494121):0
-3490.72        2.11707 0       0       0.618484        0.381516        0.345218        5.39822
((1:0.201311, 2:0.295553)*#9:0.405608, (3:0.263434, 4:0.262732)10:0.37214, ((5:0.252301, 6:0.226077)11:0.259004, (7:0.250205, 8:0.208486)12:0.293947)13:0.50331):0
9.72609 0.000908353     2.70554

1
-3503.92        2.11799 0.566239        0.433761        0       0       0.435454        1
((*1:0.198099, 2:0.293414)#9:0.453327, (3:0.261838, 4:0.25994)10:0.351368, ((5:0.250453, 6:0.223595)11:0.26004, (7:0.246588, 8:0.207818)12:0.283085)13:0.490222):0
-3503.92        2.11799 0.56624 0.43376 0       0       0.435455        1.44529
((*1:0.198099, 2:0.293414)#9:0.453327, (3:0.261838, 4:0.25994)10:0.351369, ((5:0.250453, 6:0.223595)11:0.26004, (7:0.246588, 8:0.207818)12:0.283085)13:0.490222):0
3.55249e-09     0.499976        2.70554

```
Each branch is represented by a block of results (i.e. 6 lines).
Each block contains:

 * The branch name (e.g. `#9`)
 * The MLE for H0
 * The newick string for H0
 * The MLE for H1
 * The newick string for H1
 * The result of the LRT
 
The MLE contains:

 * The log Likelihood
 * Kappa
 * The proportions (p0, p1, p2a, p2b)
 * The omegas (w0, w1)

The newick tree contains the branch lengths inferred and the the foreground branch tested is denoted by a `*`.

The result of the LRT is composed of (i) the likelihood delta (2x[H1-H0]), (ii) the p-value and (iii) the significance threshold at alpha = 0.05.

## BEB File

The BEB file contains only results for branches that have passed the LRT.

Each block of lines contains:

 * The branch name
 * The marginalized posterior for w0, w1
 * The marginalized posterior for p0, p1, p2 (flattened on one line)
 * The marginalized posterior per site
