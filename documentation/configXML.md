#XML Configuration files
[Back to main page](../README.md)

This page detail the options available when using a XML configuration file.

	> mpirun -np <P> build/fastCodonBS -x <myConfig.xml>


Example of XML configuration file are available in the `config_files` folder.

## Running FastCodonBS analysis with an XML file

### Content of an XML configuration file


```xml
<config method="BEB"> <!-- ML for maximum likelihood / BEB for LRT and BEB -->
    <topology>
        <nGradient>1</nGradient> <!-- This number must be the same as the number of processors -->
    </topology>
    <seed>1234</seed>
    <nIteration>0</nIteration>
    <logFile>Results/test</logFile> <!-- Write the standard output to 'test.out' / if not present: write in the terminal -->
    <output>
      <filename>Results/output</filename>
    </output>
    <likelihood name="PositiveSelectionLight"> <!-- -->
        <hypothesis>H1</hypothesis>
        <isUsingCompression>true</isUsingCompression>
        <foregroundBranch>all</foregroundBranch>  <!-- file if defined in nwk file with #??? / all for all branches / internal branch index otherwise -->
        <nThread>1</nThread>  <!-- Define the number of posix thread use for each likelihood evaluation -->
        <CodonFrequencyType_ID>3</CodonFrequencyType_ID> <!-- Uniform=1, F61=2, F3x4=3, CF3x4=4 -->
        <alignFile>Dataset/POSITIVE_1/POSITIVE1_03x04_03x10_04x40_1.fas</alignFile>
        <treeFile>Dataset/tree.nwk</treeFile>
        <parameters>default</parameters>
    </likelihood>
    <maximizer name="LBFGSB"> <!--NLOPT or LBFGSB -->
    </maximizer>
</config>


```

---
### Parameters
#### Global
* **method** defines the type of analysis. *ML* will just optimize the parameters for the configuration specified. *BEB* will do the LRT and BEB for Yang's branch-site model and *Holm-Bonferroni REL* will do the Holm-Bonferroni test for Kosakovsky-Pond's REL model.
* **nGradient** defines the number of processors used to compute the gradient in parallel (MPI). This must be equal to P in the the `-np <P>` parameter provided to mpirun.
* **seed** defines the random seed employed for this run.
* (Optional) **logFile** - If specified, will redirect the *standard output* (text in the console) to the file provided.
* (Optional) **filename** - If specified, will write the results in files *path/baseName.txt* and *path/baseName_BEB.txt*.
* **nIterations** defines the maximum number of iterations for the maximizer (0 means no limit).

---
#### Likelihood
* **hypothesis** defines the hypothesis tested : H0 or H1. This parameter is important only when using the *ML* method.
* **isUsingCompression** defines if sites are compressed or not.
* **foregroundBranch** defines the foreground branch. If the value specified is -1, the branch is identified by a *#* in the newick file will be used as foreground branch. Any number between *0..nBranch-1* can be employed otherwise - the number correspond to the FastCodonBS internal branch identifier however.
* **nThread** defines the number of posix threads employed to parallelize a single likelihood computation. The total number of processors employed is therefore *nGradient x nThread*.
* **CodonFrequencyType_ID** defines the method used to define the empirical codon frequency.
* **alignFile** defines the path to the alignment file (fasta)
* **treeFile** defines the path to the tree topology file (newick) - the foreground branch can be specified with *#* (see **foregroundBranch** parameter).

-
##### Output

* **logFile** - path and basename of the output log file
