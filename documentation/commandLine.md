#XML Command line
[Back to main page](../README.md)

This page detail the options available when using the command line.

	>  mpirun -np nGrad ./fastCodonBS -m BS -nG nGradient -nT nThread -align /path/to/align.fas -tree /path/to/tree.fas -out /path/to/outputBaseName [-s seed]

## Running FastCodonBS analysis from the command line

### Model:
 *	-m		 Model: only Branch-Site is available through commad line for now - all branch are tested + BEB.
### Parallel config ([More information here](documentation/configXML.md)):
 *	-nG		 Number of processor dedicated for the evaluation of the gradient - distributed memory parallelism (mpi).
 *	-nT		 Number of processor dedicated for the evaluation for one likelihood - shared memory parallelism (posix).

### Input/output:
 *	-align		 Alignment file (fasta).
 *	-tree		 Tree file (newick).
 *	-out		 Path and base name for the output file(s).

### Misc/Optional:
 *	-seed		 Random number generator seed for the starting parameter values.
 *	-h		 Print this message.
